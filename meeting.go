package meeting

import (
	"context"
	"go.saastack.com/meeting/Integration/zoom"
	"go.saastack.com/meeting/pb"
	"go.saastack.io/chaku/errors"
	integration "go.saastack.io/integration/pb"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

var (
	// Generic Error to be returned to client to hide possible sensitive information
	errInternal = status.Error(codes.Internal, "oops! Something went wrong")
)

type meetingsServer struct {
	meetingStore pb.MeetingStore
	*pb.MeetingsServiceMeetingServerCrud
	integrationCli integration.IntegrationsClient
	zoomCli zoom.ZoomAPI
}

func NewMeetingsServer(
	meetingSt pb.MeetingStore,
    integrationClient integration.IntegrationsClient,

) pb.MeetingsServer {
	r := &meetingsServer{
		meetingStore: meetingSt,
		integrationCli:integrationClient,
		zoomCli:zoom.NewZoomServer(integrationClient),
	}

	meetingSC := pb.NewMeetingsServiceMeetingServerCrud(meetingSt, r)
	r.MeetingsServiceMeetingServerCrud = meetingSC
	return r
}

// These functions represent the BLoC(Business Logical Component) of the CRUDGen Server.
// These functions will contain business logic and would be called inside the generated CRUD functions

func (s *meetingsServer) CreateMeetingBLoC(ctx context.Context, in *pb.CreateMeetingRequest) error {

	if err := in.Validate(); err != nil {
		return nil
	}
    //Validate parent request
	switch in.GetMeeting().MeetingAppType {

	case pb.MeetingAppType_ZOOM:
		meeting , err := s.zoomCli.CreateMeeting(ctx,in.GetMeeting())
          if err != nil {
          	return err
		  }else if meeting.ErrorMessage != ""{
			  return status.Error(codes.Internal, meeting.ErrorMessage)
		  }
	case pb.MeetingAppType_WEBEX:
		 //todo Implement me
	default:
       // condition := pb.MeetingOr{pb.MeetingIdEq{Id:in.Meeting.Id},pb.MeetingParticipantListFirstNameEq{FirstName:in.Meeting.}}
	}
	return nil
}

func (s *meetingsServer) GetMeetingBLoC(ctx context.Context, in *pb.GetMeetingRequest) error {
	for _, m := range in.GetViewMask().GetPaths() {
		if !pb.ValidMeetingFieldMask(m) {
			return errors.ErrInvalidField
		}
	}
  return nil
}

func (s *meetingsServer) UpdateMeetingBLoC(ctx context.Context , in *pb.UpdateMeetingRequest) error {

	//id , meeting_id , meeting_type ,
	for _, m := range in.GetUpdateMask().GetPaths() {
		if !pb.ValidMeetingFieldMask(m) {
			return errors.ErrInvalidField
		}
		if m == "id" {
			return errors.ErrInvalidField
		}
		if m == "meeting_id" {
			return errors.ErrInvalidField
		}
		if m == "meeting_app_type" {
			return errors.ErrInvalidField
		}
		if m == "meeting_type" {
			return errors.ErrInvalidField
		}
	}
	//Getting the meetting object
	meeting , err := s.meetingStore.GetMeeting(ctx,[]string{},pb.MeetingIdEq{Id:in.Meeting.Id})
	if err != nil {
		return err
	}

	switch meeting.GetMeetingAppType() {
	case pb.MeetingAppType_ZOOM:
		meeting , err := s.zoomCli.UpdateMeeting(ctx,meeting , in)
		if err != nil {
			return err
		}else if meeting.ErrorMessage != ""{
			return status.Error(codes.Internal, meeting.ErrorMessage)
		}
	case pb.MeetingAppType_WEBEX:
		//todo Implement me
	default:
	}

	return nil
}

func (s *meetingsServer) DeleteMeetingBLoC(ctx context.Context, in *pb.DeleteMeetingRequest) error {

	meeting , err := s.meetingStore.GetMeeting(ctx , []string{ "meeting_id","meeting_app_type","meeting_type"} ,
	pb.MeetingIdEq{Id:in.GetId()})
	if err != nil {
		return err
	}
	switch meeting.GetMeetingAppType() {
	   case pb.MeetingAppType_ZOOM:
		   if err := s.zoomCli.DeleteMeeting(ctx,meeting) ; err != nil {
		   	return err
		   }
	   default:
	}
	return nil
}


func (s *meetingsServer) ListMeeting(ctx context.Context, in *pb.ListMeetingsRequest) (*pb.ListMeetingResponse, error) {
	panic("implement me")
}
