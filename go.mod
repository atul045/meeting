module go.saastack.com/meeting

go 1.12

require (
	github.com/Shivam010/protoc-gen-validate v0.2.3
	github.com/elgris/sqrl v0.0.0-20190909141434-5a439265eeec
	github.com/golang/mock v1.4.0 // indirect
	github.com/golang/protobuf v1.3.3
	github.com/google/btree v1.0.0 // indirect
	github.com/grpc-ecosystem/grpc-gateway v1.13.0
	go.appointy.com/waqt/appointment v0.0.0-20200215085322-ce5a849707ca // indirect
	go.saastack.io/chaku v0.0.0-20200122152130-6ed5bdad1747
	go.saastack.io/deployment/right v0.0.0-20200214114141-636fbc275a36
	go.saastack.io/eventspush v0.0.0-20200123115639-edcdfa4d5be3
	go.saastack.io/idutil v0.0.0-20200122122527-bf060a213abc
	go.saastack.io/integration v0.0.0-20200214104600-b48773e24060
	go.saastack.io/jaal v0.0.0-20200123063706-70f83f8af8cc
	go.saastack.io/modulerole v0.0.0-20200128083234-1b73fef018d4
	go.saastack.io/pehredaar v0.0.0-20200123083532-ceaece59709e
	go.saastack.io/protoc-gen-nakaab v0.0.0-20200124074048-9eedb7860ee1
	go.saastack.io/protos v0.0.0-20200218114347-cd2f25902c03
	go.saastack.io/right v0.0.0-20200127071935-e5229082986f
	go.saastack.io/userinfo v0.0.0-20200123065653-2867662c53ed
	go.uber.org/cadence v0.10.5
	go.uber.org/fx v1.10.0
	go.uber.org/zap v1.13.0
	golang.org/x/net v0.0.0-20200202094626-16171245cfb2
	google.golang.org/genproto v0.0.0-20200212174721-66ed5ce911ce
	google.golang.org/grpc v1.27.1
)

replace go.uber.org/fx => github.com/appointy/fx v1.9.1-0.20190624110333-490d04d33ef6

replace go.uber.org/dig => github.com/paullen/dig v1.7.1-0.20190624104937-6e47ebbbdcf6

replace github.com/apache/thrift => github.com/apache/thrift v0.0.0-20190309152529-a9b748bb0e02
