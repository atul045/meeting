package meeting

import "go.uber.org/fx"
import "go.saastack.com/meeting/pb"

// Module is the fx module encapsulating all the providers of the package
var Module = fx.Provide(
	pb.NewPostgresMeetingStore,
	NewMeetingsServer,
	pb.NewLocalMeetingsClient,
)
