package model



//This is the same object which we will send to zoom ApI for create Meeting
//There are four types of meeting type 1 is for Instant Meeting ,
// 2 is for Scheduled Meeting
//3 is for recurring meeting with no fixed time
// 8 is for Recurring meeting with fixed time
//Base Object for meeting
type CreateMeetingRequest struct {
	//topic of meeting
	Topic string `protobuf:"bytes,1,opt,name=topic,proto3" json:"topic,omitempty"`
	//topic of meeting
	Type      int64  `protobuf:"varint,2,opt,name=type,proto3" json:"type,omitempty"`
	StartTime string `protobuf:"bytes,3,opt,name=start_time,json=startTime,proto3" json:"start_time,omitempty"`
	//Duration is only for scheduled Meeting
	Duration int `protobuf:"bytes,4,opt,name=duration,proto3" json:"duration,omitempty"`
	//TimeZone is for scheduled meeting only
	TimeZone string `protobuf:"bytes,5,opt,name=timeZone,proto3" json:"timeZone,omitempty"`
	//PassWord to join the meeting
	Password string `protobuf:"bytes,6,opt,name=password,proto3" json:"password,omitempty"`
	//Agenda of Meeting
	Agenda string `protobuf:"bytes,7,opt,name=agenda,proto3" json:"agenda,omitempty"`
	//Recurrence is only for recurring meeting
	Recurrence *Recurrence `protobuf:"bytes,8,opt,name=recurrence,proto3" json:"recurrence,omitempty"`
	//Setting Of Meeting
	Settings             *Settings `protobuf:"bytes,9,opt,name=settings,proto3" json:"settings,omitempty"`
}



type Recurrence struct {
	//recurrence meeting type ex: 1==>Daily , 2 ==> Weekly , 3 ==> Monthly
	Type int32 `protobuf:"varint,1,opt,name=type,proto3" json:"type,omitempty"`
	//At which interval should the meeting repeat?
	//For a daily meeting there maximum of 90 days ,
	//For Weekly meeting there should be a maximum of 12 weeks
	//For monthly meeting there should be maximum of 3 months
	RepeatInterval int32 `protobuf:"varint,2,opt,name=repeat_interval,json=repeatInterval,proto3" json:"repeat_interval,omitempty"`
	//Days of the meeting should be repeat
	//Note : multiple values be separated by comma . for example: 1, 2
	//Where 1 , 2 represent Sunday , Monday and etc
	WeeklyDays string `protobuf:"bytes,3,opt,name=weekly_days,json=weeklyDays,proto3" json:"weekly_days,omitempty"`
	//Day in the month the meeting should be scheduled. The value ranges from 0 to 31
	MonthlyDay string `protobuf:"bytes,4,opt,name=monthly_day,json=monthlyDay,proto3" json:"monthly_day,omitempty"`
	//The week meeting will recur each month.
	// -1 for LastWeek , 1 for LastWeek , 2 for SecondWeek , 3 for Third Week , 4 for Fourth Week
	MonthlyWeek string `protobuf:"bytes,5,opt,name=monthly_week,json=monthlyWeek,proto3" json:"monthly_week,omitempty"`
	//The weekday a meeting should recur each month for ex 1 for sunday , 2 for monday ...
	MonthlyWeekDay string `protobuf:"bytes,6,opt,name=monthly_week_day,json=monthlyWeekDay,proto3" json:"monthly_week_day,omitempty"`
	//Select how many times the meeting will recur in each month
	EndTimes int `protobuf:"bytes,7,opt,name=end_times,json=endTimes,proto3" json:"end_times,omitempty"`
	//Select a date the meeting  will recur  before it is cancel. Should be in UTC time and cannot be
	// used with end times
	EndDateTime          string   `protobuf:"bytes,8,opt,name=end_date_time,json=endDateTime,proto3" json:"end_date_time,omitempty"`
}



type Settings struct {
	JoinBeforeHost bool `protobuf:"varint,1,opt,name=join_before_host,json=joinBeforeHost,proto3" json:"join_before_host,omitempty"`
	// 0 for automatically approve , 1 for manually approve , 2 for no registration required
	ApprovalType int32 `protobuf:"varint,2,opt,name=approval_type,json=approvalType,proto3" json:"approval_type,omitempty"`
	//1==> Attendess register once and can attend any of the ocurrence
	//2==>Attendees need to register  for each ocurrence to attend
	//3==> Attendees register once and can choose one or more ocurrence to attend
	RegistrationType int32 `protobuf:"varint,3,opt,name=registration_type,json=registrationType,proto3" json:"registration_type,omitempty"`
	//only signed user can attend meeting
	EnforceLogin bool `protobuf:"varint,4,opt,name=enforce_login,json=enforceLogin,proto3" json:"enforce_login,omitempty"`
	//Close Registration after event date
	CloseRegistration bool `protobuf:"varint,5,opt,name=close_registration,json=closeRegistration,proto3" json:"close_registration,omitempty"`
	//Only authenticated user can join the meeting if the value  of this filled is true
	MeetingAuthentication bool     `protobuf:"varint,6,opt,name=meeting_authentication,json=meetingAuthentication,proto3" json:"meeting_authentication,omitempty"`
}



//Meeting Response
type CreateMeetingResponse struct {
	Id int `protobuf:"bytes,1,opt,name=id,proto3" json:"id,omitempty"`
	//meeting type
	Type int `protobuf:"varint,2,opt,name=type,proto3" json:"type,omitempty"`
	//uuid
	Uuid string `protobuf:"bytes,3,opt,name=uuid,proto3" json:"uuid,omitempty"`
	Join_url string `json:"join_url"`
	//occurrences
	Occurrences          []*Occurrences `protobuf:"bytes,4,rep,name=occurrences,proto3" json:"occurrences,omitempty"`
	Error
}



type Occurrences struct {
	OccurrenceId         string   `protobuf:"bytes,1,opt,name=occurrence_id,json=occurrenceId,proto3" json:"occurrence_id,omitempty"`
	StartTime            string   `protobuf:"bytes,2,opt,name=start_time,json=startTime,proto3" json:"start_time,omitempty"`
	Duration             string   `protobuf:"bytes,3,opt,name=duration,proto3" json:"duration,omitempty"`
	Status               string   `protobuf:"bytes,4,opt,name=status,proto3" json:"status,omitempty"`
}


// Add Registrant
type AddRegistrantRequest struct {
	Email                string   `protobuf:"bytes,1,opt,name=email,proto3" json:"email,omitempty"`
	FirstName            string   `protobuf:"bytes,2,opt,name=first_name,json=firstName,proto3" json:"first_name,omitempty"`
	LastName             string   `protobuf:"bytes,3,opt,name=last_name,json=lastName,proto3" json:"last_name,omitempty"`
	Address              string   `protobuf:"bytes,4,opt,name=address,proto3" json:"address,omitempty"`
	City                 string   `protobuf:"bytes,5,opt,name=city,proto3" json:"city,omitempty"`
	Country              string   `protobuf:"bytes,6,opt,name=country,proto3" json:"country,omitempty"`
	Zip                  string   `protobuf:"bytes,7,opt,name=zip,proto3" json:"zip,omitempty"`
	State                string   `protobuf:"bytes,8,opt,name=state,proto3" json:"state,omitempty"`
	Phone                string   `protobuf:"bytes,9,opt,name=phone,proto3" json:"phone,omitempty"`
	Industry             string   `protobuf:"bytes,10,opt,name=industry,proto3" json:"industry,omitempty"`
	Org                  string   `protobuf:"bytes,11,opt,name=org,proto3" json:"org,omitempty"`
	Comments             string   `protobuf:"bytes,12,opt,name=comments,proto3" json:"comments,omitempty"`
}


//Cancel Registrant
type CancelRegistrantRequest struct {
	Action string `json:"action"`
	Registrant []*Registrant `json:"registrant"`
}

type Registrant struct {
	id string `json:"id"`
	email string `json:"email"`
}

type AddRegistrantResponse struct {
	//Meeting Id
	Id int32 `protobuf:"varint,1,opt,name=id,proto3" json:"id,omitempty"`
	//Unique Identifier of the registrant
	RegistrantId string `protobuf:"bytes,2,opt,name=registrant_id,json=registrantId,proto3" json:"registrant_id,omitempty"`
	//the start time for the meeting
	StartTime string `protobuf:"bytes,3,opt,name=start_time,json=startTime,proto3" json:"start_time,omitempty"`
	//topic of the meeting
	Topic                string   `protobuf:"bytes,4,opt,name=topic,proto3" json:"topic,omitempty"`
}


//Update a meeting
type UpdateMeetingRequest struct {
	//topic of meeting
	Topic string `protobuf:"bytes,1,opt,name=topic,proto3" json:"topic,omitempty"`
	//topic of meeting
	Type      int64  `protobuf:"varint,2,opt,name=type,proto3" json:"type,omitempty"`
	StartTime string `protobuf:"bytes,3,opt,name=start_time,json=startTime,proto3" json:"start_time,omitempty"`
	//Duration is only for scheduled Meeting
	Duration int `protobuf:"bytes,4,opt,name=duration,proto3" json:"duration,omitempty"`
	//TimeZone is for scheduled meeting only
	TimeZone string `protobuf:"bytes,5,opt,name=timeZone,proto3" json:"timeZone,omitempty"`
	//PassWord to join the meeting
	Password string `protobuf:"bytes,6,opt,name=password,proto3" json:"password,omitempty"`
	//Agenda of Meeting
	Agenda string `protobuf:"bytes,7,opt,name=agenda,proto3" json:"agenda,omitempty"`
	//Recurrence is only for recurring meeting
	Recurrence *Recurrence `protobuf:"bytes,8,opt,name=recurrence,proto3" json:"recurrence,omitempty"`
	//Setting Of Meeting
	Settings             *Settings `protobuf:"bytes,9,opt,name=settings,proto3" json:"settings,omitempty"`
	ScheduleFor          string    `protobuf:"bytes,10,opt,name=schedule_for,json=scheduleFor,proto3" json:"schedule_for,omitempty"`
}


type UpdateMeetingResponse struct {
	StatusCode           int32    `protobuf:"varint,1,opt,name=status_code,json=statusCode,proto3" json:"status_code,omitempty"`

}

type Error struct {
	Code int `json:"code"`
	Message string `json:"message"`
}
