package zoom

import (
	"context"
	"encoding/json"
	"fmt"
	"github.com/golang/protobuf/ptypes"
	"go.saastack.com/meeting/Integration/zoom/helper"
	"go.saastack.com/meeting/Integration/zoom/model"
	"go.saastack.com/meeting/pb"
	integration "go.saastack.io/integration/pb"
	"go.saastack.io/protos/types"
	"strconv"
)


/*
TODO
    todo handle recurrence rule
 */

//first we will get the integration
func (s *APIServerZoom) GetIntegrations(ctx context.Context, userId string) (*integration.IntegrationToken, error) {
	// Get integrations
     lisResponse , err := s.integrationCli.ListIntegrations(ctx ,&integration.ListIntegrationsRequest{
		 IntegrationType:      []types.IntegrationType{types.IntegrationType_ZOOM},
		 TargetId:             userId,
	 })
     //Checking the length of Integration so that we know that Integration
     //found or not
     if err != nil || lisResponse == nil  || len(lisResponse.Integrations) < 1{
        if err != nil {
        	return nil , err
		}
		return nil , errIntegrationNotPresent
	 }
	 return lisResponse.Integrations[0] , nil
}


//scope must be this ==> meeting:write:admin meeting:write
func (s *APIServerZoom) CreateMeeting(ctx context.Context , meeting *pb.Meeting) ( *pb.Meeting ,error) {
	var (
	      meetingRequest *model.CreateMeetingRequest
	      meetingResponse model.CreateMeetingResponse
		  makeMeetingUrl string
	)
	makeMeetingUrl =  "https://api.zoom.us/v2/users/" + meeting.GetUserId() + "/meetings"
	/*integrationDetails , err := s.GetIntegrations(ctx, meeting.GetUserId())
	if err != nil {
		return nil,err
	}*/
	switch meeting.GetMeetingType(){

	case pb.MeetingType_SCHEDULED_MEETING:
		//2 is for scheduled meeting
		meetingRequest = helper.CreateMeetingDefaultObject()
		meetingRequest.Type = 2
		meetingRequest.Topic = meeting.GetTopic()
		time , err  := ptypes.Timestamp(meeting.GetStartTime())
		if err != nil {
			return nil , err
		}
		formatted := fmt.Sprintf("%d-%02d-%02dT%02d:%02d:%02d",
			time.Year(), time.Month(), time.Day(),
			time.Hour(), time.Minute(), time.Second())
		meetingRequest.StartTime = formatted
		meetingRequest.Duration = int(meeting.GetDuration())
		meetingRequest.TimeZone = meeting.GetTimezone()
		meetingRequest.Agenda = meeting.GetDescription()
	case pb.MeetingType_RECURRING_FIXED:
		//2 is for scheduled meeting
		meetingRequest = helper.CreateMeetingDefaultObject()
		meetingRequest.Type = 8
		meetingRequest.Topic = meeting.GetTopic()
		meetingRequest.StartTime = meeting.GetStartTime().String()
		//Time will be in seconds convert it into minute for zoom api
		meetingRequest.Duration = int(meeting.GetDuration()/60)
		meetingRequest.TimeZone = meeting.GetTimezone()
		meetingRequest.Agenda = meeting.GetDescription()

	case pb.MeetingType_RECURRING_NO_FIXED:
		//todo
	case pb.MeetingType_UNKNOWN:
		return nil,errIntegrationNotPresent
	}
	resBody ,response , err := helper.MakeMeetingRequest(meetingRequest ,makeMeetingUrl ,nil , "POST")
	if err != nil {
		return nil,err
	}
	//check status code
	if response.StatusCode != 201 {
		var zoomErr *model.Error
		err = json.Unmarshal(resBody , &zoomErr)
		if err != nil {
			return nil,err
		}
       meeting.ErrorMessage = zoomErr.Message
       return meeting , nil
	}

	err = json.Unmarshal(resBody , &meetingResponse)
	if err != nil {
		return nil,err
	}
   //Setup meeting Id and recurrence Id
	meeting.MeetingId = strconv.Itoa(meetingResponse.Id)
	//Setting Up join Url
	meeting.JoinUrl = meetingResponse.Join_url
    //Adding Participant List
    meeting , err = s.RegisterParticipant(ctx ,meeting)
    if err != nil {
    	return nil , err
	}
	//In case of recurrence meeting there will be different recurrence rule Id
    if meetingResponse.Type != 2 {
    	for i , v := range meetingResponse.Occurrences {
    		meeting.RecurrenceRuleId[i] = v.OccurrenceId
		}
	}
	return meeting , nil
}


func (s *APIServerZoom) DeleteMeeting(ctx context.Context , meeting *pb.Meeting) error {
//Delete a meeting for recurrence
	makeDeleteUrl := "https://api.zoom.us/v2/meetings/" + meeting.GetMeetingId() + "?schedule_for_reminder=true"
	/*integrationDetails , err := s.GetIntegrations(ctx, meeting.GetUserId())
	if err != nil {
		return err
	}*/
	switch meeting.GetMeetingType(){

	case pb.MeetingType_SCHEDULED_MEETING:

	case pb.MeetingType_RECURRING_FIXED:
		//todo
	case pb.MeetingType_RECURRING_NO_FIXED:
		//todo
	case pb.MeetingType_UNKNOWN:
		return errIntegrationNotPresent
	}

	resBody ,response , err := helper.MakeMeetingRequest(nil ,makeDeleteUrl ,nil , "DELETE")
	if err != nil {
		return err
	}
	//check status code
	if response.StatusCode != 204 {
		var zoomErr *model.Error
		err = json.Unmarshal(resBody , &zoomErr)
		if err != nil {
			return err
		}
	}
	return  nil
}

func (s *APIServerZoom) UpdateMeeting(ctx context.Context , meeting *pb.Meeting , in *pb.UpdateMeetingRequest)( *pb.Meeting ,error) {

	var (
		meetingRequest *model.UpdateMeetingRequest
		makeMeetingUrl string
	)
	//Initializing Update Meeting Request with previous data
	meetingRequest = helper.UpdateMeetingDefaultObject()
	meetingRequest.Type = 2
	meetingRequest.Agenda = meeting.GetDescription()
	meetingRequest.TimeZone = meeting.GetTimezone()
	meetingRequest.Duration =  int(meeting.GetDuration())
	// meetingRequest.Password = meeting.GetMeeting().GetPassword()
	time , err  := ptypes.Timestamp(meeting.GetStartTime())
	if err != nil {
		return nil , err
	}
	formatted := fmt.Sprintf("%d-%02d-%02dT%02d:%02d:%02d",
		time.Year(), time.Month(), time.Day(),
		time.Hour(), time.Minute(), time.Second())
	meetingRequest.StartTime = formatted
	meetingRequest.Topic = meeting.GetTopic()

    //update meeting Url
	makeMeetingUrl =  "https://api.zoom.us/v2/meetings/" + meeting.GetMeetingId()
	switch meeting.GetMeetingType() {
	case pb.MeetingType_SCHEDULED_MEETING:
         meetingRequest.Type = 2
         meetingRequest.Agenda = in.GetMeeting().GetDescription()
         meetingRequest.TimeZone = in.GetMeeting().GetTimezone()
         meetingRequest.Duration =  int(in.GetMeeting().GetDuration())
        // meetingRequest.Password = meeting.GetMeeting().GetPassword()
		time , err  := ptypes.Timestamp(in.GetMeeting().GetStartTime())
		if err != nil {
			return nil , err
		}
		formatted := fmt.Sprintf("%d-%02d-%02dT%02d:%02d:%02d",
			time.Year(), time.Month(), time.Day(),
			time.Hour(), time.Minute(), time.Second())
         meetingRequest.StartTime = formatted
         meetingRequest.Topic = in.GetMeeting().GetTopic()
	case pb.MeetingType_RECURRING_FIXED:
		//tod
	case pb.MeetingType_RECURRING_NO_FIXED:
		//todo
	case pb.MeetingType_UNKNOWN:
		return nil,errIntegrationNotPresent

	}
	resBody ,response , err := helper.MakeMeetingRequest(meetingRequest ,makeMeetingUrl ,nil , "PATCH")
	if err != nil {
		return nil,err
	}

	//check status code
	if response.StatusCode != 204 {
		var zoomErr *model.Error
		err = json.Unmarshal(resBody , &zoomErr)
		if err != nil {
			return nil,err
		}
		meet := meeting
		meet.ErrorMessage = zoomErr.Message
		return meet, nil
	}

	return meeting , nil
}

func (s *APIServerZoom) RegisterParticipant(ctx context.Context , meeting *pb.Meeting) ( *pb.Meeting ,error) {
    var (
    	registerMeetingRequest *model.AddRegistrantRequest
		registerMeetingResponse model.AddRegistrantResponse
	)
    makeRegisterUrl := "https://api.zoom.us/v2/meetings/" + meeting.GetMeetingId()+ "/registrants"
	/*integrationDetails , err := s.GetIntegrations(ctx, meeting.GetUserId())
	if err != nil {
		return nil,err
	}*/
	switch meeting.GetMeetingType(){
	case pb.MeetingType_SCHEDULED_MEETING:
		//2 is for scheduled
		for i , v := range meeting.GetParticipantList() {
			//creating default object
			registerMeetingRequest = helper.AddRegistrantDefaultObject()
			registerMeetingRequest.FirstName = v.GetFirstName()
			registerMeetingRequest.LastName = v.GetLastName()
			registerMeetingRequest.Email = v.GetEmail()
			//todo remove nil with integration details
			resBody ,response , err := helper.MakeMeetingRequest(registerMeetingRequest ,makeRegisterUrl ,nil,"POST")
			//check status code
			if response.StatusCode != 201 {
				if err != nil {
					return meeting , err
				}
			}
			err = json.Unmarshal(resBody , &registerMeetingResponse)
			if err != nil {
				return nil,err
			}
			//storing registrant id
			meeting.GetParticipantList()[i].ParticipantId = registerMeetingResponse.RegistrantId
			meeting.GetParticipantList()[i].Id = meeting.GetMeetingId()
		}

	case pb.MeetingType_RECURRING_FIXED:
		//8 is for recurring with fixed time
	case pb.MeetingType_RECURRING_NO_FIXED:
		//todo
	case pb.MeetingType_UNKNOWN:
		//return nil,errIntegrationNotPresent

	}
	return meeting , nil
}


func (s* APIServerZoom) CancelParticipant(ctx context.Context , meeting *pb.Meeting) (*pb.Meeting , error) {
/*	var (
		cancelRegistrantRequest model.CancelRegistrantRequest
	)
	makeRegisterCancelUrl := "https://api.zoom.us/v2/meetings/" + meeting.GetMeetingId() + "/registrants/status"
	/*integrationDetails , err := s.GetIntegrations(ctx, meeting.GetUserId())
	if err != nil {
		return nil,err
	}*/

/*
	switch meeting.GetMeetingType() {
	case pb.MeetingType_SCHEDULED_MEETING:
		//2 is for scheduled
		for i , v := range meeting.GetParticipantList() {
			//creating default object
			for i , v := range meeting.GetParticipantList()
			cancelRegistrantRequest.Registrant = []*model.Registrant
			cancelRegistrantRequest.Action ="cancel"
			registerMeetingRequest.Email = v.GetEmail()
			//todo remove nil with integration details
			resBody ,response , err := helper.MakeMeetingRequest(registerMeetingRequest ,makeRegisterUrl ,nil,"POST")
			//check status code
			if response.StatusCode != 201 {
				if err != nil {
					return meeting , err
				}
			}
			err = json.Unmarshal(resBody , &registerMeetingResponse)
			if err != nil {
				return nil,err
			}
			//storing registrant id
			meeting.GetParticipantList()[i].ParticipantId = registerMeetingResponse.RegistrantId
			meeting.GetParticipantList()[i].Id = meeting.GetMeetingId()
		}

	case pb.MeetingType_RECURRING_FIXED:
		//8 is for recurring with fixed time
	case pb.MeetingType_RECURRING_NO_FIXED:
		//todo
	case pb.MeetingType_UNKNOWN:
		//return nil,errIntegrationNotPresent
	}*/
	return  nil, nil
}


