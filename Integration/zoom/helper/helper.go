package helper

import (
	"bytes"
	"encoding/json"
	model "go.saastack.com/meeting/Integration/zoom/model"
	integration "go.saastack.io/integration/pb"
	"io/ioutil"
	"net/http"
	"time"
)

/*
This file contains the default object corresponding to the zoom Api
 */


var v = "Bearer eyJhbGciOiJIUzUxMiIsInYiOiIyLjAiLCJraWQiOiJmMGI3NDk1Yi0zNzcyLTQ2MjctYTJjNC1lZWRkNzYwZGY2MDAifQ.eyJ2ZXIiOiI2IiwiY2xpZW50SWQiOiJ4Wmhfb0FEcVI1MmJfYkp2bEtnUnciLCJjb2RlIjoiMjFBTGlvNFhVRV9FRWs3YmU5UFRzQ2R2VS1LWkQyNmdRIiwiaXNzIjoidXJuOnpvb206Y29ubmVjdDpjbGllbnRpZDp4Wmhfb0FEcVI1MmJfYkp2bEtnUnciLCJhdXRoZW50aWNhdGlvbklkIjoiMjM5OTIyYmU4NGU0YjE4NmVlNzQwNDE1OTZjMmZjOWMiLCJ1c2VySWQiOiJFRWs3YmU5UFRzQ2R2VS1LWkQyNmdRIiwiZ3JvdXBOdW1iZXIiOjAsImF1ZCI6Imh0dHBzOi8vb2F1dGguem9vbS51cyIsImFjY291bnRJZCI6Ii02OU92YU9BUWlxMHlEcXFJaWpDLUEiLCJuYmYiOjE1ODUxMTMzMTAsImV4cCI6MTU4NTExNjkxMCwidG9rZW5UeXBlIjoiYWNjZXNzX3Rva2VuIiwiaWF0IjoxNTg1MTEzMzEwLCJqdGkiOiIyODAxOGRkMS1kNzcwLTRmNmUtODE4Mi1lYzgwM2QzYjNiZWUiLCJ0b2xlcmFuY2VJZCI6MH0.bKyYK82u9-h6lQ-aEfvXJDKGSPWQPPHkpWYuTFO-yVnOnuT8fWLu3Up7vH7CbUwHb6NU0Mdb_ZPH0fLzOEHXXA"
//This is default object for Create Meeting Api Call
func CreateMeetingDefaultObject() *model.CreateMeetingRequest{
	return &model.CreateMeetingRequest{
		Topic:      "",
		Type:       0,
		StartTime:  "",
		Duration:   0,
		TimeZone:   "",
		Password:   "",
		Agenda:     "",
		Recurrence: nil,
		Settings:   &model.Settings{
			JoinBeforeHost:        true,
			ApprovalType:          0,
			RegistrationType:      0,
			EnforceLogin:          false,
			CloseRegistration:     true,
			MeetingAuthentication: false,
		},
	}
}

func UpdateMeetingDefaultObject() *model.UpdateMeetingRequest {
	return &model.UpdateMeetingRequest{
		Topic:       "",
		Type:        0,
		StartTime:   "",
		Duration:    0,
		TimeZone:    "",
		Password:    "",
		Agenda:      "",
		Recurrence:  nil,
		Settings:    &model.Settings{
			JoinBeforeHost:        true,
			ApprovalType:          0,
			RegistrationType:      0,
			EnforceLogin:          false,
			CloseRegistration:     true,
			MeetingAuthentication: false,
		},
		ScheduleFor: "",
	}
}


//Custom Method which will return the DefaultObject of meeting request
func AddRegistrantDefaultObject()  *model.AddRegistrantRequest{
	return &model.AddRegistrantRequest{
		Email:     "test@register.com",
		FirstName: "",
		LastName:  "",
		Address:   "",
		City:      "",
		Country:   "",
		Zip:       "",
		State:     "",
		Phone:     "",
		Industry:  "",
		Org:       "",
		Comments:  "",
	}
}


//Custom method which will hit the Url of Zoom to make a post request
func MakeMeetingRequest(customRequest interface{} ,
	url string ,token *integration.IntegrationToken , method string) ([]byte , *http.Response,error) {
	body , err :=  json.Marshal(customRequest)
	if err != nil {
		return nil,nil ,err
	}
	client := &http.Client {
		Timeout: 5 * time.Minute,
	}
	req , err := http.NewRequest(method , url , bytes.NewBuffer(body))
	if err != nil {
		return nil,nil ,err
	}
	//todo
	req.Header.Add("Authorization" , v)
	req.Header.Add("Content-Type" ,"application/json")
	response , err := client.Do(req)
	if err != nil {
		return nil,nil ,err
	}
	//Reading response
	resBody, err := ioutil.ReadAll(response.Body)
	if err != nil {
		return nil,nil ,err
	}
	return resBody , response , nil
}

/*
//Custom method which will convert string rrule to the meeting of Zoom recurrence
func ConvertRecurrenceToMeeting(rrule string) error {
	var recurrenceObject model.Recurrence

//FREQ=DAILY;INTERVAL=10;COUNT=5
//Freq will be Weekly , Monthly , Yearly
 str := strings.Split(rrule ,";")
 for _ , v := range str {
 	den := strings.Split(v,"=")
 	//Freq
 	if den[0] == "FREQ" {
         if den[1] == "DAILY" {
           recurrenceObject.Type = 1
		 }else if den[1] == "WEEKLY" {
			 recurrenceObject.Type = 2
		 }else if den[1] == "MONTHLY" {
			 recurrenceObject.Type = 3
		 }else if den[1] == "YEARLY" {
            //todo yearly service is not available in zoom
		 }
	}
	//Interval logic
	if den[0] == "INTERVAL" {
		interval , err := strconv.Atoi(den[1])
		if err != nil {
			return err
		}
		recurrenceObject.RepeatInterval = int32(interval)
	}
	//Count Logic
	if den[0] == "COUNT" {
		count , err := strconv.Atoi(den[1])
		if err != nil {
			return err
		}
		recurrenceObject.EndTimes = count
	}
	//DTStart Logic
	if den[0] == "DTSTART" {
		//todo
	}
	//Weekday logic
    if den[0] == "WKST" {

	}

 }

}
*/









