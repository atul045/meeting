package zoom

import (
	"context"
	"go.saastack.com/meeting/pb"
	integration "go.saastack.io/integration/pb"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

var (
	errIntegrationNotPresent = status.Error(codes.NotFound ,"Integration Not present")
	errInternal = status.Error(codes.Internal, "oops! Something went wrong")
)
//This is the struct which have the list of services provided by the
//zoom Api that we want to implement in our services.
type ZoomAPI interface {
	//Create Zoom Meeting
	CreateMeeting(ctx context.Context , meeting *pb.Meeting) ( *pb.Meeting ,error)
	//Delete the zoom meeting
	DeleteMeeting(ctx context.Context , meeting *pb.Meeting) error
	//Update the list of zoom meeting
	UpdateMeeting(ctx context.Context , meeting *pb.Meeting , in *pb.UpdateMeetingRequest)( *pb.Meeting ,error )
	//Register a participant for a meeting.
	RegisterParticipant(ctx context.Context , meeting *pb.Meeting) ( *pb.Meeting ,error)
	//Cancel Registrant
	CancelParticipant(ctx context.Context , meeting *pb.Meeting) (*pb.Meeting , error)
}


//API Server zoom consist the integration client to get Access Token , user client
//to get the user Id of the user and config consist the list of detail of zoom
//for example client id and etc.
type APIServerZoom struct {
	integrationCli integration.IntegrationsClient
}

//This will be created inside the proto file with the particular end point
func NewZoomServer(
	integrationCli integration.IntegrationsClient)  ZoomAPI{
	return &APIServerZoom{
		integrationCli: integrationCli,
	}
}



