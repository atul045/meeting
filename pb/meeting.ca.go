package pb

import (
	"context"

	. "github.com/golang/protobuf/ptypes/empty"
	"go.uber.org/cadence/activity"
	"go.uber.org/cadence/workflow"
)

var _ = Empty{}

const (
	MeetingsListMeetingActivity   = "/saastack.meetings.v1.Meetings/ListMeeting"
	MeetingsCreateMeetingActivity = "/saastack.meetings.v1.Meetings/CreateMeeting"
	MeetingsGetMeetingActivity    = "/saastack.meetings.v1.Meetings/GetMeeting"
	MeetingsUpdateMeetingActivity = "/saastack.meetings.v1.Meetings/UpdateMeeting"
	MeetingsDeleteMeetingActivity = "/saastack.meetings.v1.Meetings/DeleteMeeting"
)

func RegisterMeetingsActivities(cli MeetingsClient) {
	activity.RegisterWithOptions(func(ctx context.Context, in *ListMeetingsRequest) (*ListMeetingResponse, error) {
		return cli.ListMeeting(ctx, in)
	}, activity.RegisterOptions{Name: MeetingsListMeetingActivity})
	activity.RegisterWithOptions(func(ctx context.Context, in *CreateMeetingRequest) (*Meeting, error) {
		return cli.CreateMeeting(ctx, in)
	}, activity.RegisterOptions{Name: MeetingsCreateMeetingActivity})
	activity.RegisterWithOptions(func(ctx context.Context, in *GetMeetingRequest) (*Meeting, error) { return cli.GetMeeting(ctx, in) }, activity.RegisterOptions{Name: MeetingsGetMeetingActivity})
	activity.RegisterWithOptions(func(ctx context.Context, in *UpdateMeetingRequest) (*Meeting, error) {
		return cli.UpdateMeeting(ctx, in)
	}, activity.RegisterOptions{Name: MeetingsUpdateMeetingActivity})
	activity.RegisterWithOptions(func(ctx context.Context, in *DeleteMeetingRequest) (*Empty, error) {
		return cli.DeleteMeeting(ctx, in)
	}, activity.RegisterOptions{Name: MeetingsDeleteMeetingActivity})

}

// MeetingsActivitiesClient is a typesafe wrapper for MeetingsActivities.
type MeetingsActivitiesClient struct {
}

// NewMeetingsActivitiesClient creates a new MeetingsActivitiesClient.
func NewMeetingsActivitiesClient(cli MeetingsClient) MeetingsActivitiesClient {
	RegisterMeetingsActivities(cli)
	return MeetingsActivitiesClient{}
}

func (ca *MeetingsActivitiesClient) ListMeeting(ctx workflow.Context, in *ListMeetingsRequest) (*ListMeetingResponse, error) {
	future := workflow.ExecuteActivity(ctx, MeetingsListMeetingActivity, in)

	var result ListMeetingResponse
	if err := future.Get(ctx, &result); err != nil {
		return nil, err
	}

	return &result, nil
}

func (ca *MeetingsActivitiesClient) CreateMeeting(ctx workflow.Context, in *CreateMeetingRequest) (*Meeting, error) {
	future := workflow.ExecuteActivity(ctx, MeetingsCreateMeetingActivity, in)

	var result Meeting
	if err := future.Get(ctx, &result); err != nil {
		return nil, err
	}

	return &result, nil
}

func (ca *MeetingsActivitiesClient) GetMeeting(ctx workflow.Context, in *GetMeetingRequest) (*Meeting, error) {
	future := workflow.ExecuteActivity(ctx, MeetingsGetMeetingActivity, in)

	var result Meeting
	if err := future.Get(ctx, &result); err != nil {
		return nil, err
	}

	return &result, nil
}

func (ca *MeetingsActivitiesClient) UpdateMeeting(ctx workflow.Context, in *UpdateMeetingRequest) (*Meeting, error) {
	future := workflow.ExecuteActivity(ctx, MeetingsUpdateMeetingActivity, in)

	var result Meeting
	if err := future.Get(ctx, &result); err != nil {
		return nil, err
	}

	return &result, nil
}

func (ca *MeetingsActivitiesClient) DeleteMeeting(ctx workflow.Context, in *DeleteMeetingRequest) (*Empty, error) {
	future := workflow.ExecuteActivity(ctx, MeetingsDeleteMeetingActivity, in)

	var result Empty
	if err := future.Get(ctx, &result); err != nil {
		return nil, err
	}

	return &result, nil
}

const (
	ParentServiceValidateParentActivity = "/saastack.meetings.v1.ParentService/ValidateParent"
)

func RegisterParentServiceActivities(cli ParentServiceClient) {
	activity.RegisterWithOptions(func(ctx context.Context, in *ValidateParentRequest) (*ValidateParentResponse, error) {
		return cli.ValidateParent(ctx, in)
	}, activity.RegisterOptions{Name: ParentServiceValidateParentActivity})

}

// ParentServiceActivitiesClient is a typesafe wrapper for ParentServiceActivities.
type ParentServiceActivitiesClient struct {
}

// NewParentServiceActivitiesClient creates a new ParentServiceActivitiesClient.
func NewParentServiceActivitiesClient(cli ParentServiceClient) ParentServiceActivitiesClient {
	RegisterParentServiceActivities(cli)
	return ParentServiceActivitiesClient{}
}

func (ca *ParentServiceActivitiesClient) ValidateParent(ctx workflow.Context, in *ValidateParentRequest) (*ValidateParentResponse, error) {
	future := workflow.ExecuteActivity(ctx, ParentServiceValidateParentActivity, in)

	var result ValidateParentResponse
	if err := future.Get(ctx, &result); err != nil {
		return nil, err
	}

	return &result, nil
}
