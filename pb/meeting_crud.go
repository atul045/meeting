package pb

import (
	"context"

	"github.com/golang/protobuf/ptypes/empty"
	"go.saastack.io/chaku/errors"
	"go.saastack.io/idutil"
	"google.golang.org/genproto/protobuf/field_mask"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type MeetingsServiceMeetingServerCrud struct {
	store MeetingStore
	bloc  MeetingsServiceMeetingServerBLoC
}

type MeetingsServiceMeetingServerBLoC interface {
	CreateMeetingBLoC(context.Context, *CreateMeetingRequest) error

	GetMeetingBLoC(context.Context, *GetMeetingRequest) error

	UpdateMeetingBLoC(context.Context, *UpdateMeetingRequest) error

	DeleteMeetingBLoC(context.Context, *DeleteMeetingRequest) error
}

func NewMeetingsServiceMeetingServerCrud(s MeetingStore, b MeetingsServiceMeetingServerBLoC) *MeetingsServiceMeetingServerCrud {
	return &MeetingsServiceMeetingServerCrud{store: s, bloc: b}
}

func (s *MeetingsServiceMeetingServerCrud) CreateMeeting(ctx context.Context, in *CreateMeetingRequest) (*Meeting, error) {

	if err := in.Validate(); err != nil {
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	err := s.bloc.CreateMeetingBLoC(ctx, in)
	if err != nil {
		return nil, err
	}

	if idutil.GetPrefix(in.Meeting.Id) != in.Meeting.GetPrefix() {
		in.Meeting.Id = in.Parent
	}

	ids, err := s.store.CreateMeetings(ctx, in.Meeting)
	if err != nil {
		return nil, err
	}

	in.Meeting.Id = ids[0]

	return in.GetMeeting(), nil
}

func (s *MeetingsServiceMeetingServerCrud) UpdateMeeting(ctx context.Context, in *UpdateMeetingRequest) (*Meeting, error) {

	mask := s.GetViewMask(in.UpdateMask)
	if len(mask) == 0 {
		return nil, status.Error(codes.InvalidArgument, "cannot send empty update mask")
	}

	if err := in.GetMeeting().Validate(mask...); err != nil {
		return nil, err
	}

	err := s.bloc.UpdateMeetingBLoC(ctx, in)
	if err != nil {
		return nil, err
	}

	if err := s.store.UpdateMeeting(ctx,
		in.Meeting, mask,
		MeetingIdEq{Id: in.Meeting.Id},
	); err != nil {
		return nil, err
	}

	updatedMeeting, err := s.store.GetMeeting(ctx, []string{},
		MeetingIdEq{
			Id: in.GetMeeting().GetId(),
		},
	)
	if err != nil {
		return nil, err
	}

	return updatedMeeting, nil
}

func (s *MeetingsServiceMeetingServerCrud) GetMeeting(ctx context.Context, in *GetMeetingRequest) (*Meeting, error) {
	if err := in.Validate(); err != nil {
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	err := s.bloc.GetMeetingBLoC(ctx, in)
	if err != nil {
		return nil, err
	}

	mask := s.GetViewMask(in.ViewMask)

	res, err := s.store.GetMeeting(ctx, mask, MeetingIdEq{Id: in.Id})
	if err != nil {
		if err == errors.ErrNotFound {
			return nil, status.Error(codes.NotFound, "Meeting not found")
		}
		return nil, err
	}

	return res, nil
}

func (s *MeetingsServiceMeetingServerCrud) DeleteMeeting(ctx context.Context, in *DeleteMeetingRequest) (*empty.Empty, error) {
	if err := in.Validate(); err != nil {
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	err := s.bloc.DeleteMeetingBLoC(ctx, in)
	if err != nil {
		return nil, err
	}

	if err := s.store.DeleteMeeting(ctx, MeetingIdEq{Id: in.Id}); err != nil {
		return nil, err
	}

	return &empty.Empty{}, nil
}

func (s *MeetingsServiceMeetingServerCrud) GetViewMask(mask *field_mask.FieldMask) []string {
	if mask == nil || mask.GetPaths() == nil {
		return []string{}
	}
	return mask.GetPaths()
}
