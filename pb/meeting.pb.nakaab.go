// Code generated by protoc-gen-nakaab, DO NOT EDIT.

package pb

import (
	"go.saastack.io/protoc-gen-nakaab/nakaab"

	"go.saastack.io/protoc-gen-nakaab/nakaab/google"
)

func ListMeetingsRequestFields(ff ...IsListMeetingsRequestField) []string {
	var uu []nakaab.Unwrapper
	for _, f := range ff {
		uu = append(uu, f)
	}
	return nakaab.MakeFieldMask(uu...)
}

func ListMeetingsRequestAllFields() []string {

	return []string{
		// primitive fields
		"user_id",
		// embedded fields
	}
}

type IsListMeetingsRequestField interface {
	nakaab.Unwrapper
	isListMeetingsRequestField()
}

type listMeetingsRequestPrimitiveField string

const (
	ListMeetingsRequest_UserId listMeetingsRequestPrimitiveField = "user_id"
)

func (s listMeetingsRequestPrimitiveField) isListMeetingsRequestField() {}

func (s listMeetingsRequestPrimitiveField) Unwrap() (string, []nakaab.Unwrapper) {
	return string(s), nil
}

func ListMeetingResponseFields(ff ...IsListMeetingResponseField) []string {
	var uu []nakaab.Unwrapper
	for _, f := range ff {
		uu = append(uu, f)
	}
	return nakaab.MakeFieldMask(uu...)
}

func ListMeetingResponseAllFields() []string {

	return []string{
		// primitive fields
		// embedded fields
		"meetings",
	}
}

type IsListMeetingResponseField interface {
	nakaab.Unwrapper
	isListMeetingResponseField()
}

type listMeetingResponsePrimitiveField string

const ()

func (s listMeetingResponsePrimitiveField) isListMeetingResponseField() {}

func (s listMeetingResponsePrimitiveField) Unwrap() (string, []nakaab.Unwrapper) {
	return string(s), nil
}

type ListMeetingResponse_Meetings []IsMeetingField

func (l ListMeetingResponse_Meetings) Unwrap() (string, []nakaab.Unwrapper) {
	us := make([]nakaab.Unwrapper, len(l))
	for i := range l {
		us[i] = l[i]
	}
	return "meetings", us
}

func (ListMeetingResponse_Meetings) isListMeetingResponseField() {}

func GetMeetingRequestFields(ff ...IsGetMeetingRequestField) []string {
	var uu []nakaab.Unwrapper
	for _, f := range ff {
		uu = append(uu, f)
	}
	return nakaab.MakeFieldMask(uu...)
}

func GetMeetingRequestAllFields() []string {

	return []string{
		// primitive fields
		"id",
		// embedded fields
		"view_mask",
	}
}

type IsGetMeetingRequestField interface {
	nakaab.Unwrapper
	isGetMeetingRequestField()
}

type getMeetingRequestPrimitiveField string

const (
	GetMeetingRequest_Id getMeetingRequestPrimitiveField = "id"
)

func (s getMeetingRequestPrimitiveField) isGetMeetingRequestField() {}

func (s getMeetingRequestPrimitiveField) Unwrap() (string, []nakaab.Unwrapper) {
	return string(s), nil
}

type GetMeetingRequest_ViewMask []google.IsFieldMaskField

func (l GetMeetingRequest_ViewMask) Unwrap() (string, []nakaab.Unwrapper) {
	us := make([]nakaab.Unwrapper, len(l))
	for i := range l {
		us[i] = l[i]
	}
	return "view_mask", us
}

func (GetMeetingRequest_ViewMask) isGetMeetingRequestField() {}

func MeetingFields(ff ...IsMeetingField) []string {
	var uu []nakaab.Unwrapper
	for _, f := range ff {
		uu = append(uu, f)
	}
	return nakaab.MakeFieldMask(uu...)
}

func MeetingAllFields() []string {

	return []string{
		// primitive fields
		"id",
		"topic",
		"description",
		"meeting_type",
		"meeting_app_type",
		"duration",
		"timezone",
		"password",
		"start_url",
		"join_url",
		"meeting_id",
		"user_id",
		"recurrence_rule",
		"recurrence_rule_id",
		"error_message",
		// embedded fields
		"start_time",
		"end_time",
		"participant_list",
	}
}

type IsMeetingField interface {
	nakaab.Unwrapper
	isMeetingField()
}

type meetingPrimitiveField string

const (
	Meeting_Id meetingPrimitiveField = "id"

	Meeting_Topic meetingPrimitiveField = "topic"

	Meeting_Description meetingPrimitiveField = "description"

	Meeting_MeetingType meetingPrimitiveField = "meeting_type"

	Meeting_MeetingAppType meetingPrimitiveField = "meeting_app_type"

	Meeting_Duration meetingPrimitiveField = "duration"

	Meeting_Timezone meetingPrimitiveField = "timezone"

	Meeting_Password meetingPrimitiveField = "password"

	Meeting_StartUrl meetingPrimitiveField = "start_url"

	Meeting_JoinUrl meetingPrimitiveField = "join_url"

	Meeting_MeetingId meetingPrimitiveField = "meeting_id"

	Meeting_UserId meetingPrimitiveField = "user_id"

	Meeting_RecurrenceRule meetingPrimitiveField = "recurrence_rule"

	Meeting_RecurrenceRuleId meetingPrimitiveField = "recurrence_rule_id"

	Meeting_ErrorMessage meetingPrimitiveField = "error_message"
)

func (s meetingPrimitiveField) isMeetingField() {}

func (s meetingPrimitiveField) Unwrap() (string, []nakaab.Unwrapper) {
	return string(s), nil
}

type Meeting_StartTime []google.IsTimestampField

func (l Meeting_StartTime) Unwrap() (string, []nakaab.Unwrapper) {
	us := make([]nakaab.Unwrapper, len(l))
	for i := range l {
		us[i] = l[i]
	}
	return "start_time", us
}

func (Meeting_StartTime) isMeetingField() {}

type Meeting_EndTime []google.IsTimestampField

func (l Meeting_EndTime) Unwrap() (string, []nakaab.Unwrapper) {
	us := make([]nakaab.Unwrapper, len(l))
	for i := range l {
		us[i] = l[i]
	}
	return "end_time", us
}

func (Meeting_EndTime) isMeetingField() {}

type Meeting_ParticipantList []IsParticipantListField

func (l Meeting_ParticipantList) Unwrap() (string, []nakaab.Unwrapper) {
	us := make([]nakaab.Unwrapper, len(l))
	for i := range l {
		us[i] = l[i]
	}
	return "participant_list", us
}

func (Meeting_ParticipantList) isMeetingField() {}

func ParticipantListFields(ff ...IsParticipantListField) []string {
	var uu []nakaab.Unwrapper
	for _, f := range ff {
		uu = append(uu, f)
	}
	return nakaab.MakeFieldMask(uu...)
}

func ParticipantListAllFields() []string {

	return []string{
		// primitive fields
		"id",
		"staff_id",
		"participant_id",
		"first_name",
		"email",
		"last_name",
		"participant_join_url",
		// embedded fields
	}
}

type IsParticipantListField interface {
	nakaab.Unwrapper
	isParticipantListField()
}

type participantListPrimitiveField string

const (
	ParticipantList_Id participantListPrimitiveField = "id"

	ParticipantList_StaffId participantListPrimitiveField = "staff_id"

	ParticipantList_ParticipantId participantListPrimitiveField = "participant_id"

	ParticipantList_FirstName participantListPrimitiveField = "first_name"

	ParticipantList_Email participantListPrimitiveField = "email"

	ParticipantList_LastName participantListPrimitiveField = "last_name"

	ParticipantList_ParticipantJoinUrl participantListPrimitiveField = "participant_join_url"
)

func (s participantListPrimitiveField) isParticipantListField() {}

func (s participantListPrimitiveField) Unwrap() (string, []nakaab.Unwrapper) {
	return string(s), nil
}

func UpdateMeetingRequestFields(ff ...IsUpdateMeetingRequestField) []string {
	var uu []nakaab.Unwrapper
	for _, f := range ff {
		uu = append(uu, f)
	}
	return nakaab.MakeFieldMask(uu...)
}

func UpdateMeetingRequestAllFields() []string {

	return []string{
		// primitive fields
		// embedded fields
		"meeting",
		"update_mask",
	}
}

type IsUpdateMeetingRequestField interface {
	nakaab.Unwrapper
	isUpdateMeetingRequestField()
}

type updateMeetingRequestPrimitiveField string

const ()

func (s updateMeetingRequestPrimitiveField) isUpdateMeetingRequestField() {}

func (s updateMeetingRequestPrimitiveField) Unwrap() (string, []nakaab.Unwrapper) {
	return string(s), nil
}

type UpdateMeetingRequest_Meeting []IsMeetingField

func (l UpdateMeetingRequest_Meeting) Unwrap() (string, []nakaab.Unwrapper) {
	us := make([]nakaab.Unwrapper, len(l))
	for i := range l {
		us[i] = l[i]
	}
	return "meeting", us
}

func (UpdateMeetingRequest_Meeting) isUpdateMeetingRequestField() {}

type UpdateMeetingRequest_UpdateMask []google.IsFieldMaskField

func (l UpdateMeetingRequest_UpdateMask) Unwrap() (string, []nakaab.Unwrapper) {
	us := make([]nakaab.Unwrapper, len(l))
	for i := range l {
		us[i] = l[i]
	}
	return "update_mask", us
}

func (UpdateMeetingRequest_UpdateMask) isUpdateMeetingRequestField() {}

func CreateMeetingRequestFields(ff ...IsCreateMeetingRequestField) []string {
	var uu []nakaab.Unwrapper
	for _, f := range ff {
		uu = append(uu, f)
	}
	return nakaab.MakeFieldMask(uu...)
}

func CreateMeetingRequestAllFields() []string {

	return []string{
		// primitive fields
		"parent",
		// embedded fields
		"meeting",
	}
}

type IsCreateMeetingRequestField interface {
	nakaab.Unwrapper
	isCreateMeetingRequestField()
}

type createMeetingRequestPrimitiveField string

const (
	CreateMeetingRequest_Parent createMeetingRequestPrimitiveField = "parent"
)

func (s createMeetingRequestPrimitiveField) isCreateMeetingRequestField() {}

func (s createMeetingRequestPrimitiveField) Unwrap() (string, []nakaab.Unwrapper) {
	return string(s), nil
}

type CreateMeetingRequest_Meeting []IsMeetingField

func (l CreateMeetingRequest_Meeting) Unwrap() (string, []nakaab.Unwrapper) {
	us := make([]nakaab.Unwrapper, len(l))
	for i := range l {
		us[i] = l[i]
	}
	return "meeting", us
}

func (CreateMeetingRequest_Meeting) isCreateMeetingRequestField() {}

func DeleteMeetingRequestFields(ff ...IsDeleteMeetingRequestField) []string {
	var uu []nakaab.Unwrapper
	for _, f := range ff {
		uu = append(uu, f)
	}
	return nakaab.MakeFieldMask(uu...)
}

func DeleteMeetingRequestAllFields() []string {

	return []string{
		// primitive fields
		"id",
		// embedded fields
	}
}

type IsDeleteMeetingRequestField interface {
	nakaab.Unwrapper
	isDeleteMeetingRequestField()
}

type deleteMeetingRequestPrimitiveField string

const (
	DeleteMeetingRequest_Id deleteMeetingRequestPrimitiveField = "id"
)

func (s deleteMeetingRequestPrimitiveField) isDeleteMeetingRequestField() {}

func (s deleteMeetingRequestPrimitiveField) Unwrap() (string, []nakaab.Unwrapper) {
	return string(s), nil
}

func ValidateParentRequestFields(ff ...IsValidateParentRequestField) []string {
	var uu []nakaab.Unwrapper
	for _, f := range ff {
		uu = append(uu, f)
	}
	return nakaab.MakeFieldMask(uu...)
}

func ValidateParentRequestAllFields() []string {

	return []string{
		// primitive fields
		"id",
		// embedded fields
	}
}

type IsValidateParentRequestField interface {
	nakaab.Unwrapper
	isValidateParentRequestField()
}

type validateParentRequestPrimitiveField string

const (
	ValidateParentRequest_Id validateParentRequestPrimitiveField = "id"
)

func (s validateParentRequestPrimitiveField) isValidateParentRequestField() {}

func (s validateParentRequestPrimitiveField) Unwrap() (string, []nakaab.Unwrapper) {
	return string(s), nil
}

func ValidateParentResponseFields(ff ...IsValidateParentResponseField) []string {
	var uu []nakaab.Unwrapper
	for _, f := range ff {
		uu = append(uu, f)
	}
	return nakaab.MakeFieldMask(uu...)
}

func ValidateParentResponseAllFields() []string {

	return []string{
		// primitive fields
		"valid",
		// embedded fields
	}
}

type IsValidateParentResponseField interface {
	nakaab.Unwrapper
	isValidateParentResponseField()
}

type validateParentResponsePrimitiveField string

const (
	ValidateParentResponse_Valid validateParentResponsePrimitiveField = "valid"
)

func (s validateParentResponsePrimitiveField) isValidateParentResponseField() {}

func (s validateParentResponsePrimitiveField) Unwrap() (string, []nakaab.Unwrapper) {
	return string(s), nil
}
