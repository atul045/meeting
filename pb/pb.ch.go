package pb

import (
	context "context"
	x "database/sql"

	sqrl "github.com/elgris/sqrl"
	timestamp "github.com/golang/protobuf/ptypes/timestamp"
	chaku_globals "go.saastack.io/chaku/chaku-globals"
	driver "go.saastack.io/chaku/driver"
	sql "go.saastack.io/chaku/driver/pgsql"
	errors "go.saastack.io/chaku/errors"
)

var objectTableMap = chaku_globals.ObjectTable{
	"meeting": {
		"id":                 "meeting",
		"topic":              "meeting",
		"description":        "meeting",
		"meeting_type":       "meeting",
		"meeting_app_type":   "meeting",
		"start_time":         "meeting",
		"end_time":           "meeting",
		"duration":           "meeting",
		"timezone":           "meeting",
		"password":           "meeting",
		"start_url":          "meeting",
		"join_url":           "meeting",
		"meeting_id":         "meeting",
		"user_id":            "meeting",
		"recurrence_rule":    "meeting",
		"recurrence_rule_id": "meeting",
		"error_message":      "meeting",
		"participant_list":   "participant_list",
	},
	"participant_list": {
		"id":                   "participant_list",
		"staff_id":             "participant_list",
		"participant_id":       "participant_list",
		"first_name":           "participant_list",
		"email":                "participant_list",
		"last_name":            "participant_list",
		"participant_join_url": "participant_list",
	},
}

func (m *Meeting) PackageName() string {
	return "saastack_meetings_v1"
}

func (m *Meeting) TableOfObject(f, s string) string {
	return objectTableMap[f][s]
}

func (m *Meeting) GetDescriptorsOf(f string) (driver.Descriptor, error) {
	switch f {
	case "participant_list":
		if len(m.ParticipantList) == 0 {
			m.ParticipantList = append(m.ParticipantList, &ParticipantList{})
		}
		return m.ParticipantList[0], nil
	default:
		return nil, errors.ErrInvalidField
	}
}

func (m *Meeting) ObjectName() string {
	return "meeting"
}

func (m *Meeting) Fields() []string {
	return []string{
		"id", "topic", "description", "meeting_type", "meeting_app_type", "start_time", "end_time", "duration", "timezone", "password", "start_url", "join_url", "meeting_id", "user_id", "recurrence_rule", "recurrence_rule_id", "error_message", "participant_list",
	}
}

func (m *Meeting) IsObject(field string) bool {
	switch field {
	case "participant_list":
		return true
	default:
		return false
	}
}

func (m *Meeting) ValuerSlice(field string) ([]driver.Descriptor, error) {
	if m == nil {
		return nil, nil
	}
	switch field {
	case "participant_list":
		sli := make([]driver.Descriptor, 0, len(m.ParticipantList))
		for _, c := range m.ParticipantList {
			sli = append(sli, c)
		}
		return sli, nil
	default:
		return []driver.Descriptor{}, errors.ErrInvalidField
	}
}

func (m *Meeting) Valuer(field string) (interface{}, error) {
	if m == nil {
		return nil, nil
	}
	switch field {
	case "id":
		return m.Id, nil
	case "topic":
		return m.Topic, nil
	case "description":
		return m.Description, nil
	case "meeting_type":
		return m.MeetingType, nil
	case "meeting_app_type":
		return m.MeetingAppType, nil
	case "start_time":
		return m.StartTime, nil
	case "end_time":
		return m.EndTime, nil
	case "duration":
		return m.Duration, nil
	case "timezone":
		return m.Timezone, nil
	case "password":
		return m.Password, nil
	case "start_url":
		return m.StartUrl, nil
	case "join_url":
		return m.JoinUrl, nil
	case "meeting_id":
		return m.MeetingId, nil
	case "user_id":
		return m.UserId, nil
	case "recurrence_rule":
		return m.RecurrenceRule, nil
	case "recurrence_rule_id":
		return m.RecurrenceRuleId, nil
	case "error_message":
		return m.ErrorMessage, nil
	case "participant_list":
		return m.ParticipantList, nil
	default:
		return nil, errors.ErrInvalidField
	}
}

func (m *Meeting) Addresser(field string) (interface{}, error) {
	if m == nil {
		return nil, nil
	}
	switch field {
	case "id":
		return &m.Id, nil
	case "topic":
		return &m.Topic, nil
	case "description":
		return &m.Description, nil
	case "meeting_type":
		return &m.MeetingType, nil
	case "meeting_app_type":
		return &m.MeetingAppType, nil
	case "start_time":
		return &m.StartTime, nil
	case "end_time":
		return &m.EndTime, nil
	case "duration":
		return &m.Duration, nil
	case "timezone":
		return &m.Timezone, nil
	case "password":
		return &m.Password, nil
	case "start_url":
		return &m.StartUrl, nil
	case "join_url":
		return &m.JoinUrl, nil
	case "meeting_id":
		return &m.MeetingId, nil
	case "user_id":
		return &m.UserId, nil
	case "recurrence_rule":
		return &m.RecurrenceRule, nil
	case "recurrence_rule_id":
		return &m.RecurrenceRuleId, nil
	case "error_message":
		return &m.ErrorMessage, nil
	case "participant_list":
		return &m.ParticipantList, nil
	default:
		return nil, errors.ErrInvalidField
	}
}

func (m *Meeting) New(field string) error {
	switch field {
	case "id":
		return nil
	case "topic":
		return nil
	case "description":
		return nil
	case "meeting_type":
		return nil
	case "meeting_app_type":
		return nil
	case "start_time":
		if m.StartTime == nil {
			m.StartTime = &timestamp.Timestamp{}
		}
		return nil
	case "end_time":
		if m.EndTime == nil {
			m.EndTime = &timestamp.Timestamp{}
		}
		return nil
	case "duration":
		return nil
	case "timezone":
		return nil
	case "password":
		return nil
	case "start_url":
		return nil
	case "join_url":
		return nil
	case "meeting_id":
		return nil
	case "user_id":
		return nil
	case "recurrence_rule":
		return nil
	case "recurrence_rule_id":
		if m.RecurrenceRuleId == nil {
			m.RecurrenceRuleId = make([]string, 0)
		}
		return nil
	case "error_message":
		return nil
	case "participant_list":
		if m.ParticipantList == nil {
			m.ParticipantList = make([]*ParticipantList, 0)
		}
		return nil
	default:
		return errors.ErrInvalidField
	}
}

func (m *Meeting) Type(field string) string {
	switch field {
	case "id":
		return "string"
	case "topic":
		return "string"
	case "description":
		return "string"
	case "meeting_type":
		return "enum"
	case "meeting_app_type":
		return "enum"
	case "start_time":
		return "timestamp"
	case "end_time":
		return "timestamp"
	case "duration":
		return "int64"
	case "timezone":
		return "string"
	case "password":
		return "string"
	case "start_url":
		return "string"
	case "join_url":
		return "string"
	case "meeting_id":
		return "string"
	case "user_id":
		return "string"
	case "recurrence_rule":
		return "string"
	case "recurrence_rule_id":
		return "repeated"
	case "error_message":
		return "string"
	case "participant_list":
		return "repeated"
	default:
		return ""
	}
}

func (_ *Meeting) GetEmptyObject() (m *Meeting) {
	m = &Meeting{}
	_ = m.New("participantList")
	m.ParticipantList = append(m.ParticipantList, &ParticipantList{})
	m.ParticipantList[0].GetEmptyObject()
	return
}

func (m *Meeting) GetPrefix() string {
	return "meet"
}

func (m *Meeting) GetID() string {
	return m.Id
}

func (m *Meeting) SetID(id string) {
	m.Id = id
}

func (m *Meeting) IsRoot() bool {
	return true
}

func (m *Meeting) IsFlatObject(f string) bool {
	return false
}

func (m *ParticipantList) GetDescriptorsOf(f string) (driver.Descriptor, error) {
	switch f {
	default:
		return nil, errors.ErrInvalidField
	}
}

func (m *ParticipantList) ObjectName() string {
	return "participant_list"
}

func (m *ParticipantList) Fields() []string {
	return []string{
		"id", "staff_id", "participant_id", "first_name", "email", "last_name", "participant_join_url",
	}
}

func (m *ParticipantList) IsObject(field string) bool {
	switch field {
	default:
		return false
	}
}

func (m *ParticipantList) ValuerSlice(field string) ([]driver.Descriptor, error) {
	if m == nil {
		return nil, nil
	}
	switch field {
	default:
		return []driver.Descriptor{}, errors.ErrInvalidField
	}
}

func (m *ParticipantList) Valuer(field string) (interface{}, error) {
	if m == nil {
		return nil, nil
	}
	switch field {
	case "id":
		return m.Id, nil
	case "staff_id":
		return m.StaffId, nil
	case "participant_id":
		return m.ParticipantId, nil
	case "first_name":
		return m.FirstName, nil
	case "email":
		return m.Email, nil
	case "last_name":
		return m.LastName, nil
	case "participant_join_url":
		return m.ParticipantJoinUrl, nil
	default:
		return nil, errors.ErrInvalidField
	}
}

func (m *ParticipantList) Addresser(field string) (interface{}, error) {
	if m == nil {
		return nil, nil
	}
	switch field {
	case "id":
		return &m.Id, nil
	case "staff_id":
		return &m.StaffId, nil
	case "participant_id":
		return &m.ParticipantId, nil
	case "first_name":
		return &m.FirstName, nil
	case "email":
		return &m.Email, nil
	case "last_name":
		return &m.LastName, nil
	case "participant_join_url":
		return &m.ParticipantJoinUrl, nil
	default:
		return nil, errors.ErrInvalidField
	}
}

func (m *ParticipantList) New(field string) error {
	switch field {
	case "id":
		return nil
	case "staff_id":
		return nil
	case "participant_id":
		return nil
	case "first_name":
		return nil
	case "email":
		return nil
	case "last_name":
		return nil
	case "participant_join_url":
		return nil
	default:
		return errors.ErrInvalidField
	}
}

func (m *ParticipantList) Type(field string) string {
	switch field {
	case "id":
		return "string"
	case "staff_id":
		return "string"
	case "participant_id":
		return "string"
	case "first_name":
		return "string"
	case "email":
		return "string"
	case "last_name":
		return "string"
	case "participant_join_url":
		return "string"
	default:
		return ""
	}
}

func (_ *ParticipantList) GetEmptyObject() (m *ParticipantList) {
	m = &ParticipantList{}
	return
}

func (m *ParticipantList) GetPrefix() string {
	return ""
}

func (m *ParticipantList) GetID() string {
	return m.Id
}

func (m *ParticipantList) SetID(id string) {
	m.Id = id
}

func (m *ParticipantList) IsRoot() bool {
	return false
}

func (m *ParticipantList) IsFlatObject(f string) bool {
	return false
}

func (m *Meeting) NoOfParents(d driver.Descriptor) int {
	switch d.ObjectName() {
	case "participant_list":
		return 1
	}
	return 0
}

type MeetingStore struct {
	d      driver.Driver
	withTx bool
	tx     driver.Transaction
}

func (s MeetingStore) Execute(ctx context.Context, query string, args ...interface{}) error {
	if s.withTx {
		return s.tx.Execute(ctx, query, args...)
	}
	return s.d.Execute(ctx, query, args...)
}

func (s MeetingStore) QueryRows(ctx context.Context, query string, scanners []string, args ...interface{}) (driver.Result, error) {
	if s.withTx {
		return s.tx.QueryRows(ctx, query, scanners, args...)
	}
	return s.d.QueryRows(ctx, query, scanners, args...)
}

func NewMeetingStore(d driver.Driver) MeetingStore {
	return MeetingStore{d: d}
}

func NewPostgresMeetingStore(db *x.DB, usr driver.IUserInfo) MeetingStore {
	return MeetingStore{
		d: &sql.Sql{DB: db, UserInfo: usr, Placeholder: sqrl.Dollar},
	}
}

type MeetingTx struct {
	MeetingStore
}

func (s MeetingStore) BeginTx(ctx context.Context) (*MeetingTx, error) {
	tx, err := s.d.BeginTx(ctx)
	if err != nil {
		return nil, err
	}
	return &MeetingTx{
		MeetingStore: MeetingStore{
			d:      s.d,
			withTx: true,
			tx:     tx,
		},
	}, nil
}

func (tx *MeetingTx) Commit(ctx context.Context) error {
	return tx.tx.Commit(ctx)
}

func (tx *MeetingTx) RollBack(ctx context.Context) error {
	return tx.tx.RollBack(ctx)
}

func (s MeetingStore) CreateMeetingPGStore(ctx context.Context) error {
	const queries = `
CREATE SCHEMA IF NOT EXISTS saastack_meetings_v1;
CREATE TABLE IF NOT EXISTS  saastack_meetings_v1.meeting( id text DEFAULT ''::text , topic text DEFAULT ''::text , description text DEFAULT ''::text , meeting_type integer DEFAULT 0 , meeting_app_type integer DEFAULT 0 , start_time timestamp without time zone DEFAULT '1970-01-01 00:00:00'::timestamp without time zone , end_time timestamp without time zone DEFAULT '1970-01-01 00:00:00'::timestamp without time zone , duration integer DEFAULT 0 , timezone text DEFAULT ''::text , password text DEFAULT ''::text , start_url text DEFAULT ''::text , join_url text DEFAULT ''::text , meeting_id text DEFAULT ''::text , user_id text DEFAULT ''::text , recurrence_rule text DEFAULT ''::text , recurrence_rule_id text[] DEFAULT '{}'::text[] , error_message text DEFAULT ''::text , parent text DEFAULT ''::text , is_deleted boolean DEFAULT false, deleted_by text DEFAULT ''::text, deleted_on timestamp without time zone DEFAULT '1970-01-01 00:00:00'::timestamp without time zone, updated_by text DEFAULT ''::text, updated_on timestamp without time zone DEFAULT '1970-01-01 00:00:00'::timestamp without time zone, created_by text DEFAULT ''::text, created_on timestamp without time zone DEFAULT '1970-01-01 00:00:00'::timestamp without time zone, field_variable_mask text DEFAULT ''::text, PRIMARY KEY (id, parent)); 
CREATE TABLE IF NOT EXISTS  saastack_meetings_v1.participant_list( id text DEFAULT ''::text , staff_id text DEFAULT ''::text , participant_id text DEFAULT ''::text , first_name text DEFAULT ''::text , email text DEFAULT ''::text , last_name text DEFAULT ''::text , participant_join_url text DEFAULT ''::text , p0id text DEFAULT ''::text , is_deleted boolean DEFAULT false, deleted_by text DEFAULT ''::text, deleted_on timestamp without time zone DEFAULT '1970-01-01 00:00:00'::timestamp without time zone, updated_by text DEFAULT ''::text, updated_on timestamp without time zone DEFAULT '1970-01-01 00:00:00'::timestamp without time zone, created_by text DEFAULT ''::text, created_on timestamp without time zone DEFAULT '1970-01-01 00:00:00'::timestamp without time zone, field_variable_mask text DEFAULT ''::text, PRIMARY KEY (id, p0id)); 
CREATE TABLE IF NOT EXISTS  saastack_meetings_v1.meeting_parent( id text DEFAULT ''::text , parent text DEFAULT ''::text ); 
`
	if err := s.d.Execute(ctx, queries); err != nil {
		return err
	}
	return nil
}

func (s MeetingStore) CreateMeetings(ctx context.Context, list ...*Meeting) ([]string, error) {
	vv := make([]driver.Descriptor, len(list))
	for i := range list {
		vv[i] = list[i]
	}
	if s.withTx {
		return s.tx.Insert(ctx, vv, &Meeting{}, &Meeting{}, "", []string{})
	}
	return s.d.Insert(ctx, vv, &Meeting{}, &Meeting{}, "", []string{})
}

func (s MeetingStore) DeleteMeeting(ctx context.Context, cond MeetingCondition) error {
	if s.withTx {
		return s.tx.Delete(ctx, cond.meetingCondToDriverMeetingCond(s.d), &Meeting{}, &Meeting{})
	}
	return s.d.Delete(ctx, cond.meetingCondToDriverMeetingCond(s.d), &Meeting{}, &Meeting{})
}

func (s MeetingStore) UpdateMeeting(ctx context.Context, req *Meeting, fields []string, cond MeetingCondition) error {
	if s.withTx {
		return s.tx.Update(ctx, cond.meetingCondToDriverMeetingCond(s.d), req, &Meeting{}, fields...)
	}
	return s.d.Update(ctx, cond.meetingCondToDriverMeetingCond(s.d), req, &Meeting{}, fields...)
}

func (s MeetingStore) GetMeeting(ctx context.Context, fields []string, cond MeetingCondition, opt ...getMeetingsOption) (*Meeting, error) {
	if len(fields) == 0 {
		fields = (&Meeting{}).Fields()
	}
	m := MetaInfoForList{}
	listOpts := []listMeetingsOption{
		&CursorBasedPagination{Limit: 1},
	}
	for _, o := range opt {
		t, _ := o.getValue()
		switch t {
		case driver.OptionType_MetaInfo:
			listOpts = append(listOpts, &m)
		}
	}
	objList, err := s.ListMeetings(ctx, fields, cond, listOpts...)
	if len(objList) == 0 && err == nil {
		err = errors.ErrNotFound
	}
	if err != nil {
		return nil, err
	}
	for _, o := range opt {
		t, in := o.getValue()
		switch t {
		case driver.OptionType_MetaInfo:
			in.(*MetaInfo).UpdatedBy = m[0].UpdatedBy
			in.(*MetaInfo).CreatedBy = m[0].CreatedBy
			in.(*MetaInfo).DeletedBy = m[0].DeletedBy
			in.(*MetaInfo).UpdatedOn = m[0].UpdatedOn
			in.(*MetaInfo).CreatedOn = m[0].CreatedOn
			in.(*MetaInfo).DeletedOn = m[0].DeletedOn
			in.(*MetaInfo).IsDeleted = m[0].IsDeleted
		}
	}
	return objList[0], nil
}

func (s MeetingStore) ListMeetings(ctx context.Context, fields []string, cond MeetingCondition, opt ...listMeetingsOption) ([]*Meeting, error) {
	if len(fields) == 0 {
		fields = (&Meeting{}).Fields()
	}
	var (
		res driver.Result
		err error
		m   driver.MetaInfo
	)
	for _, o := range opt {
		t, in := o.getValue()
		switch t {
		case driver.OptionType_Pagination:
			cond, err = in.(meetingsPageInterface).applyOffsetToMeetingsList(ctx, s.d, cond)
			if err != nil {
				return nil, err
			}
		case driver.OptionType_MetaInfo:
			s.d.MetaInfoRequested(&ctx, &m)
		}
	}
	if s.withTx {
		res, err = s.tx.Get(ctx, cond.meetingCondToDriverMeetingCond(s.d), &Meeting{}, &Meeting{}, fields...)
	} else {
		res, err = s.d.Get(ctx, cond.meetingCondToDriverMeetingCond(s.d), &Meeting{}, &Meeting{}, fields...)
	}
	if err != nil {
		return nil, err
	}
	if res == nil {
		return nil, nil
	}
	defer res.Close()

	list := make([]*Meeting, 0, 1000)
	infoMap := make(map[string]*driver.MetaInfo, 0)

	for res.Next(ctx) {
		obj := &Meeting{}
		if err := res.Scan(ctx, obj); err != nil {
			return nil, err
		}
		for _, o := range opt {
			t, _ := o.getValue()
			switch t {
			case driver.OptionType_MetaInfo:
				infoMap[obj.Id] = &driver.MetaInfo{
					UpdatedBy: m.UpdatedBy,
					CreatedBy: m.CreatedBy,
					DeletedBy: m.DeletedBy,
					UpdatedOn: m.UpdatedOn,
					CreatedOn: m.CreatedOn,
					DeletedOn: m.DeletedOn,
					IsDeleted: m.IsDeleted,
				}
				break
			}
		}
		list = append(list, obj)
	}

	if err := res.Close(); err != nil {
		return nil, err
	}

	list = MapperMeeting(list)
	meta := &MetaInfoForList{}

	for _, o := range opt {
		t, in := o.getValue()
		switch t {
		case driver.OptionType_Pagination:
			list = in.(meetingsPageInterface).applyLimitToMeetingsList(list)
		case driver.OptionType_MetaInfo:
			meta = in.(*MetaInfoForList)
		}
	}
	for _, l := range list {
		*meta = append(*meta, infoMap[l.Id])
	}
	return list, nil
}

type getMeetingsOption interface {
	getOptMeetings() // method of no significant use
	getValue() (driver.OptionType, interface{})
}

func (*MetaInfo) getOptMeetings() { // method of no significant use
}

type listMeetingsOption interface {
	listOptMeetings() // method of no significant use
	getValue() (driver.OptionType, interface{})
}

type meetingsPageInterface interface {
	listMeetingsOption
	applyOffsetToMeetingsList(context.Context, driver.Driver, MeetingCondition) (MeetingCondition, error)
	applyLimitToMeetingsList([]*Meeting) []*Meeting // temporary
}

func (*MetaInfoForList) listOptMeetings() {
}

func (*OffsetBasedPagination) listOptMeetings() {
}

func (*CursorBasedPagination) listOptMeetings() {
}

func (p *OffsetBasedPagination) applyLimitToMeetingsList(ls []*Meeting) []*Meeting {
	p.Total = len(ls)
	if p.Total <= p.Limit {
		return ls
	}
	return ls[0:p.Limit]
}

func (p *CursorBasedPagination) applyLimitToMeetingsList(ls []*Meeting) []*Meeting {
	if len(ls) <= p.Limit {
		p.HasNext = false
		p.HasPrevious = false
		return ls
	}
	if p.UpOrDown {
		p.HasPrevious = true
		return ls[len(ls)-p.Limit:]
	} else {
		p.HasNext = true
		return ls[:p.Limit]
	}
}

func (p *OffsetBasedPagination) applyOffsetToMeetingsList(ctx context.Context, d driver.Driver, cond MeetingCondition) (MeetingCondition, error) {
	offID, err := d.OffsetValue(ctx, &Meeting{}, &Meeting{}, p.Offset)
	if err != nil {
		return cond, err
	}
	return MeetingAnd{cond, MeetingIdGtOrEq{offID}}, nil
}

func (p *CursorBasedPagination) applyOffsetToMeetingsList(ctx context.Context, d driver.Driver, cond MeetingCondition) (MeetingCondition, error) {
	if p.UpOrDown {
		if p.Cursor == "" {
			p.Cursor = "zzzzzz" // biggest id with prefix
		}
		return MeetingAnd{cond, MeetingIdLt{p.Cursor}}, nil
	} else {
		if p.Cursor != "" {
			cond = MeetingAnd{cond, MeetingIdGt{p.Cursor}}
		}
		return cond, nil
	}
}

type OffsetBasedPagination struct {
	Offset int
	Limit  int

	// Response objects Items - will be updated and set after the list call
	Total int // not handled
}

type CursorBasedPagination struct {
	// Set UpOrDown = true for getting list of data above Cursor-ID,
	// limited to 'limit' amount, when ordered by ID in Ascending order.
	// Set UpOrDown = false for getting list of data below Cursor-ID,
	// limited to 'limit' amount, when ordered by ID in Ascending order.
	Cursor   string
	Limit    int
	UpOrDown bool

	// Response objects Items - will be updated and set after the list call
	HasNext     bool // Used in case of UpOrDown = false
	HasPrevious bool // Used in case of UpOrDown = true
}

func (p *OffsetBasedPagination) getValue() (driver.OptionType, interface{}) {
	return driver.OptionType_Pagination, p
}

func (p *CursorBasedPagination) getValue() (driver.OptionType, interface{}) {
	return driver.OptionType_Pagination, p
}

type MetaInfo driver.MetaInfo
type MetaInfoForList []*driver.MetaInfo

func (p *MetaInfo) getValue() (driver.OptionType, interface{}) {
	return driver.OptionType_MetaInfo, p
}

func (p *MetaInfoForList) getValue() (driver.OptionType, interface{}) {
	return driver.OptionType_MetaInfo, p
}

type TrueCondition struct{}

type MeetingAnd []MeetingCondition

func (p MeetingAnd) meetingCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	dc := make([]driver.Conditioner, 0, len(p))
	for _, c := range p {
		dc = append(dc, c.meetingCondToDriverMeetingCond(d))
	}
	return driver.And{Conditioners: dc, Operator: d}
}

type MeetingOr []MeetingCondition

func (p MeetingOr) meetingCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	dc := make([]driver.Conditioner, 0, len(p))
	for _, c := range p {
		dc = append(dc, c.meetingCondToDriverMeetingCond(d))
	}
	return driver.Or{Conditioners: dc, Operator: d}
}

type MeetingParentEq struct {
	Parent string
}

func (c MeetingParentEq) meetingCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.Eq{Key: "parent", Value: c.Parent, Operator: d, Descriptor: &Meeting{}, RootDescriptor: &Meeting{}, CurrentDescriptor: &Meeting{}}
}

type MeetingFullParentEq struct {
	Parent string
}

func (c MeetingFullParentEq) meetingCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.Eq{Key: "fullParent", Value: c.Parent, Operator: d, Descriptor: &Meeting{}, RootDescriptor: &Meeting{}, CurrentDescriptor: &Meeting{}}
}

type MeetingParentNotEq struct {
	Parent string
}

func (c MeetingParentNotEq) meetingCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.NotEq{Key: "parent", Value: c.Parent, Operator: d, Descriptor: &Meeting{}, RootDescriptor: &Meeting{}, CurrentDescriptor: &Meeting{}}
}

type MeetingFullParentNotEq struct {
	Parent string
}

func (c MeetingFullParentNotEq) meetingCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.NotEq{Key: "fullParent", Value: c.Parent, Operator: d, Descriptor: &Meeting{}, RootDescriptor: &Meeting{}, CurrentDescriptor: &Meeting{}}
}

type MeetingIdEq struct {
	Id string
}

func (c MeetingIdEq) meetingCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.Eq{Key: "id", Value: c.Id, Operator: d, Descriptor: &Meeting{}, FieldMask: "id", RootDescriptor: &Meeting{}, CurrentDescriptor: &Meeting{}}
}

type MeetingTopicEq struct {
	Topic string
}

func (c MeetingTopicEq) meetingCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.Eq{Key: "topic", Value: c.Topic, Operator: d, Descriptor: &Meeting{}, FieldMask: "topic", RootDescriptor: &Meeting{}, CurrentDescriptor: &Meeting{}}
}

type MeetingDescriptionEq struct {
	Description string
}

func (c MeetingDescriptionEq) meetingCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.Eq{Key: "description", Value: c.Description, Operator: d, Descriptor: &Meeting{}, FieldMask: "description", RootDescriptor: &Meeting{}, CurrentDescriptor: &Meeting{}}
}

type MeetingMeetingTypeEq struct {
	MeetingType MeetingType
}

func (c MeetingMeetingTypeEq) meetingCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.Eq{Key: "meeting_type", Value: c.MeetingType, Operator: d, Descriptor: &Meeting{}, FieldMask: "meeting_type", RootDescriptor: &Meeting{}, CurrentDescriptor: &Meeting{}}
}

type MeetingMeetingAppTypeEq struct {
	MeetingAppType MeetingAppType
}

func (c MeetingMeetingAppTypeEq) meetingCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.Eq{Key: "meeting_app_type", Value: c.MeetingAppType, Operator: d, Descriptor: &Meeting{}, FieldMask: "meeting_app_type", RootDescriptor: &Meeting{}, CurrentDescriptor: &Meeting{}}
}

type MeetingStartTimeEq struct {
	StartTime *timestamp.Timestamp
}

func (c MeetingStartTimeEq) meetingCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.Eq{Key: "start_time", Value: c.StartTime, Operator: d, Descriptor: &Meeting{}, FieldMask: "start_time", RootDescriptor: &Meeting{}, CurrentDescriptor: &Meeting{}}
}

type MeetingEndTimeEq struct {
	EndTime *timestamp.Timestamp
}

func (c MeetingEndTimeEq) meetingCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.Eq{Key: "end_time", Value: c.EndTime, Operator: d, Descriptor: &Meeting{}, FieldMask: "end_time", RootDescriptor: &Meeting{}, CurrentDescriptor: &Meeting{}}
}

type MeetingDurationEq struct {
	Duration int64
}

func (c MeetingDurationEq) meetingCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.Eq{Key: "duration", Value: c.Duration, Operator: d, Descriptor: &Meeting{}, FieldMask: "duration", RootDescriptor: &Meeting{}, CurrentDescriptor: &Meeting{}}
}

type MeetingTimezoneEq struct {
	Timezone string
}

func (c MeetingTimezoneEq) meetingCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.Eq{Key: "timezone", Value: c.Timezone, Operator: d, Descriptor: &Meeting{}, FieldMask: "timezone", RootDescriptor: &Meeting{}, CurrentDescriptor: &Meeting{}}
}

type MeetingPasswordEq struct {
	Password string
}

func (c MeetingPasswordEq) meetingCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.Eq{Key: "password", Value: c.Password, Operator: d, Descriptor: &Meeting{}, FieldMask: "password", RootDescriptor: &Meeting{}, CurrentDescriptor: &Meeting{}}
}

type MeetingStartUrlEq struct {
	StartUrl string
}

func (c MeetingStartUrlEq) meetingCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.Eq{Key: "start_url", Value: c.StartUrl, Operator: d, Descriptor: &Meeting{}, FieldMask: "start_url", RootDescriptor: &Meeting{}, CurrentDescriptor: &Meeting{}}
}

type MeetingJoinUrlEq struct {
	JoinUrl string
}

func (c MeetingJoinUrlEq) meetingCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.Eq{Key: "join_url", Value: c.JoinUrl, Operator: d, Descriptor: &Meeting{}, FieldMask: "join_url", RootDescriptor: &Meeting{}, CurrentDescriptor: &Meeting{}}
}

type MeetingMeetingIdEq struct {
	MeetingId string
}

func (c MeetingMeetingIdEq) meetingCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.Eq{Key: "meeting_id", Value: c.MeetingId, Operator: d, Descriptor: &Meeting{}, FieldMask: "meeting_id", RootDescriptor: &Meeting{}, CurrentDescriptor: &Meeting{}}
}

type MeetingUserIdEq struct {
	UserId string
}

func (c MeetingUserIdEq) meetingCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.Eq{Key: "user_id", Value: c.UserId, Operator: d, Descriptor: &Meeting{}, FieldMask: "user_id", RootDescriptor: &Meeting{}, CurrentDescriptor: &Meeting{}}
}

type MeetingRecurrenceRuleEq struct {
	RecurrenceRule string
}

func (c MeetingRecurrenceRuleEq) meetingCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.Eq{Key: "recurrence_rule", Value: c.RecurrenceRule, Operator: d, Descriptor: &Meeting{}, FieldMask: "recurrence_rule", RootDescriptor: &Meeting{}, CurrentDescriptor: &Meeting{}}
}

type MeetingErrorMessageEq struct {
	ErrorMessage string
}

func (c MeetingErrorMessageEq) meetingCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.Eq{Key: "error_message", Value: c.ErrorMessage, Operator: d, Descriptor: &Meeting{}, FieldMask: "error_message", RootDescriptor: &Meeting{}, CurrentDescriptor: &Meeting{}}
}

type MeetingParticipantListIdEq struct {
	Id string
}

func (c MeetingParticipantListIdEq) meetingCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.Eq{Key: "id", Value: c.Id, Operator: d, Descriptor: &ParticipantList{}, FieldMask: "participant_list.id", RootDescriptor: &Meeting{}, CurrentDescriptor: &Meeting{}}
}

type MeetingParticipantListStaffIdEq struct {
	StaffId string
}

func (c MeetingParticipantListStaffIdEq) meetingCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.Eq{Key: "staff_id", Value: c.StaffId, Operator: d, Descriptor: &ParticipantList{}, FieldMask: "participant_list.staff_id", RootDescriptor: &Meeting{}, CurrentDescriptor: &Meeting{}}
}

type MeetingParticipantListParticipantIdEq struct {
	ParticipantId string
}

func (c MeetingParticipantListParticipantIdEq) meetingCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.Eq{Key: "participant_id", Value: c.ParticipantId, Operator: d, Descriptor: &ParticipantList{}, FieldMask: "participant_list.participant_id", RootDescriptor: &Meeting{}, CurrentDescriptor: &Meeting{}}
}

type MeetingParticipantListFirstNameEq struct {
	FirstName string
}

func (c MeetingParticipantListFirstNameEq) meetingCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.Eq{Key: "first_name", Value: c.FirstName, Operator: d, Descriptor: &ParticipantList{}, FieldMask: "participant_list.first_name", RootDescriptor: &Meeting{}, CurrentDescriptor: &Meeting{}}
}

type MeetingParticipantListEmailEq struct {
	Email string
}

func (c MeetingParticipantListEmailEq) meetingCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.Eq{Key: "email", Value: c.Email, Operator: d, Descriptor: &ParticipantList{}, FieldMask: "participant_list.email", RootDescriptor: &Meeting{}, CurrentDescriptor: &Meeting{}}
}

type MeetingParticipantListLastNameEq struct {
	LastName string
}

func (c MeetingParticipantListLastNameEq) meetingCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.Eq{Key: "last_name", Value: c.LastName, Operator: d, Descriptor: &ParticipantList{}, FieldMask: "participant_list.last_name", RootDescriptor: &Meeting{}, CurrentDescriptor: &Meeting{}}
}

type MeetingParticipantListParticipantJoinUrlEq struct {
	ParticipantJoinUrl string
}

func (c MeetingParticipantListParticipantJoinUrlEq) meetingCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.Eq{Key: "participant_join_url", Value: c.ParticipantJoinUrl, Operator: d, Descriptor: &ParticipantList{}, FieldMask: "participant_list.participant_join_url", RootDescriptor: &Meeting{}, CurrentDescriptor: &Meeting{}}
}

type MeetingIdNotEq struct {
	Id string
}

func (c MeetingIdNotEq) meetingCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.NotEq{Key: "id", Value: c.Id, Operator: d, Descriptor: &Meeting{}, FieldMask: "id", RootDescriptor: &Meeting{}, CurrentDescriptor: &Meeting{}}
}

type MeetingTopicNotEq struct {
	Topic string
}

func (c MeetingTopicNotEq) meetingCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.NotEq{Key: "topic", Value: c.Topic, Operator: d, Descriptor: &Meeting{}, FieldMask: "topic", RootDescriptor: &Meeting{}, CurrentDescriptor: &Meeting{}}
}

type MeetingDescriptionNotEq struct {
	Description string
}

func (c MeetingDescriptionNotEq) meetingCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.NotEq{Key: "description", Value: c.Description, Operator: d, Descriptor: &Meeting{}, FieldMask: "description", RootDescriptor: &Meeting{}, CurrentDescriptor: &Meeting{}}
}

type MeetingMeetingTypeNotEq struct {
	MeetingType MeetingType
}

func (c MeetingMeetingTypeNotEq) meetingCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.NotEq{Key: "meeting_type", Value: c.MeetingType, Operator: d, Descriptor: &Meeting{}, FieldMask: "meeting_type", RootDescriptor: &Meeting{}, CurrentDescriptor: &Meeting{}}
}

type MeetingMeetingAppTypeNotEq struct {
	MeetingAppType MeetingAppType
}

func (c MeetingMeetingAppTypeNotEq) meetingCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.NotEq{Key: "meeting_app_type", Value: c.MeetingAppType, Operator: d, Descriptor: &Meeting{}, FieldMask: "meeting_app_type", RootDescriptor: &Meeting{}, CurrentDescriptor: &Meeting{}}
}

type MeetingStartTimeNotEq struct {
	StartTime *timestamp.Timestamp
}

func (c MeetingStartTimeNotEq) meetingCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.NotEq{Key: "start_time", Value: c.StartTime, Operator: d, Descriptor: &Meeting{}, FieldMask: "start_time", RootDescriptor: &Meeting{}, CurrentDescriptor: &Meeting{}}
}

type MeetingEndTimeNotEq struct {
	EndTime *timestamp.Timestamp
}

func (c MeetingEndTimeNotEq) meetingCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.NotEq{Key: "end_time", Value: c.EndTime, Operator: d, Descriptor: &Meeting{}, FieldMask: "end_time", RootDescriptor: &Meeting{}, CurrentDescriptor: &Meeting{}}
}

type MeetingDurationNotEq struct {
	Duration int64
}

func (c MeetingDurationNotEq) meetingCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.NotEq{Key: "duration", Value: c.Duration, Operator: d, Descriptor: &Meeting{}, FieldMask: "duration", RootDescriptor: &Meeting{}, CurrentDescriptor: &Meeting{}}
}

type MeetingTimezoneNotEq struct {
	Timezone string
}

func (c MeetingTimezoneNotEq) meetingCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.NotEq{Key: "timezone", Value: c.Timezone, Operator: d, Descriptor: &Meeting{}, FieldMask: "timezone", RootDescriptor: &Meeting{}, CurrentDescriptor: &Meeting{}}
}

type MeetingPasswordNotEq struct {
	Password string
}

func (c MeetingPasswordNotEq) meetingCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.NotEq{Key: "password", Value: c.Password, Operator: d, Descriptor: &Meeting{}, FieldMask: "password", RootDescriptor: &Meeting{}, CurrentDescriptor: &Meeting{}}
}

type MeetingStartUrlNotEq struct {
	StartUrl string
}

func (c MeetingStartUrlNotEq) meetingCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.NotEq{Key: "start_url", Value: c.StartUrl, Operator: d, Descriptor: &Meeting{}, FieldMask: "start_url", RootDescriptor: &Meeting{}, CurrentDescriptor: &Meeting{}}
}

type MeetingJoinUrlNotEq struct {
	JoinUrl string
}

func (c MeetingJoinUrlNotEq) meetingCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.NotEq{Key: "join_url", Value: c.JoinUrl, Operator: d, Descriptor: &Meeting{}, FieldMask: "join_url", RootDescriptor: &Meeting{}, CurrentDescriptor: &Meeting{}}
}

type MeetingMeetingIdNotEq struct {
	MeetingId string
}

func (c MeetingMeetingIdNotEq) meetingCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.NotEq{Key: "meeting_id", Value: c.MeetingId, Operator: d, Descriptor: &Meeting{}, FieldMask: "meeting_id", RootDescriptor: &Meeting{}, CurrentDescriptor: &Meeting{}}
}

type MeetingUserIdNotEq struct {
	UserId string
}

func (c MeetingUserIdNotEq) meetingCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.NotEq{Key: "user_id", Value: c.UserId, Operator: d, Descriptor: &Meeting{}, FieldMask: "user_id", RootDescriptor: &Meeting{}, CurrentDescriptor: &Meeting{}}
}

type MeetingRecurrenceRuleNotEq struct {
	RecurrenceRule string
}

func (c MeetingRecurrenceRuleNotEq) meetingCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.NotEq{Key: "recurrence_rule", Value: c.RecurrenceRule, Operator: d, Descriptor: &Meeting{}, FieldMask: "recurrence_rule", RootDescriptor: &Meeting{}, CurrentDescriptor: &Meeting{}}
}

type MeetingErrorMessageNotEq struct {
	ErrorMessage string
}

func (c MeetingErrorMessageNotEq) meetingCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.NotEq{Key: "error_message", Value: c.ErrorMessage, Operator: d, Descriptor: &Meeting{}, FieldMask: "error_message", RootDescriptor: &Meeting{}, CurrentDescriptor: &Meeting{}}
}

type MeetingParticipantListIdNotEq struct {
	Id string
}

func (c MeetingParticipantListIdNotEq) meetingCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.NotEq{Key: "id", Value: c.Id, Operator: d, Descriptor: &ParticipantList{}, FieldMask: "participant_list.id", RootDescriptor: &Meeting{}, CurrentDescriptor: &Meeting{}}
}

type MeetingParticipantListStaffIdNotEq struct {
	StaffId string
}

func (c MeetingParticipantListStaffIdNotEq) meetingCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.NotEq{Key: "staff_id", Value: c.StaffId, Operator: d, Descriptor: &ParticipantList{}, FieldMask: "participant_list.staff_id", RootDescriptor: &Meeting{}, CurrentDescriptor: &Meeting{}}
}

type MeetingParticipantListParticipantIdNotEq struct {
	ParticipantId string
}

func (c MeetingParticipantListParticipantIdNotEq) meetingCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.NotEq{Key: "participant_id", Value: c.ParticipantId, Operator: d, Descriptor: &ParticipantList{}, FieldMask: "participant_list.participant_id", RootDescriptor: &Meeting{}, CurrentDescriptor: &Meeting{}}
}

type MeetingParticipantListFirstNameNotEq struct {
	FirstName string
}

func (c MeetingParticipantListFirstNameNotEq) meetingCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.NotEq{Key: "first_name", Value: c.FirstName, Operator: d, Descriptor: &ParticipantList{}, FieldMask: "participant_list.first_name", RootDescriptor: &Meeting{}, CurrentDescriptor: &Meeting{}}
}

type MeetingParticipantListEmailNotEq struct {
	Email string
}

func (c MeetingParticipantListEmailNotEq) meetingCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.NotEq{Key: "email", Value: c.Email, Operator: d, Descriptor: &ParticipantList{}, FieldMask: "participant_list.email", RootDescriptor: &Meeting{}, CurrentDescriptor: &Meeting{}}
}

type MeetingParticipantListLastNameNotEq struct {
	LastName string
}

func (c MeetingParticipantListLastNameNotEq) meetingCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.NotEq{Key: "last_name", Value: c.LastName, Operator: d, Descriptor: &ParticipantList{}, FieldMask: "participant_list.last_name", RootDescriptor: &Meeting{}, CurrentDescriptor: &Meeting{}}
}

type MeetingParticipantListParticipantJoinUrlNotEq struct {
	ParticipantJoinUrl string
}

func (c MeetingParticipantListParticipantJoinUrlNotEq) meetingCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.NotEq{Key: "participant_join_url", Value: c.ParticipantJoinUrl, Operator: d, Descriptor: &ParticipantList{}, FieldMask: "participant_list.participant_join_url", RootDescriptor: &Meeting{}, CurrentDescriptor: &Meeting{}}
}

type MeetingIdGt struct {
	Id string
}

func (c MeetingIdGt) meetingCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.Gt{Key: "id", Value: c.Id, Operator: d, Descriptor: &Meeting{}, FieldMask: "id", RootDescriptor: &Meeting{}, CurrentDescriptor: &Meeting{}}
}

type MeetingTopicGt struct {
	Topic string
}

func (c MeetingTopicGt) meetingCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.Gt{Key: "topic", Value: c.Topic, Operator: d, Descriptor: &Meeting{}, FieldMask: "topic", RootDescriptor: &Meeting{}, CurrentDescriptor: &Meeting{}}
}

type MeetingDescriptionGt struct {
	Description string
}

func (c MeetingDescriptionGt) meetingCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.Gt{Key: "description", Value: c.Description, Operator: d, Descriptor: &Meeting{}, FieldMask: "description", RootDescriptor: &Meeting{}, CurrentDescriptor: &Meeting{}}
}

type MeetingMeetingTypeGt struct {
	MeetingType MeetingType
}

func (c MeetingMeetingTypeGt) meetingCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.Gt{Key: "meeting_type", Value: c.MeetingType, Operator: d, Descriptor: &Meeting{}, FieldMask: "meeting_type", RootDescriptor: &Meeting{}, CurrentDescriptor: &Meeting{}}
}

type MeetingMeetingAppTypeGt struct {
	MeetingAppType MeetingAppType
}

func (c MeetingMeetingAppTypeGt) meetingCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.Gt{Key: "meeting_app_type", Value: c.MeetingAppType, Operator: d, Descriptor: &Meeting{}, FieldMask: "meeting_app_type", RootDescriptor: &Meeting{}, CurrentDescriptor: &Meeting{}}
}

type MeetingStartTimeGt struct {
	StartTime *timestamp.Timestamp
}

func (c MeetingStartTimeGt) meetingCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.Gt{Key: "start_time", Value: c.StartTime, Operator: d, Descriptor: &Meeting{}, FieldMask: "start_time", RootDescriptor: &Meeting{}, CurrentDescriptor: &Meeting{}}
}

type MeetingEndTimeGt struct {
	EndTime *timestamp.Timestamp
}

func (c MeetingEndTimeGt) meetingCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.Gt{Key: "end_time", Value: c.EndTime, Operator: d, Descriptor: &Meeting{}, FieldMask: "end_time", RootDescriptor: &Meeting{}, CurrentDescriptor: &Meeting{}}
}

type MeetingDurationGt struct {
	Duration int64
}

func (c MeetingDurationGt) meetingCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.Gt{Key: "duration", Value: c.Duration, Operator: d, Descriptor: &Meeting{}, FieldMask: "duration", RootDescriptor: &Meeting{}, CurrentDescriptor: &Meeting{}}
}

type MeetingTimezoneGt struct {
	Timezone string
}

func (c MeetingTimezoneGt) meetingCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.Gt{Key: "timezone", Value: c.Timezone, Operator: d, Descriptor: &Meeting{}, FieldMask: "timezone", RootDescriptor: &Meeting{}, CurrentDescriptor: &Meeting{}}
}

type MeetingPasswordGt struct {
	Password string
}

func (c MeetingPasswordGt) meetingCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.Gt{Key: "password", Value: c.Password, Operator: d, Descriptor: &Meeting{}, FieldMask: "password", RootDescriptor: &Meeting{}, CurrentDescriptor: &Meeting{}}
}

type MeetingStartUrlGt struct {
	StartUrl string
}

func (c MeetingStartUrlGt) meetingCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.Gt{Key: "start_url", Value: c.StartUrl, Operator: d, Descriptor: &Meeting{}, FieldMask: "start_url", RootDescriptor: &Meeting{}, CurrentDescriptor: &Meeting{}}
}

type MeetingJoinUrlGt struct {
	JoinUrl string
}

func (c MeetingJoinUrlGt) meetingCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.Gt{Key: "join_url", Value: c.JoinUrl, Operator: d, Descriptor: &Meeting{}, FieldMask: "join_url", RootDescriptor: &Meeting{}, CurrentDescriptor: &Meeting{}}
}

type MeetingMeetingIdGt struct {
	MeetingId string
}

func (c MeetingMeetingIdGt) meetingCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.Gt{Key: "meeting_id", Value: c.MeetingId, Operator: d, Descriptor: &Meeting{}, FieldMask: "meeting_id", RootDescriptor: &Meeting{}, CurrentDescriptor: &Meeting{}}
}

type MeetingUserIdGt struct {
	UserId string
}

func (c MeetingUserIdGt) meetingCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.Gt{Key: "user_id", Value: c.UserId, Operator: d, Descriptor: &Meeting{}, FieldMask: "user_id", RootDescriptor: &Meeting{}, CurrentDescriptor: &Meeting{}}
}

type MeetingRecurrenceRuleGt struct {
	RecurrenceRule string
}

func (c MeetingRecurrenceRuleGt) meetingCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.Gt{Key: "recurrence_rule", Value: c.RecurrenceRule, Operator: d, Descriptor: &Meeting{}, FieldMask: "recurrence_rule", RootDescriptor: &Meeting{}, CurrentDescriptor: &Meeting{}}
}

type MeetingErrorMessageGt struct {
	ErrorMessage string
}

func (c MeetingErrorMessageGt) meetingCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.Gt{Key: "error_message", Value: c.ErrorMessage, Operator: d, Descriptor: &Meeting{}, FieldMask: "error_message", RootDescriptor: &Meeting{}, CurrentDescriptor: &Meeting{}}
}

type MeetingParticipantListIdGt struct {
	Id string
}

func (c MeetingParticipantListIdGt) meetingCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.Gt{Key: "id", Value: c.Id, Operator: d, Descriptor: &ParticipantList{}, FieldMask: "participant_list.id", RootDescriptor: &Meeting{}, CurrentDescriptor: &Meeting{}}
}

type MeetingParticipantListStaffIdGt struct {
	StaffId string
}

func (c MeetingParticipantListStaffIdGt) meetingCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.Gt{Key: "staff_id", Value: c.StaffId, Operator: d, Descriptor: &ParticipantList{}, FieldMask: "participant_list.staff_id", RootDescriptor: &Meeting{}, CurrentDescriptor: &Meeting{}}
}

type MeetingParticipantListParticipantIdGt struct {
	ParticipantId string
}

func (c MeetingParticipantListParticipantIdGt) meetingCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.Gt{Key: "participant_id", Value: c.ParticipantId, Operator: d, Descriptor: &ParticipantList{}, FieldMask: "participant_list.participant_id", RootDescriptor: &Meeting{}, CurrentDescriptor: &Meeting{}}
}

type MeetingParticipantListFirstNameGt struct {
	FirstName string
}

func (c MeetingParticipantListFirstNameGt) meetingCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.Gt{Key: "first_name", Value: c.FirstName, Operator: d, Descriptor: &ParticipantList{}, FieldMask: "participant_list.first_name", RootDescriptor: &Meeting{}, CurrentDescriptor: &Meeting{}}
}

type MeetingParticipantListEmailGt struct {
	Email string
}

func (c MeetingParticipantListEmailGt) meetingCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.Gt{Key: "email", Value: c.Email, Operator: d, Descriptor: &ParticipantList{}, FieldMask: "participant_list.email", RootDescriptor: &Meeting{}, CurrentDescriptor: &Meeting{}}
}

type MeetingParticipantListLastNameGt struct {
	LastName string
}

func (c MeetingParticipantListLastNameGt) meetingCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.Gt{Key: "last_name", Value: c.LastName, Operator: d, Descriptor: &ParticipantList{}, FieldMask: "participant_list.last_name", RootDescriptor: &Meeting{}, CurrentDescriptor: &Meeting{}}
}

type MeetingParticipantListParticipantJoinUrlGt struct {
	ParticipantJoinUrl string
}

func (c MeetingParticipantListParticipantJoinUrlGt) meetingCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.Gt{Key: "participant_join_url", Value: c.ParticipantJoinUrl, Operator: d, Descriptor: &ParticipantList{}, FieldMask: "participant_list.participant_join_url", RootDescriptor: &Meeting{}, CurrentDescriptor: &Meeting{}}
}

type MeetingIdLt struct {
	Id string
}

func (c MeetingIdLt) meetingCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.Lt{Key: "id", Value: c.Id, Operator: d, Descriptor: &Meeting{}, FieldMask: "id", RootDescriptor: &Meeting{}, CurrentDescriptor: &Meeting{}}
}

type MeetingTopicLt struct {
	Topic string
}

func (c MeetingTopicLt) meetingCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.Lt{Key: "topic", Value: c.Topic, Operator: d, Descriptor: &Meeting{}, FieldMask: "topic", RootDescriptor: &Meeting{}, CurrentDescriptor: &Meeting{}}
}

type MeetingDescriptionLt struct {
	Description string
}

func (c MeetingDescriptionLt) meetingCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.Lt{Key: "description", Value: c.Description, Operator: d, Descriptor: &Meeting{}, FieldMask: "description", RootDescriptor: &Meeting{}, CurrentDescriptor: &Meeting{}}
}

type MeetingMeetingTypeLt struct {
	MeetingType MeetingType
}

func (c MeetingMeetingTypeLt) meetingCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.Lt{Key: "meeting_type", Value: c.MeetingType, Operator: d, Descriptor: &Meeting{}, FieldMask: "meeting_type", RootDescriptor: &Meeting{}, CurrentDescriptor: &Meeting{}}
}

type MeetingMeetingAppTypeLt struct {
	MeetingAppType MeetingAppType
}

func (c MeetingMeetingAppTypeLt) meetingCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.Lt{Key: "meeting_app_type", Value: c.MeetingAppType, Operator: d, Descriptor: &Meeting{}, FieldMask: "meeting_app_type", RootDescriptor: &Meeting{}, CurrentDescriptor: &Meeting{}}
}

type MeetingStartTimeLt struct {
	StartTime *timestamp.Timestamp
}

func (c MeetingStartTimeLt) meetingCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.Lt{Key: "start_time", Value: c.StartTime, Operator: d, Descriptor: &Meeting{}, FieldMask: "start_time", RootDescriptor: &Meeting{}, CurrentDescriptor: &Meeting{}}
}

type MeetingEndTimeLt struct {
	EndTime *timestamp.Timestamp
}

func (c MeetingEndTimeLt) meetingCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.Lt{Key: "end_time", Value: c.EndTime, Operator: d, Descriptor: &Meeting{}, FieldMask: "end_time", RootDescriptor: &Meeting{}, CurrentDescriptor: &Meeting{}}
}

type MeetingDurationLt struct {
	Duration int64
}

func (c MeetingDurationLt) meetingCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.Lt{Key: "duration", Value: c.Duration, Operator: d, Descriptor: &Meeting{}, FieldMask: "duration", RootDescriptor: &Meeting{}, CurrentDescriptor: &Meeting{}}
}

type MeetingTimezoneLt struct {
	Timezone string
}

func (c MeetingTimezoneLt) meetingCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.Lt{Key: "timezone", Value: c.Timezone, Operator: d, Descriptor: &Meeting{}, FieldMask: "timezone", RootDescriptor: &Meeting{}, CurrentDescriptor: &Meeting{}}
}

type MeetingPasswordLt struct {
	Password string
}

func (c MeetingPasswordLt) meetingCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.Lt{Key: "password", Value: c.Password, Operator: d, Descriptor: &Meeting{}, FieldMask: "password", RootDescriptor: &Meeting{}, CurrentDescriptor: &Meeting{}}
}

type MeetingStartUrlLt struct {
	StartUrl string
}

func (c MeetingStartUrlLt) meetingCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.Lt{Key: "start_url", Value: c.StartUrl, Operator: d, Descriptor: &Meeting{}, FieldMask: "start_url", RootDescriptor: &Meeting{}, CurrentDescriptor: &Meeting{}}
}

type MeetingJoinUrlLt struct {
	JoinUrl string
}

func (c MeetingJoinUrlLt) meetingCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.Lt{Key: "join_url", Value: c.JoinUrl, Operator: d, Descriptor: &Meeting{}, FieldMask: "join_url", RootDescriptor: &Meeting{}, CurrentDescriptor: &Meeting{}}
}

type MeetingMeetingIdLt struct {
	MeetingId string
}

func (c MeetingMeetingIdLt) meetingCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.Lt{Key: "meeting_id", Value: c.MeetingId, Operator: d, Descriptor: &Meeting{}, FieldMask: "meeting_id", RootDescriptor: &Meeting{}, CurrentDescriptor: &Meeting{}}
}

type MeetingUserIdLt struct {
	UserId string
}

func (c MeetingUserIdLt) meetingCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.Lt{Key: "user_id", Value: c.UserId, Operator: d, Descriptor: &Meeting{}, FieldMask: "user_id", RootDescriptor: &Meeting{}, CurrentDescriptor: &Meeting{}}
}

type MeetingRecurrenceRuleLt struct {
	RecurrenceRule string
}

func (c MeetingRecurrenceRuleLt) meetingCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.Lt{Key: "recurrence_rule", Value: c.RecurrenceRule, Operator: d, Descriptor: &Meeting{}, FieldMask: "recurrence_rule", RootDescriptor: &Meeting{}, CurrentDescriptor: &Meeting{}}
}

type MeetingErrorMessageLt struct {
	ErrorMessage string
}

func (c MeetingErrorMessageLt) meetingCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.Lt{Key: "error_message", Value: c.ErrorMessage, Operator: d, Descriptor: &Meeting{}, FieldMask: "error_message", RootDescriptor: &Meeting{}, CurrentDescriptor: &Meeting{}}
}

type MeetingParticipantListIdLt struct {
	Id string
}

func (c MeetingParticipantListIdLt) meetingCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.Lt{Key: "id", Value: c.Id, Operator: d, Descriptor: &ParticipantList{}, FieldMask: "participant_list.id", RootDescriptor: &Meeting{}, CurrentDescriptor: &Meeting{}}
}

type MeetingParticipantListStaffIdLt struct {
	StaffId string
}

func (c MeetingParticipantListStaffIdLt) meetingCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.Lt{Key: "staff_id", Value: c.StaffId, Operator: d, Descriptor: &ParticipantList{}, FieldMask: "participant_list.staff_id", RootDescriptor: &Meeting{}, CurrentDescriptor: &Meeting{}}
}

type MeetingParticipantListParticipantIdLt struct {
	ParticipantId string
}

func (c MeetingParticipantListParticipantIdLt) meetingCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.Lt{Key: "participant_id", Value: c.ParticipantId, Operator: d, Descriptor: &ParticipantList{}, FieldMask: "participant_list.participant_id", RootDescriptor: &Meeting{}, CurrentDescriptor: &Meeting{}}
}

type MeetingParticipantListFirstNameLt struct {
	FirstName string
}

func (c MeetingParticipantListFirstNameLt) meetingCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.Lt{Key: "first_name", Value: c.FirstName, Operator: d, Descriptor: &ParticipantList{}, FieldMask: "participant_list.first_name", RootDescriptor: &Meeting{}, CurrentDescriptor: &Meeting{}}
}

type MeetingParticipantListEmailLt struct {
	Email string
}

func (c MeetingParticipantListEmailLt) meetingCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.Lt{Key: "email", Value: c.Email, Operator: d, Descriptor: &ParticipantList{}, FieldMask: "participant_list.email", RootDescriptor: &Meeting{}, CurrentDescriptor: &Meeting{}}
}

type MeetingParticipantListLastNameLt struct {
	LastName string
}

func (c MeetingParticipantListLastNameLt) meetingCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.Lt{Key: "last_name", Value: c.LastName, Operator: d, Descriptor: &ParticipantList{}, FieldMask: "participant_list.last_name", RootDescriptor: &Meeting{}, CurrentDescriptor: &Meeting{}}
}

type MeetingParticipantListParticipantJoinUrlLt struct {
	ParticipantJoinUrl string
}

func (c MeetingParticipantListParticipantJoinUrlLt) meetingCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.Lt{Key: "participant_join_url", Value: c.ParticipantJoinUrl, Operator: d, Descriptor: &ParticipantList{}, FieldMask: "participant_list.participant_join_url", RootDescriptor: &Meeting{}, CurrentDescriptor: &Meeting{}}
}

type MeetingIdGtOrEq struct {
	Id string
}

func (c MeetingIdGtOrEq) meetingCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.GtOrEq{Key: "id", Value: c.Id, Operator: d, Descriptor: &Meeting{}, FieldMask: "id", RootDescriptor: &Meeting{}, CurrentDescriptor: &Meeting{}}
}

type MeetingTopicGtOrEq struct {
	Topic string
}

func (c MeetingTopicGtOrEq) meetingCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.GtOrEq{Key: "topic", Value: c.Topic, Operator: d, Descriptor: &Meeting{}, FieldMask: "topic", RootDescriptor: &Meeting{}, CurrentDescriptor: &Meeting{}}
}

type MeetingDescriptionGtOrEq struct {
	Description string
}

func (c MeetingDescriptionGtOrEq) meetingCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.GtOrEq{Key: "description", Value: c.Description, Operator: d, Descriptor: &Meeting{}, FieldMask: "description", RootDescriptor: &Meeting{}, CurrentDescriptor: &Meeting{}}
}

type MeetingMeetingTypeGtOrEq struct {
	MeetingType MeetingType
}

func (c MeetingMeetingTypeGtOrEq) meetingCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.GtOrEq{Key: "meeting_type", Value: c.MeetingType, Operator: d, Descriptor: &Meeting{}, FieldMask: "meeting_type", RootDescriptor: &Meeting{}, CurrentDescriptor: &Meeting{}}
}

type MeetingMeetingAppTypeGtOrEq struct {
	MeetingAppType MeetingAppType
}

func (c MeetingMeetingAppTypeGtOrEq) meetingCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.GtOrEq{Key: "meeting_app_type", Value: c.MeetingAppType, Operator: d, Descriptor: &Meeting{}, FieldMask: "meeting_app_type", RootDescriptor: &Meeting{}, CurrentDescriptor: &Meeting{}}
}

type MeetingStartTimeGtOrEq struct {
	StartTime *timestamp.Timestamp
}

func (c MeetingStartTimeGtOrEq) meetingCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.GtOrEq{Key: "start_time", Value: c.StartTime, Operator: d, Descriptor: &Meeting{}, FieldMask: "start_time", RootDescriptor: &Meeting{}, CurrentDescriptor: &Meeting{}}
}

type MeetingEndTimeGtOrEq struct {
	EndTime *timestamp.Timestamp
}

func (c MeetingEndTimeGtOrEq) meetingCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.GtOrEq{Key: "end_time", Value: c.EndTime, Operator: d, Descriptor: &Meeting{}, FieldMask: "end_time", RootDescriptor: &Meeting{}, CurrentDescriptor: &Meeting{}}
}

type MeetingDurationGtOrEq struct {
	Duration int64
}

func (c MeetingDurationGtOrEq) meetingCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.GtOrEq{Key: "duration", Value: c.Duration, Operator: d, Descriptor: &Meeting{}, FieldMask: "duration", RootDescriptor: &Meeting{}, CurrentDescriptor: &Meeting{}}
}

type MeetingTimezoneGtOrEq struct {
	Timezone string
}

func (c MeetingTimezoneGtOrEq) meetingCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.GtOrEq{Key: "timezone", Value: c.Timezone, Operator: d, Descriptor: &Meeting{}, FieldMask: "timezone", RootDescriptor: &Meeting{}, CurrentDescriptor: &Meeting{}}
}

type MeetingPasswordGtOrEq struct {
	Password string
}

func (c MeetingPasswordGtOrEq) meetingCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.GtOrEq{Key: "password", Value: c.Password, Operator: d, Descriptor: &Meeting{}, FieldMask: "password", RootDescriptor: &Meeting{}, CurrentDescriptor: &Meeting{}}
}

type MeetingStartUrlGtOrEq struct {
	StartUrl string
}

func (c MeetingStartUrlGtOrEq) meetingCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.GtOrEq{Key: "start_url", Value: c.StartUrl, Operator: d, Descriptor: &Meeting{}, FieldMask: "start_url", RootDescriptor: &Meeting{}, CurrentDescriptor: &Meeting{}}
}

type MeetingJoinUrlGtOrEq struct {
	JoinUrl string
}

func (c MeetingJoinUrlGtOrEq) meetingCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.GtOrEq{Key: "join_url", Value: c.JoinUrl, Operator: d, Descriptor: &Meeting{}, FieldMask: "join_url", RootDescriptor: &Meeting{}, CurrentDescriptor: &Meeting{}}
}

type MeetingMeetingIdGtOrEq struct {
	MeetingId string
}

func (c MeetingMeetingIdGtOrEq) meetingCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.GtOrEq{Key: "meeting_id", Value: c.MeetingId, Operator: d, Descriptor: &Meeting{}, FieldMask: "meeting_id", RootDescriptor: &Meeting{}, CurrentDescriptor: &Meeting{}}
}

type MeetingUserIdGtOrEq struct {
	UserId string
}

func (c MeetingUserIdGtOrEq) meetingCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.GtOrEq{Key: "user_id", Value: c.UserId, Operator: d, Descriptor: &Meeting{}, FieldMask: "user_id", RootDescriptor: &Meeting{}, CurrentDescriptor: &Meeting{}}
}

type MeetingRecurrenceRuleGtOrEq struct {
	RecurrenceRule string
}

func (c MeetingRecurrenceRuleGtOrEq) meetingCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.GtOrEq{Key: "recurrence_rule", Value: c.RecurrenceRule, Operator: d, Descriptor: &Meeting{}, FieldMask: "recurrence_rule", RootDescriptor: &Meeting{}, CurrentDescriptor: &Meeting{}}
}

type MeetingErrorMessageGtOrEq struct {
	ErrorMessage string
}

func (c MeetingErrorMessageGtOrEq) meetingCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.GtOrEq{Key: "error_message", Value: c.ErrorMessage, Operator: d, Descriptor: &Meeting{}, FieldMask: "error_message", RootDescriptor: &Meeting{}, CurrentDescriptor: &Meeting{}}
}

type MeetingParticipantListIdGtOrEq struct {
	Id string
}

func (c MeetingParticipantListIdGtOrEq) meetingCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.GtOrEq{Key: "id", Value: c.Id, Operator: d, Descriptor: &ParticipantList{}, FieldMask: "participant_list.id", RootDescriptor: &Meeting{}, CurrentDescriptor: &Meeting{}}
}

type MeetingParticipantListStaffIdGtOrEq struct {
	StaffId string
}

func (c MeetingParticipantListStaffIdGtOrEq) meetingCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.GtOrEq{Key: "staff_id", Value: c.StaffId, Operator: d, Descriptor: &ParticipantList{}, FieldMask: "participant_list.staff_id", RootDescriptor: &Meeting{}, CurrentDescriptor: &Meeting{}}
}

type MeetingParticipantListParticipantIdGtOrEq struct {
	ParticipantId string
}

func (c MeetingParticipantListParticipantIdGtOrEq) meetingCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.GtOrEq{Key: "participant_id", Value: c.ParticipantId, Operator: d, Descriptor: &ParticipantList{}, FieldMask: "participant_list.participant_id", RootDescriptor: &Meeting{}, CurrentDescriptor: &Meeting{}}
}

type MeetingParticipantListFirstNameGtOrEq struct {
	FirstName string
}

func (c MeetingParticipantListFirstNameGtOrEq) meetingCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.GtOrEq{Key: "first_name", Value: c.FirstName, Operator: d, Descriptor: &ParticipantList{}, FieldMask: "participant_list.first_name", RootDescriptor: &Meeting{}, CurrentDescriptor: &Meeting{}}
}

type MeetingParticipantListEmailGtOrEq struct {
	Email string
}

func (c MeetingParticipantListEmailGtOrEq) meetingCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.GtOrEq{Key: "email", Value: c.Email, Operator: d, Descriptor: &ParticipantList{}, FieldMask: "participant_list.email", RootDescriptor: &Meeting{}, CurrentDescriptor: &Meeting{}}
}

type MeetingParticipantListLastNameGtOrEq struct {
	LastName string
}

func (c MeetingParticipantListLastNameGtOrEq) meetingCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.GtOrEq{Key: "last_name", Value: c.LastName, Operator: d, Descriptor: &ParticipantList{}, FieldMask: "participant_list.last_name", RootDescriptor: &Meeting{}, CurrentDescriptor: &Meeting{}}
}

type MeetingParticipantListParticipantJoinUrlGtOrEq struct {
	ParticipantJoinUrl string
}

func (c MeetingParticipantListParticipantJoinUrlGtOrEq) meetingCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.GtOrEq{Key: "participant_join_url", Value: c.ParticipantJoinUrl, Operator: d, Descriptor: &ParticipantList{}, FieldMask: "participant_list.participant_join_url", RootDescriptor: &Meeting{}, CurrentDescriptor: &Meeting{}}
}

type MeetingIdLtOrEq struct {
	Id string
}

func (c MeetingIdLtOrEq) meetingCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.LtOrEq{Key: "id", Value: c.Id, Operator: d, Descriptor: &Meeting{}, FieldMask: "id", RootDescriptor: &Meeting{}, CurrentDescriptor: &Meeting{}}
}

type MeetingTopicLtOrEq struct {
	Topic string
}

func (c MeetingTopicLtOrEq) meetingCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.LtOrEq{Key: "topic", Value: c.Topic, Operator: d, Descriptor: &Meeting{}, FieldMask: "topic", RootDescriptor: &Meeting{}, CurrentDescriptor: &Meeting{}}
}

type MeetingDescriptionLtOrEq struct {
	Description string
}

func (c MeetingDescriptionLtOrEq) meetingCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.LtOrEq{Key: "description", Value: c.Description, Operator: d, Descriptor: &Meeting{}, FieldMask: "description", RootDescriptor: &Meeting{}, CurrentDescriptor: &Meeting{}}
}

type MeetingMeetingTypeLtOrEq struct {
	MeetingType MeetingType
}

func (c MeetingMeetingTypeLtOrEq) meetingCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.LtOrEq{Key: "meeting_type", Value: c.MeetingType, Operator: d, Descriptor: &Meeting{}, FieldMask: "meeting_type", RootDescriptor: &Meeting{}, CurrentDescriptor: &Meeting{}}
}

type MeetingMeetingAppTypeLtOrEq struct {
	MeetingAppType MeetingAppType
}

func (c MeetingMeetingAppTypeLtOrEq) meetingCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.LtOrEq{Key: "meeting_app_type", Value: c.MeetingAppType, Operator: d, Descriptor: &Meeting{}, FieldMask: "meeting_app_type", RootDescriptor: &Meeting{}, CurrentDescriptor: &Meeting{}}
}

type MeetingStartTimeLtOrEq struct {
	StartTime *timestamp.Timestamp
}

func (c MeetingStartTimeLtOrEq) meetingCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.LtOrEq{Key: "start_time", Value: c.StartTime, Operator: d, Descriptor: &Meeting{}, FieldMask: "start_time", RootDescriptor: &Meeting{}, CurrentDescriptor: &Meeting{}}
}

type MeetingEndTimeLtOrEq struct {
	EndTime *timestamp.Timestamp
}

func (c MeetingEndTimeLtOrEq) meetingCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.LtOrEq{Key: "end_time", Value: c.EndTime, Operator: d, Descriptor: &Meeting{}, FieldMask: "end_time", RootDescriptor: &Meeting{}, CurrentDescriptor: &Meeting{}}
}

type MeetingDurationLtOrEq struct {
	Duration int64
}

func (c MeetingDurationLtOrEq) meetingCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.LtOrEq{Key: "duration", Value: c.Duration, Operator: d, Descriptor: &Meeting{}, FieldMask: "duration", RootDescriptor: &Meeting{}, CurrentDescriptor: &Meeting{}}
}

type MeetingTimezoneLtOrEq struct {
	Timezone string
}

func (c MeetingTimezoneLtOrEq) meetingCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.LtOrEq{Key: "timezone", Value: c.Timezone, Operator: d, Descriptor: &Meeting{}, FieldMask: "timezone", RootDescriptor: &Meeting{}, CurrentDescriptor: &Meeting{}}
}

type MeetingPasswordLtOrEq struct {
	Password string
}

func (c MeetingPasswordLtOrEq) meetingCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.LtOrEq{Key: "password", Value: c.Password, Operator: d, Descriptor: &Meeting{}, FieldMask: "password", RootDescriptor: &Meeting{}, CurrentDescriptor: &Meeting{}}
}

type MeetingStartUrlLtOrEq struct {
	StartUrl string
}

func (c MeetingStartUrlLtOrEq) meetingCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.LtOrEq{Key: "start_url", Value: c.StartUrl, Operator: d, Descriptor: &Meeting{}, FieldMask: "start_url", RootDescriptor: &Meeting{}, CurrentDescriptor: &Meeting{}}
}

type MeetingJoinUrlLtOrEq struct {
	JoinUrl string
}

func (c MeetingJoinUrlLtOrEq) meetingCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.LtOrEq{Key: "join_url", Value: c.JoinUrl, Operator: d, Descriptor: &Meeting{}, FieldMask: "join_url", RootDescriptor: &Meeting{}, CurrentDescriptor: &Meeting{}}
}

type MeetingMeetingIdLtOrEq struct {
	MeetingId string
}

func (c MeetingMeetingIdLtOrEq) meetingCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.LtOrEq{Key: "meeting_id", Value: c.MeetingId, Operator: d, Descriptor: &Meeting{}, FieldMask: "meeting_id", RootDescriptor: &Meeting{}, CurrentDescriptor: &Meeting{}}
}

type MeetingUserIdLtOrEq struct {
	UserId string
}

func (c MeetingUserIdLtOrEq) meetingCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.LtOrEq{Key: "user_id", Value: c.UserId, Operator: d, Descriptor: &Meeting{}, FieldMask: "user_id", RootDescriptor: &Meeting{}, CurrentDescriptor: &Meeting{}}
}

type MeetingRecurrenceRuleLtOrEq struct {
	RecurrenceRule string
}

func (c MeetingRecurrenceRuleLtOrEq) meetingCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.LtOrEq{Key: "recurrence_rule", Value: c.RecurrenceRule, Operator: d, Descriptor: &Meeting{}, FieldMask: "recurrence_rule", RootDescriptor: &Meeting{}, CurrentDescriptor: &Meeting{}}
}

type MeetingErrorMessageLtOrEq struct {
	ErrorMessage string
}

func (c MeetingErrorMessageLtOrEq) meetingCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.LtOrEq{Key: "error_message", Value: c.ErrorMessage, Operator: d, Descriptor: &Meeting{}, FieldMask: "error_message", RootDescriptor: &Meeting{}, CurrentDescriptor: &Meeting{}}
}

type MeetingParticipantListIdLtOrEq struct {
	Id string
}

func (c MeetingParticipantListIdLtOrEq) meetingCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.LtOrEq{Key: "id", Value: c.Id, Operator: d, Descriptor: &ParticipantList{}, FieldMask: "participant_list.id", RootDescriptor: &Meeting{}, CurrentDescriptor: &Meeting{}}
}

type MeetingParticipantListStaffIdLtOrEq struct {
	StaffId string
}

func (c MeetingParticipantListStaffIdLtOrEq) meetingCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.LtOrEq{Key: "staff_id", Value: c.StaffId, Operator: d, Descriptor: &ParticipantList{}, FieldMask: "participant_list.staff_id", RootDescriptor: &Meeting{}, CurrentDescriptor: &Meeting{}}
}

type MeetingParticipantListParticipantIdLtOrEq struct {
	ParticipantId string
}

func (c MeetingParticipantListParticipantIdLtOrEq) meetingCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.LtOrEq{Key: "participant_id", Value: c.ParticipantId, Operator: d, Descriptor: &ParticipantList{}, FieldMask: "participant_list.participant_id", RootDescriptor: &Meeting{}, CurrentDescriptor: &Meeting{}}
}

type MeetingParticipantListFirstNameLtOrEq struct {
	FirstName string
}

func (c MeetingParticipantListFirstNameLtOrEq) meetingCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.LtOrEq{Key: "first_name", Value: c.FirstName, Operator: d, Descriptor: &ParticipantList{}, FieldMask: "participant_list.first_name", RootDescriptor: &Meeting{}, CurrentDescriptor: &Meeting{}}
}

type MeetingParticipantListEmailLtOrEq struct {
	Email string
}

func (c MeetingParticipantListEmailLtOrEq) meetingCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.LtOrEq{Key: "email", Value: c.Email, Operator: d, Descriptor: &ParticipantList{}, FieldMask: "participant_list.email", RootDescriptor: &Meeting{}, CurrentDescriptor: &Meeting{}}
}

type MeetingParticipantListLastNameLtOrEq struct {
	LastName string
}

func (c MeetingParticipantListLastNameLtOrEq) meetingCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.LtOrEq{Key: "last_name", Value: c.LastName, Operator: d, Descriptor: &ParticipantList{}, FieldMask: "participant_list.last_name", RootDescriptor: &Meeting{}, CurrentDescriptor: &Meeting{}}
}

type MeetingParticipantListParticipantJoinUrlLtOrEq struct {
	ParticipantJoinUrl string
}

func (c MeetingParticipantListParticipantJoinUrlLtOrEq) meetingCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.LtOrEq{Key: "participant_join_url", Value: c.ParticipantJoinUrl, Operator: d, Descriptor: &ParticipantList{}, FieldMask: "participant_list.participant_join_url", RootDescriptor: &Meeting{}, CurrentDescriptor: &Meeting{}}
}

type MeetingIdLike struct {
	Id string
}

func (c MeetingIdLike) meetingCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.Like{Key: "id", Value: c.Id, Operator: d, Descriptor: &Meeting{}, FieldMask: "id", RootDescriptor: &Meeting{}, CurrentDescriptor: &Meeting{}}
}

type MeetingTopicLike struct {
	Topic string
}

func (c MeetingTopicLike) meetingCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.Like{Key: "topic", Value: c.Topic, Operator: d, Descriptor: &Meeting{}, FieldMask: "topic", RootDescriptor: &Meeting{}, CurrentDescriptor: &Meeting{}}
}

type MeetingDescriptionLike struct {
	Description string
}

func (c MeetingDescriptionLike) meetingCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.Like{Key: "description", Value: c.Description, Operator: d, Descriptor: &Meeting{}, FieldMask: "description", RootDescriptor: &Meeting{}, CurrentDescriptor: &Meeting{}}
}

type MeetingTimezoneLike struct {
	Timezone string
}

func (c MeetingTimezoneLike) meetingCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.Like{Key: "timezone", Value: c.Timezone, Operator: d, Descriptor: &Meeting{}, FieldMask: "timezone", RootDescriptor: &Meeting{}, CurrentDescriptor: &Meeting{}}
}

type MeetingPasswordLike struct {
	Password string
}

func (c MeetingPasswordLike) meetingCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.Like{Key: "password", Value: c.Password, Operator: d, Descriptor: &Meeting{}, FieldMask: "password", RootDescriptor: &Meeting{}, CurrentDescriptor: &Meeting{}}
}

type MeetingStartUrlLike struct {
	StartUrl string
}

func (c MeetingStartUrlLike) meetingCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.Like{Key: "start_url", Value: c.StartUrl, Operator: d, Descriptor: &Meeting{}, FieldMask: "start_url", RootDescriptor: &Meeting{}, CurrentDescriptor: &Meeting{}}
}

type MeetingJoinUrlLike struct {
	JoinUrl string
}

func (c MeetingJoinUrlLike) meetingCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.Like{Key: "join_url", Value: c.JoinUrl, Operator: d, Descriptor: &Meeting{}, FieldMask: "join_url", RootDescriptor: &Meeting{}, CurrentDescriptor: &Meeting{}}
}

type MeetingMeetingIdLike struct {
	MeetingId string
}

func (c MeetingMeetingIdLike) meetingCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.Like{Key: "meeting_id", Value: c.MeetingId, Operator: d, Descriptor: &Meeting{}, FieldMask: "meeting_id", RootDescriptor: &Meeting{}, CurrentDescriptor: &Meeting{}}
}

type MeetingUserIdLike struct {
	UserId string
}

func (c MeetingUserIdLike) meetingCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.Like{Key: "user_id", Value: c.UserId, Operator: d, Descriptor: &Meeting{}, FieldMask: "user_id", RootDescriptor: &Meeting{}, CurrentDescriptor: &Meeting{}}
}

type MeetingRecurrenceRuleLike struct {
	RecurrenceRule string
}

func (c MeetingRecurrenceRuleLike) meetingCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.Like{Key: "recurrence_rule", Value: c.RecurrenceRule, Operator: d, Descriptor: &Meeting{}, FieldMask: "recurrence_rule", RootDescriptor: &Meeting{}, CurrentDescriptor: &Meeting{}}
}

type MeetingErrorMessageLike struct {
	ErrorMessage string
}

func (c MeetingErrorMessageLike) meetingCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.Like{Key: "error_message", Value: c.ErrorMessage, Operator: d, Descriptor: &Meeting{}, FieldMask: "error_message", RootDescriptor: &Meeting{}, CurrentDescriptor: &Meeting{}}
}

type MeetingParticipantListIdLike struct {
	Id string
}

func (c MeetingParticipantListIdLike) meetingCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.Like{Key: "id", Value: c.Id, Operator: d, Descriptor: &ParticipantList{}, FieldMask: "participant_list.id", RootDescriptor: &Meeting{}, CurrentDescriptor: &Meeting{}}
}

type MeetingParticipantListStaffIdLike struct {
	StaffId string
}

func (c MeetingParticipantListStaffIdLike) meetingCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.Like{Key: "staff_id", Value: c.StaffId, Operator: d, Descriptor: &ParticipantList{}, FieldMask: "participant_list.staff_id", RootDescriptor: &Meeting{}, CurrentDescriptor: &Meeting{}}
}

type MeetingParticipantListParticipantIdLike struct {
	ParticipantId string
}

func (c MeetingParticipantListParticipantIdLike) meetingCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.Like{Key: "participant_id", Value: c.ParticipantId, Operator: d, Descriptor: &ParticipantList{}, FieldMask: "participant_list.participant_id", RootDescriptor: &Meeting{}, CurrentDescriptor: &Meeting{}}
}

type MeetingParticipantListFirstNameLike struct {
	FirstName string
}

func (c MeetingParticipantListFirstNameLike) meetingCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.Like{Key: "first_name", Value: c.FirstName, Operator: d, Descriptor: &ParticipantList{}, FieldMask: "participant_list.first_name", RootDescriptor: &Meeting{}, CurrentDescriptor: &Meeting{}}
}

type MeetingParticipantListEmailLike struct {
	Email string
}

func (c MeetingParticipantListEmailLike) meetingCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.Like{Key: "email", Value: c.Email, Operator: d, Descriptor: &ParticipantList{}, FieldMask: "participant_list.email", RootDescriptor: &Meeting{}, CurrentDescriptor: &Meeting{}}
}

type MeetingParticipantListLastNameLike struct {
	LastName string
}

func (c MeetingParticipantListLastNameLike) meetingCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.Like{Key: "last_name", Value: c.LastName, Operator: d, Descriptor: &ParticipantList{}, FieldMask: "participant_list.last_name", RootDescriptor: &Meeting{}, CurrentDescriptor: &Meeting{}}
}

type MeetingParticipantListParticipantJoinUrlLike struct {
	ParticipantJoinUrl string
}

func (c MeetingParticipantListParticipantJoinUrlLike) meetingCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.Like{Key: "participant_join_url", Value: c.ParticipantJoinUrl, Operator: d, Descriptor: &ParticipantList{}, FieldMask: "participant_list.participant_join_url", RootDescriptor: &Meeting{}, CurrentDescriptor: &Meeting{}}
}

type MeetingDeleted struct {
	IsDeleted bool
}

func (c MeetingDeleted) meetingCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.Eq{Key: "is_deleted", Value: c.IsDeleted, Operator: d, Descriptor: &Meeting{}, FieldMask: "id", RootDescriptor: &Meeting{}, CurrentDescriptor: &Meeting{}}
}

type MeetingParticipantListDeleted struct {
	IsDeleted bool
}

func (c MeetingParticipantListDeleted) meetingCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.Eq{Key: "is_deleted", Value: c.IsDeleted, Operator: d, Descriptor: &ParticipantList{}, FieldMask: "participant_list.id", RootDescriptor: &Meeting{}, CurrentDescriptor: &Meeting{}}
}

type MeetingCreatedByEq struct {
	By string
}

func (c MeetingCreatedByEq) meetingCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.Eq{Key: "created_by", Value: c.By, Operator: d, Descriptor: &Meeting{}, RootDescriptor: &Meeting{}, CurrentDescriptor: &Meeting{}}
}

type MeetingCreatedOnEq struct {
	On *timestamp.Timestamp
}

func (c MeetingCreatedOnEq) meetingCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.Eq{Key: "created_on", Value: c.On, Operator: d, Descriptor: &Meeting{}, RootDescriptor: &Meeting{}, CurrentDescriptor: &Meeting{}}
}

type MeetingCreatedByNotEq struct {
	By string
}

func (c MeetingCreatedByNotEq) meetingCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.NotEq{Key: "created_by", Value: c.By, Operator: d, Descriptor: &Meeting{}, RootDescriptor: &Meeting{}, CurrentDescriptor: &Meeting{}}
}

type MeetingCreatedOnNotEq struct {
	On *timestamp.Timestamp
}

func (c MeetingCreatedOnNotEq) meetingCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.NotEq{Key: "created_on", Value: c.On, Operator: d, Descriptor: &Meeting{}, RootDescriptor: &Meeting{}, CurrentDescriptor: &Meeting{}}
}

type MeetingCreatedByGt struct {
	By string
}

func (c MeetingCreatedByGt) meetingCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.Gt{Key: "created_by", Value: c.By, Operator: d, Descriptor: &Meeting{}, RootDescriptor: &Meeting{}, CurrentDescriptor: &Meeting{}}
}

type MeetingCreatedOnGt struct {
	On *timestamp.Timestamp
}

func (c MeetingCreatedOnGt) meetingCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.Gt{Key: "created_on", Value: c.On, Operator: d, Descriptor: &Meeting{}, RootDescriptor: &Meeting{}, CurrentDescriptor: &Meeting{}}
}

type MeetingCreatedByLt struct {
	By string
}

func (c MeetingCreatedByLt) meetingCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.Lt{Key: "created_by", Value: c.By, Operator: d, Descriptor: &Meeting{}, RootDescriptor: &Meeting{}, CurrentDescriptor: &Meeting{}}
}

type MeetingCreatedOnLt struct {
	On *timestamp.Timestamp
}

func (c MeetingCreatedOnLt) meetingCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.Lt{Key: "created_on", Value: c.On, Operator: d, Descriptor: &Meeting{}, RootDescriptor: &Meeting{}, CurrentDescriptor: &Meeting{}}
}

type MeetingCreatedByGtOrEq struct {
	By string
}

func (c MeetingCreatedByGtOrEq) meetingCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.GtOrEq{Key: "created_by", Value: c.By, Operator: d, Descriptor: &Meeting{}, RootDescriptor: &Meeting{}, CurrentDescriptor: &Meeting{}}
}

type MeetingCreatedOnGtOrEq struct {
	On *timestamp.Timestamp
}

func (c MeetingCreatedOnGtOrEq) meetingCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.GtOrEq{Key: "created_on", Value: c.On, Operator: d, Descriptor: &Meeting{}, RootDescriptor: &Meeting{}, CurrentDescriptor: &Meeting{}}
}

type MeetingCreatedByLtOrEq struct {
	By string
}

func (c MeetingCreatedByLtOrEq) meetingCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.LtOrEq{Key: "created_by", Value: c.By, Operator: d, Descriptor: &Meeting{}, RootDescriptor: &Meeting{}, CurrentDescriptor: &Meeting{}}
}

type MeetingCreatedOnLtOrEq struct {
	On *timestamp.Timestamp
}

func (c MeetingCreatedOnLtOrEq) meetingCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.LtOrEq{Key: "created_on", Value: c.On, Operator: d, Descriptor: &Meeting{}, RootDescriptor: &Meeting{}, CurrentDescriptor: &Meeting{}}
}

type MeetingCreatedByLike struct {
	By string
}

func (c MeetingCreatedByLike) meetingCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.Like{Key: "created_by", Value: c.By, Operator: d, Descriptor: &Meeting{}, RootDescriptor: &Meeting{}, CurrentDescriptor: &Meeting{}}
}

type MeetingUpdatedByEq struct {
	By string
}

func (c MeetingUpdatedByEq) meetingCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.Eq{Key: "updated_by", Value: c.By, Operator: d, Descriptor: &Meeting{}, RootDescriptor: &Meeting{}, CurrentDescriptor: &Meeting{}}
}

type MeetingUpdatedOnEq struct {
	On *timestamp.Timestamp
}

func (c MeetingUpdatedOnEq) meetingCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.Eq{Key: "updated_on", Value: c.On, Operator: d, Descriptor: &Meeting{}, RootDescriptor: &Meeting{}, CurrentDescriptor: &Meeting{}}
}

type MeetingUpdatedByNotEq struct {
	By string
}

func (c MeetingUpdatedByNotEq) meetingCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.NotEq{Key: "updated_by", Value: c.By, Operator: d, Descriptor: &Meeting{}, RootDescriptor: &Meeting{}, CurrentDescriptor: &Meeting{}}
}

type MeetingUpdatedOnNotEq struct {
	On *timestamp.Timestamp
}

func (c MeetingUpdatedOnNotEq) meetingCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.NotEq{Key: "updated_on", Value: c.On, Operator: d, Descriptor: &Meeting{}, RootDescriptor: &Meeting{}, CurrentDescriptor: &Meeting{}}
}

type MeetingUpdatedByGt struct {
	By string
}

func (c MeetingUpdatedByGt) meetingCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.Gt{Key: "updated_by", Value: c.By, Operator: d, Descriptor: &Meeting{}, RootDescriptor: &Meeting{}, CurrentDescriptor: &Meeting{}}
}

type MeetingUpdatedOnGt struct {
	On *timestamp.Timestamp
}

func (c MeetingUpdatedOnGt) meetingCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.Gt{Key: "updated_on", Value: c.On, Operator: d, Descriptor: &Meeting{}, RootDescriptor: &Meeting{}, CurrentDescriptor: &Meeting{}}
}

type MeetingUpdatedByLt struct {
	By string
}

func (c MeetingUpdatedByLt) meetingCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.Lt{Key: "updated_by", Value: c.By, Operator: d, Descriptor: &Meeting{}, RootDescriptor: &Meeting{}, CurrentDescriptor: &Meeting{}}
}

type MeetingUpdatedOnLt struct {
	On *timestamp.Timestamp
}

func (c MeetingUpdatedOnLt) meetingCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.Lt{Key: "updated_on", Value: c.On, Operator: d, Descriptor: &Meeting{}, RootDescriptor: &Meeting{}, CurrentDescriptor: &Meeting{}}
}

type MeetingUpdatedByGtOrEq struct {
	By string
}

func (c MeetingUpdatedByGtOrEq) meetingCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.GtOrEq{Key: "updated_by", Value: c.By, Operator: d, Descriptor: &Meeting{}, RootDescriptor: &Meeting{}, CurrentDescriptor: &Meeting{}}
}

type MeetingUpdatedOnGtOrEq struct {
	On *timestamp.Timestamp
}

func (c MeetingUpdatedOnGtOrEq) meetingCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.GtOrEq{Key: "updated_on", Value: c.On, Operator: d, Descriptor: &Meeting{}, RootDescriptor: &Meeting{}, CurrentDescriptor: &Meeting{}}
}

type MeetingUpdatedByLtOrEq struct {
	By string
}

func (c MeetingUpdatedByLtOrEq) meetingCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.LtOrEq{Key: "updated_by", Value: c.By, Operator: d, Descriptor: &Meeting{}, RootDescriptor: &Meeting{}, CurrentDescriptor: &Meeting{}}
}

type MeetingUpdatedOnLtOrEq struct {
	On *timestamp.Timestamp
}

func (c MeetingUpdatedOnLtOrEq) meetingCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.LtOrEq{Key: "updated_on", Value: c.On, Operator: d, Descriptor: &Meeting{}, RootDescriptor: &Meeting{}, CurrentDescriptor: &Meeting{}}
}

type MeetingUpdatedByLike struct {
	By string
}

func (c MeetingUpdatedByLike) meetingCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.Like{Key: "updated_by", Value: c.By, Operator: d, Descriptor: &Meeting{}, RootDescriptor: &Meeting{}, CurrentDescriptor: &Meeting{}}
}

type MeetingDeletedByEq struct {
	By string
}

func (c MeetingDeletedByEq) meetingCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.Eq{Key: "deleted_by", Value: c.By, Operator: d, Descriptor: &Meeting{}, RootDescriptor: &Meeting{}, CurrentDescriptor: &Meeting{}}
}

type MeetingDeletedOnEq struct {
	On *timestamp.Timestamp
}

func (c MeetingDeletedOnEq) meetingCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.Eq{Key: "deleted_on", Value: c.On, Operator: d, Descriptor: &Meeting{}, RootDescriptor: &Meeting{}, CurrentDescriptor: &Meeting{}}
}

type MeetingDeletedByNotEq struct {
	By string
}

func (c MeetingDeletedByNotEq) meetingCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.NotEq{Key: "deleted_by", Value: c.By, Operator: d, Descriptor: &Meeting{}, RootDescriptor: &Meeting{}, CurrentDescriptor: &Meeting{}}
}

type MeetingDeletedOnNotEq struct {
	On *timestamp.Timestamp
}

func (c MeetingDeletedOnNotEq) meetingCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.NotEq{Key: "deleted_on", Value: c.On, Operator: d, Descriptor: &Meeting{}, RootDescriptor: &Meeting{}, CurrentDescriptor: &Meeting{}}
}

type MeetingDeletedByGt struct {
	By string
}

func (c MeetingDeletedByGt) meetingCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.Gt{Key: "deleted_by", Value: c.By, Operator: d, Descriptor: &Meeting{}, RootDescriptor: &Meeting{}, CurrentDescriptor: &Meeting{}}
}

type MeetingDeletedOnGt struct {
	On *timestamp.Timestamp
}

func (c MeetingDeletedOnGt) meetingCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.Gt{Key: "deleted_on", Value: c.On, Operator: d, Descriptor: &Meeting{}, RootDescriptor: &Meeting{}, CurrentDescriptor: &Meeting{}}
}

type MeetingDeletedByLt struct {
	By string
}

func (c MeetingDeletedByLt) meetingCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.Lt{Key: "deleted_by", Value: c.By, Operator: d, Descriptor: &Meeting{}, RootDescriptor: &Meeting{}, CurrentDescriptor: &Meeting{}}
}

type MeetingDeletedOnLt struct {
	On *timestamp.Timestamp
}

func (c MeetingDeletedOnLt) meetingCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.Lt{Key: "deleted_on", Value: c.On, Operator: d, Descriptor: &Meeting{}, RootDescriptor: &Meeting{}, CurrentDescriptor: &Meeting{}}
}

type MeetingDeletedByGtOrEq struct {
	By string
}

func (c MeetingDeletedByGtOrEq) meetingCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.GtOrEq{Key: "deleted_by", Value: c.By, Operator: d, Descriptor: &Meeting{}, RootDescriptor: &Meeting{}, CurrentDescriptor: &Meeting{}}
}

type MeetingDeletedOnGtOrEq struct {
	On *timestamp.Timestamp
}

func (c MeetingDeletedOnGtOrEq) meetingCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.GtOrEq{Key: "deleted_on", Value: c.On, Operator: d, Descriptor: &Meeting{}, RootDescriptor: &Meeting{}, CurrentDescriptor: &Meeting{}}
}

type MeetingDeletedByLtOrEq struct {
	By string
}

func (c MeetingDeletedByLtOrEq) meetingCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.LtOrEq{Key: "deleted_by", Value: c.By, Operator: d, Descriptor: &Meeting{}, RootDescriptor: &Meeting{}, CurrentDescriptor: &Meeting{}}
}

type MeetingDeletedOnLtOrEq struct {
	On *timestamp.Timestamp
}

func (c MeetingDeletedOnLtOrEq) meetingCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.LtOrEq{Key: "deleted_on", Value: c.On, Operator: d, Descriptor: &Meeting{}, RootDescriptor: &Meeting{}, CurrentDescriptor: &Meeting{}}
}

type MeetingDeletedByLike struct {
	By string
}

func (c MeetingDeletedByLike) meetingCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.Like{Key: "deleted_by", Value: c.By, Operator: d, Descriptor: &Meeting{}, RootDescriptor: &Meeting{}, CurrentDescriptor: &Meeting{}}
}

type MeetingIdIn struct {
	Id []string
}

func (c MeetingIdIn) meetingCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.In{Key: "id", Value: c.Id, Operator: d, Descriptor: &Meeting{}, FieldMask: "id", RootDescriptor: &Meeting{}, CurrentDescriptor: &Meeting{}}
}

type MeetingTopicIn struct {
	Topic []string
}

func (c MeetingTopicIn) meetingCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.In{Key: "topic", Value: c.Topic, Operator: d, Descriptor: &Meeting{}, FieldMask: "topic", RootDescriptor: &Meeting{}, CurrentDescriptor: &Meeting{}}
}

type MeetingDescriptionIn struct {
	Description []string
}

func (c MeetingDescriptionIn) meetingCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.In{Key: "description", Value: c.Description, Operator: d, Descriptor: &Meeting{}, FieldMask: "description", RootDescriptor: &Meeting{}, CurrentDescriptor: &Meeting{}}
}

type MeetingMeetingTypeIn struct {
	MeetingType []MeetingType
}

func (c MeetingMeetingTypeIn) meetingCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.In{Key: "meeting_type", Value: c.MeetingType, Operator: d, Descriptor: &Meeting{}, FieldMask: "meeting_type", RootDescriptor: &Meeting{}, CurrentDescriptor: &Meeting{}}
}

type MeetingMeetingAppTypeIn struct {
	MeetingAppType []MeetingAppType
}

func (c MeetingMeetingAppTypeIn) meetingCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.In{Key: "meeting_app_type", Value: c.MeetingAppType, Operator: d, Descriptor: &Meeting{}, FieldMask: "meeting_app_type", RootDescriptor: &Meeting{}, CurrentDescriptor: &Meeting{}}
}

type MeetingStartTimeIn struct {
	StartTime []*timestamp.Timestamp
}

func (c MeetingStartTimeIn) meetingCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.In{Key: "start_time", Value: c.StartTime, Operator: d, Descriptor: &Meeting{}, FieldMask: "start_time", RootDescriptor: &Meeting{}, CurrentDescriptor: &Meeting{}}
}

type MeetingEndTimeIn struct {
	EndTime []*timestamp.Timestamp
}

func (c MeetingEndTimeIn) meetingCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.In{Key: "end_time", Value: c.EndTime, Operator: d, Descriptor: &Meeting{}, FieldMask: "end_time", RootDescriptor: &Meeting{}, CurrentDescriptor: &Meeting{}}
}

type MeetingDurationIn struct {
	Duration []int64
}

func (c MeetingDurationIn) meetingCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.In{Key: "duration", Value: c.Duration, Operator: d, Descriptor: &Meeting{}, FieldMask: "duration", RootDescriptor: &Meeting{}, CurrentDescriptor: &Meeting{}}
}

type MeetingTimezoneIn struct {
	Timezone []string
}

func (c MeetingTimezoneIn) meetingCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.In{Key: "timezone", Value: c.Timezone, Operator: d, Descriptor: &Meeting{}, FieldMask: "timezone", RootDescriptor: &Meeting{}, CurrentDescriptor: &Meeting{}}
}

type MeetingPasswordIn struct {
	Password []string
}

func (c MeetingPasswordIn) meetingCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.In{Key: "password", Value: c.Password, Operator: d, Descriptor: &Meeting{}, FieldMask: "password", RootDescriptor: &Meeting{}, CurrentDescriptor: &Meeting{}}
}

type MeetingStartUrlIn struct {
	StartUrl []string
}

func (c MeetingStartUrlIn) meetingCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.In{Key: "start_url", Value: c.StartUrl, Operator: d, Descriptor: &Meeting{}, FieldMask: "start_url", RootDescriptor: &Meeting{}, CurrentDescriptor: &Meeting{}}
}

type MeetingJoinUrlIn struct {
	JoinUrl []string
}

func (c MeetingJoinUrlIn) meetingCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.In{Key: "join_url", Value: c.JoinUrl, Operator: d, Descriptor: &Meeting{}, FieldMask: "join_url", RootDescriptor: &Meeting{}, CurrentDescriptor: &Meeting{}}
}

type MeetingMeetingIdIn struct {
	MeetingId []string
}

func (c MeetingMeetingIdIn) meetingCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.In{Key: "meeting_id", Value: c.MeetingId, Operator: d, Descriptor: &Meeting{}, FieldMask: "meeting_id", RootDescriptor: &Meeting{}, CurrentDescriptor: &Meeting{}}
}

type MeetingUserIdIn struct {
	UserId []string
}

func (c MeetingUserIdIn) meetingCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.In{Key: "user_id", Value: c.UserId, Operator: d, Descriptor: &Meeting{}, FieldMask: "user_id", RootDescriptor: &Meeting{}, CurrentDescriptor: &Meeting{}}
}

type MeetingRecurrenceRuleIn struct {
	RecurrenceRule []string
}

func (c MeetingRecurrenceRuleIn) meetingCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.In{Key: "recurrence_rule", Value: c.RecurrenceRule, Operator: d, Descriptor: &Meeting{}, FieldMask: "recurrence_rule", RootDescriptor: &Meeting{}, CurrentDescriptor: &Meeting{}}
}

type MeetingErrorMessageIn struct {
	ErrorMessage []string
}

func (c MeetingErrorMessageIn) meetingCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.In{Key: "error_message", Value: c.ErrorMessage, Operator: d, Descriptor: &Meeting{}, FieldMask: "error_message", RootDescriptor: &Meeting{}, CurrentDescriptor: &Meeting{}}
}

type MeetingParticipantListIdIn struct {
	Id []string
}

func (c MeetingParticipantListIdIn) meetingCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.In{Key: "id", Value: c.Id, Operator: d, Descriptor: &ParticipantList{}, FieldMask: "participant_list.id", RootDescriptor: &Meeting{}, CurrentDescriptor: &Meeting{}}
}

type MeetingParticipantListStaffIdIn struct {
	StaffId []string
}

func (c MeetingParticipantListStaffIdIn) meetingCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.In{Key: "staff_id", Value: c.StaffId, Operator: d, Descriptor: &ParticipantList{}, FieldMask: "participant_list.staff_id", RootDescriptor: &Meeting{}, CurrentDescriptor: &Meeting{}}
}

type MeetingParticipantListParticipantIdIn struct {
	ParticipantId []string
}

func (c MeetingParticipantListParticipantIdIn) meetingCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.In{Key: "participant_id", Value: c.ParticipantId, Operator: d, Descriptor: &ParticipantList{}, FieldMask: "participant_list.participant_id", RootDescriptor: &Meeting{}, CurrentDescriptor: &Meeting{}}
}

type MeetingParticipantListFirstNameIn struct {
	FirstName []string
}

func (c MeetingParticipantListFirstNameIn) meetingCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.In{Key: "first_name", Value: c.FirstName, Operator: d, Descriptor: &ParticipantList{}, FieldMask: "participant_list.first_name", RootDescriptor: &Meeting{}, CurrentDescriptor: &Meeting{}}
}

type MeetingParticipantListEmailIn struct {
	Email []string
}

func (c MeetingParticipantListEmailIn) meetingCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.In{Key: "email", Value: c.Email, Operator: d, Descriptor: &ParticipantList{}, FieldMask: "participant_list.email", RootDescriptor: &Meeting{}, CurrentDescriptor: &Meeting{}}
}

type MeetingParticipantListLastNameIn struct {
	LastName []string
}

func (c MeetingParticipantListLastNameIn) meetingCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.In{Key: "last_name", Value: c.LastName, Operator: d, Descriptor: &ParticipantList{}, FieldMask: "participant_list.last_name", RootDescriptor: &Meeting{}, CurrentDescriptor: &Meeting{}}
}

type MeetingParticipantListParticipantJoinUrlIn struct {
	ParticipantJoinUrl []string
}

func (c MeetingParticipantListParticipantJoinUrlIn) meetingCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.In{Key: "participant_join_url", Value: c.ParticipantJoinUrl, Operator: d, Descriptor: &ParticipantList{}, FieldMask: "participant_list.participant_join_url", RootDescriptor: &Meeting{}, CurrentDescriptor: &Meeting{}}
}

type MeetingIdNotIn struct {
	Id []string
}

func (c MeetingIdNotIn) meetingCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.NotIn{Key: "id", Value: c.Id, Operator: d, Descriptor: &Meeting{}, FieldMask: "id", RootDescriptor: &Meeting{}, CurrentDescriptor: &Meeting{}}
}

type MeetingTopicNotIn struct {
	Topic []string
}

func (c MeetingTopicNotIn) meetingCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.NotIn{Key: "topic", Value: c.Topic, Operator: d, Descriptor: &Meeting{}, FieldMask: "topic", RootDescriptor: &Meeting{}, CurrentDescriptor: &Meeting{}}
}

type MeetingDescriptionNotIn struct {
	Description []string
}

func (c MeetingDescriptionNotIn) meetingCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.NotIn{Key: "description", Value: c.Description, Operator: d, Descriptor: &Meeting{}, FieldMask: "description", RootDescriptor: &Meeting{}, CurrentDescriptor: &Meeting{}}
}

type MeetingMeetingTypeNotIn struct {
	MeetingType []MeetingType
}

func (c MeetingMeetingTypeNotIn) meetingCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.NotIn{Key: "meeting_type", Value: c.MeetingType, Operator: d, Descriptor: &Meeting{}, FieldMask: "meeting_type", RootDescriptor: &Meeting{}, CurrentDescriptor: &Meeting{}}
}

type MeetingMeetingAppTypeNotIn struct {
	MeetingAppType []MeetingAppType
}

func (c MeetingMeetingAppTypeNotIn) meetingCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.NotIn{Key: "meeting_app_type", Value: c.MeetingAppType, Operator: d, Descriptor: &Meeting{}, FieldMask: "meeting_app_type", RootDescriptor: &Meeting{}, CurrentDescriptor: &Meeting{}}
}

type MeetingStartTimeNotIn struct {
	StartTime []*timestamp.Timestamp
}

func (c MeetingStartTimeNotIn) meetingCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.NotIn{Key: "start_time", Value: c.StartTime, Operator: d, Descriptor: &Meeting{}, FieldMask: "start_time", RootDescriptor: &Meeting{}, CurrentDescriptor: &Meeting{}}
}

type MeetingEndTimeNotIn struct {
	EndTime []*timestamp.Timestamp
}

func (c MeetingEndTimeNotIn) meetingCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.NotIn{Key: "end_time", Value: c.EndTime, Operator: d, Descriptor: &Meeting{}, FieldMask: "end_time", RootDescriptor: &Meeting{}, CurrentDescriptor: &Meeting{}}
}

type MeetingDurationNotIn struct {
	Duration []int64
}

func (c MeetingDurationNotIn) meetingCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.NotIn{Key: "duration", Value: c.Duration, Operator: d, Descriptor: &Meeting{}, FieldMask: "duration", RootDescriptor: &Meeting{}, CurrentDescriptor: &Meeting{}}
}

type MeetingTimezoneNotIn struct {
	Timezone []string
}

func (c MeetingTimezoneNotIn) meetingCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.NotIn{Key: "timezone", Value: c.Timezone, Operator: d, Descriptor: &Meeting{}, FieldMask: "timezone", RootDescriptor: &Meeting{}, CurrentDescriptor: &Meeting{}}
}

type MeetingPasswordNotIn struct {
	Password []string
}

func (c MeetingPasswordNotIn) meetingCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.NotIn{Key: "password", Value: c.Password, Operator: d, Descriptor: &Meeting{}, FieldMask: "password", RootDescriptor: &Meeting{}, CurrentDescriptor: &Meeting{}}
}

type MeetingStartUrlNotIn struct {
	StartUrl []string
}

func (c MeetingStartUrlNotIn) meetingCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.NotIn{Key: "start_url", Value: c.StartUrl, Operator: d, Descriptor: &Meeting{}, FieldMask: "start_url", RootDescriptor: &Meeting{}, CurrentDescriptor: &Meeting{}}
}

type MeetingJoinUrlNotIn struct {
	JoinUrl []string
}

func (c MeetingJoinUrlNotIn) meetingCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.NotIn{Key: "join_url", Value: c.JoinUrl, Operator: d, Descriptor: &Meeting{}, FieldMask: "join_url", RootDescriptor: &Meeting{}, CurrentDescriptor: &Meeting{}}
}

type MeetingMeetingIdNotIn struct {
	MeetingId []string
}

func (c MeetingMeetingIdNotIn) meetingCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.NotIn{Key: "meeting_id", Value: c.MeetingId, Operator: d, Descriptor: &Meeting{}, FieldMask: "meeting_id", RootDescriptor: &Meeting{}, CurrentDescriptor: &Meeting{}}
}

type MeetingUserIdNotIn struct {
	UserId []string
}

func (c MeetingUserIdNotIn) meetingCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.NotIn{Key: "user_id", Value: c.UserId, Operator: d, Descriptor: &Meeting{}, FieldMask: "user_id", RootDescriptor: &Meeting{}, CurrentDescriptor: &Meeting{}}
}

type MeetingRecurrenceRuleNotIn struct {
	RecurrenceRule []string
}

func (c MeetingRecurrenceRuleNotIn) meetingCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.NotIn{Key: "recurrence_rule", Value: c.RecurrenceRule, Operator: d, Descriptor: &Meeting{}, FieldMask: "recurrence_rule", RootDescriptor: &Meeting{}, CurrentDescriptor: &Meeting{}}
}

type MeetingErrorMessageNotIn struct {
	ErrorMessage []string
}

func (c MeetingErrorMessageNotIn) meetingCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.NotIn{Key: "error_message", Value: c.ErrorMessage, Operator: d, Descriptor: &Meeting{}, FieldMask: "error_message", RootDescriptor: &Meeting{}, CurrentDescriptor: &Meeting{}}
}

type MeetingParticipantListIdNotIn struct {
	Id []string
}

func (c MeetingParticipantListIdNotIn) meetingCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.NotIn{Key: "id", Value: c.Id, Operator: d, Descriptor: &ParticipantList{}, FieldMask: "participant_list.id", RootDescriptor: &Meeting{}, CurrentDescriptor: &Meeting{}}
}

type MeetingParticipantListStaffIdNotIn struct {
	StaffId []string
}

func (c MeetingParticipantListStaffIdNotIn) meetingCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.NotIn{Key: "staff_id", Value: c.StaffId, Operator: d, Descriptor: &ParticipantList{}, FieldMask: "participant_list.staff_id", RootDescriptor: &Meeting{}, CurrentDescriptor: &Meeting{}}
}

type MeetingParticipantListParticipantIdNotIn struct {
	ParticipantId []string
}

func (c MeetingParticipantListParticipantIdNotIn) meetingCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.NotIn{Key: "participant_id", Value: c.ParticipantId, Operator: d, Descriptor: &ParticipantList{}, FieldMask: "participant_list.participant_id", RootDescriptor: &Meeting{}, CurrentDescriptor: &Meeting{}}
}

type MeetingParticipantListFirstNameNotIn struct {
	FirstName []string
}

func (c MeetingParticipantListFirstNameNotIn) meetingCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.NotIn{Key: "first_name", Value: c.FirstName, Operator: d, Descriptor: &ParticipantList{}, FieldMask: "participant_list.first_name", RootDescriptor: &Meeting{}, CurrentDescriptor: &Meeting{}}
}

type MeetingParticipantListEmailNotIn struct {
	Email []string
}

func (c MeetingParticipantListEmailNotIn) meetingCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.NotIn{Key: "email", Value: c.Email, Operator: d, Descriptor: &ParticipantList{}, FieldMask: "participant_list.email", RootDescriptor: &Meeting{}, CurrentDescriptor: &Meeting{}}
}

type MeetingParticipantListLastNameNotIn struct {
	LastName []string
}

func (c MeetingParticipantListLastNameNotIn) meetingCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.NotIn{Key: "last_name", Value: c.LastName, Operator: d, Descriptor: &ParticipantList{}, FieldMask: "participant_list.last_name", RootDescriptor: &Meeting{}, CurrentDescriptor: &Meeting{}}
}

type MeetingParticipantListParticipantJoinUrlNotIn struct {
	ParticipantJoinUrl []string
}

func (c MeetingParticipantListParticipantJoinUrlNotIn) meetingCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.NotIn{Key: "participant_join_url", Value: c.ParticipantJoinUrl, Operator: d, Descriptor: &ParticipantList{}, FieldMask: "participant_list.participant_join_url", RootDescriptor: &Meeting{}, CurrentDescriptor: &Meeting{}}
}

func (c TrueCondition) meetingCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.TrueCondition{Operator: d}
}

type meetingMapperObject struct {
	id               string
	topic            string
	description      string
	meetingType      MeetingType
	meetingAppType   MeetingAppType
	startTime        *timestamp.Timestamp
	endTime          *timestamp.Timestamp
	duration         int64
	timezone         string
	password         string
	startUrl         string
	joinUrl          string
	meetingId        string
	userId           string
	recurrenceRule   string
	recurrenceRuleId []string
	errorMessage     string
	participantList  map[string]*meetingParticipantListMapperObject
}

func (s *meetingMapperObject) GetUniqueIdentifier() string {
	return s.id
}

type meetingParticipantListMapperObject struct {
	id                 string
	staffId            string
	participantId      string
	firstName          string
	email              string
	lastName           string
	participantJoinUrl string
}

func (s *meetingParticipantListMapperObject) GetUniqueIdentifier() string {
	return s.id
}

func MapperMeeting(rows []*Meeting) []*Meeting {

	ids := make([]string, 0, len(rows))
	uniqueIDMap := map[string]bool{}
	for _, r := range rows {
		if uniqueIDMap[r.Id] {
			continue
		}
		uniqueIDMap[r.Id] = true
		ids = append(ids, r.Id)
	}

	combinedMeetingMappers := map[string]*meetingMapperObject{}

	for _, rw := range rows {

		tempMeeting := &meetingMapperObject{}
		tempMeetingParticipantList := &meetingParticipantListMapperObject{}

		if rw == nil {
			rw = rw.GetEmptyObject()
		}
		if len(rw.ParticipantList) == 0 {
			_ = rw.New("participant_list")
			rw.ParticipantList = append(rw.ParticipantList, &ParticipantList{})
			rw.ParticipantList[0] = rw.ParticipantList[0].GetEmptyObject()
		}
		tempMeetingParticipantList.id = rw.ParticipantList[0].Id
		tempMeetingParticipantList.staffId = rw.ParticipantList[0].StaffId
		tempMeetingParticipantList.participantId = rw.ParticipantList[0].ParticipantId
		tempMeetingParticipantList.firstName = rw.ParticipantList[0].FirstName
		tempMeetingParticipantList.email = rw.ParticipantList[0].Email
		tempMeetingParticipantList.lastName = rw.ParticipantList[0].LastName
		tempMeetingParticipantList.participantJoinUrl = rw.ParticipantList[0].ParticipantJoinUrl

		tempMeeting.id = rw.Id
		tempMeeting.topic = rw.Topic
		tempMeeting.description = rw.Description
		tempMeeting.meetingType = rw.MeetingType
		tempMeeting.meetingAppType = rw.MeetingAppType
		tempMeeting.startTime = rw.StartTime
		tempMeeting.endTime = rw.EndTime
		tempMeeting.duration = rw.Duration
		tempMeeting.timezone = rw.Timezone
		tempMeeting.password = rw.Password
		tempMeeting.startUrl = rw.StartUrl
		tempMeeting.joinUrl = rw.JoinUrl
		tempMeeting.meetingId = rw.MeetingId
		tempMeeting.userId = rw.UserId
		tempMeeting.recurrenceRule = rw.RecurrenceRule
		tempMeeting.recurrenceRuleId = rw.RecurrenceRuleId
		tempMeeting.errorMessage = rw.ErrorMessage
		tempMeeting.participantList = map[string]*meetingParticipantListMapperObject{
			tempMeetingParticipantList.GetUniqueIdentifier(): tempMeetingParticipantList,
		}

		if combinedMeetingMappers[tempMeeting.GetUniqueIdentifier()] == nil {
			combinedMeetingMappers[tempMeeting.GetUniqueIdentifier()] = tempMeeting
		} else {

			meetingMapper := combinedMeetingMappers[tempMeeting.GetUniqueIdentifier()]

			if meetingMapper.participantList[tempMeetingParticipantList.GetUniqueIdentifier()] == nil {
				meetingMapper.participantList[tempMeetingParticipantList.GetUniqueIdentifier()] = tempMeetingParticipantList
			}
			combinedMeetingMappers[tempMeeting.GetUniqueIdentifier()] = meetingMapper
		}

	}

	combinedMeetings := make(map[string]*Meeting, 0)

	for _, meeting := range combinedMeetingMappers {
		tempMeeting := &Meeting{}
		tempMeeting.Id = meeting.id
		tempMeeting.Topic = meeting.topic
		tempMeeting.Description = meeting.description
		tempMeeting.MeetingType = meeting.meetingType
		tempMeeting.MeetingAppType = meeting.meetingAppType
		tempMeeting.StartTime = meeting.startTime
		tempMeeting.EndTime = meeting.endTime
		tempMeeting.Duration = meeting.duration
		tempMeeting.Timezone = meeting.timezone
		tempMeeting.Password = meeting.password
		tempMeeting.StartUrl = meeting.startUrl
		tempMeeting.JoinUrl = meeting.joinUrl
		tempMeeting.MeetingId = meeting.meetingId
		tempMeeting.UserId = meeting.userId
		tempMeeting.RecurrenceRule = meeting.recurrenceRule
		tempMeeting.RecurrenceRuleId = meeting.recurrenceRuleId
		tempMeeting.ErrorMessage = meeting.errorMessage

		combinedMeetingParticipantLists := []*ParticipantList{}

		for _, meetingParticipantList := range meeting.participantList {
			tempMeetingParticipantList := &ParticipantList{}
			tempMeetingParticipantList.Id = meetingParticipantList.id
			tempMeetingParticipantList.StaffId = meetingParticipantList.staffId
			tempMeetingParticipantList.ParticipantId = meetingParticipantList.participantId
			tempMeetingParticipantList.FirstName = meetingParticipantList.firstName
			tempMeetingParticipantList.Email = meetingParticipantList.email
			tempMeetingParticipantList.LastName = meetingParticipantList.lastName
			tempMeetingParticipantList.ParticipantJoinUrl = meetingParticipantList.participantJoinUrl

			if tempMeetingParticipantList.Id == "" {
				continue
			}

			combinedMeetingParticipantLists = append(combinedMeetingParticipantLists, tempMeetingParticipantList)

		}
		tempMeeting.ParticipantList = combinedMeetingParticipantLists

		if tempMeeting.Id == "" {
			continue
		}

		combinedMeetings[tempMeeting.Id] = tempMeeting

	}
	list := make([]*Meeting, 0, len(combinedMeetings))
	for _, i := range ids {
		list = append(list, combinedMeetings[i])
	}
	return list
}

func (s MeetingStore) CreateParticipantLists(ctx context.Context, objFieldMask string, pid []string, list ...*ParticipantList) ([]string, error) {
	vv := make([]driver.Descriptor, len(list))
	for i := range list {
		vv[i] = list[i]
	}
	if s.withTx {
		return s.tx.Insert(ctx, vv, &ParticipantList{}, &Meeting{}, objFieldMask, pid)
	}
	return s.d.Insert(ctx, vv, &ParticipantList{}, &Meeting{}, objFieldMask, pid)
}

func (s MeetingStore) DeleteParticipantList(ctx context.Context, cond ParticipantListCondition) error {
	if s.withTx {
		return s.tx.Delete(ctx, cond.participantListCondToDriverMeetingCond(s.d), &ParticipantList{}, &Meeting{})
	}
	return s.d.Delete(ctx, cond.participantListCondToDriverMeetingCond(s.d), &ParticipantList{}, &Meeting{})
}

func (s MeetingStore) UpdateParticipantList(ctx context.Context, req *ParticipantList, fields []string, cond ParticipantListCondition) error {
	if s.withTx {
		return s.tx.Update(ctx, cond.participantListCondToDriverMeetingCond(s.d), req, &Meeting{}, fields...)
	}
	return s.d.Update(ctx, cond.participantListCondToDriverMeetingCond(s.d), req, &Meeting{}, fields...)
}

func (s MeetingStore) GetParticipantList(ctx context.Context, fields []string, cond ParticipantListCondition, opt ...getParticipantListsMeetingOption) (*ParticipantList, error) {
	if len(fields) == 0 {
		fields = (&ParticipantList{}).Fields()
	}
	m := MetaInfoForList{}
	listOpts := []listParticipantListsMeetingOption{
		&CursorBasedPagination{Limit: 1},
	}
	for _, o := range opt {
		t, _ := o.getValue()
		switch t {
		case driver.OptionType_MetaInfo:
			listOpts = append(listOpts, &m)
		}
	}
	objList, err := s.ListParticipantLists(ctx, fields, cond, listOpts...)
	if len(objList) == 0 && err == nil {
		err = errors.ErrNotFound
	}
	if err != nil {
		return nil, err
	}
	for _, o := range opt {
		t, in := o.getValue()
		switch t {
		case driver.OptionType_MetaInfo:
			in.(*MetaInfo).UpdatedBy = m[0].UpdatedBy
			in.(*MetaInfo).CreatedBy = m[0].CreatedBy
			in.(*MetaInfo).DeletedBy = m[0].DeletedBy
			in.(*MetaInfo).UpdatedOn = m[0].UpdatedOn
			in.(*MetaInfo).CreatedOn = m[0].CreatedOn
			in.(*MetaInfo).DeletedOn = m[0].DeletedOn
			in.(*MetaInfo).IsDeleted = m[0].IsDeleted
		}
	}
	return objList[0], nil
}

func (s MeetingStore) ListParticipantLists(ctx context.Context, fields []string, cond ParticipantListCondition, opt ...listParticipantListsMeetingOption) ([]*ParticipantList, error) {
	if len(fields) == 0 {
		fields = (&ParticipantList{}).Fields()
	}
	var (
		res driver.Result
		err error
		m   driver.MetaInfo
	)
	for _, o := range opt {
		t, in := o.getValue()
		switch t {
		case driver.OptionType_Pagination:
			cond, err = in.(participantListsMeetingPageInterface).applyOffsetToParticipantListsMeetingList(ctx, s.d, cond)
			if err != nil {
				return nil, err
			}
		case driver.OptionType_MetaInfo:
			s.d.MetaInfoRequested(&ctx, &m)
		}
	}
	if s.withTx {
		res, err = s.tx.Get(ctx, cond.participantListCondToDriverMeetingCond(s.d), &ParticipantList{}, &Meeting{}, fields...)
	} else {
		res, err = s.d.Get(ctx, cond.participantListCondToDriverMeetingCond(s.d), &ParticipantList{}, &Meeting{}, fields...)
	}
	if err != nil {
		return nil, err
	}
	if res == nil {
		return nil, nil
	}
	defer res.Close()

	list := make([]*ParticipantList, 0, 1000)
	infoMap := make(map[string]*driver.MetaInfo, 0)

	for res.Next(ctx) {
		obj := &ParticipantList{}
		if err := res.Scan(ctx, obj); err != nil {
			return nil, err
		}
		for _, o := range opt {
			t, _ := o.getValue()
			switch t {
			case driver.OptionType_MetaInfo:
				infoMap[obj.Id] = &driver.MetaInfo{
					UpdatedBy: m.UpdatedBy,
					CreatedBy: m.CreatedBy,
					DeletedBy: m.DeletedBy,
					UpdatedOn: m.UpdatedOn,
					CreatedOn: m.CreatedOn,
					DeletedOn: m.DeletedOn,
					IsDeleted: m.IsDeleted,
				}
				break
			}
		}
		list = append(list, obj)
	}

	if err := res.Close(); err != nil {
		return nil, err
	}

	list = MapperParticipantList(list)
	meta := &MetaInfoForList{}

	for _, o := range opt {
		t, in := o.getValue()
		switch t {
		case driver.OptionType_Pagination:
			list = in.(participantListsMeetingPageInterface).applyLimitToParticipantListsMeetingList(list)
		case driver.OptionType_MetaInfo:
			meta = in.(*MetaInfoForList)
		}
	}
	for _, l := range list {
		*meta = append(*meta, infoMap[l.Id])
	}
	return list, nil
}

type getParticipantListsMeetingOption interface {
	getOptParticipantListsMeeting() // method of no significant use
	getValue() (driver.OptionType, interface{})
}

func (*MetaInfo) getOptParticipantListsMeeting() { // method of no significant use
}

type listParticipantListsMeetingOption interface {
	listOptParticipantListsMeeting() // method of no significant use
	getValue() (driver.OptionType, interface{})
}

type participantListsMeetingPageInterface interface {
	listParticipantListsMeetingOption
	applyOffsetToParticipantListsMeetingList(context.Context, driver.Driver, ParticipantListCondition) (ParticipantListCondition, error)
	applyLimitToParticipantListsMeetingList([]*ParticipantList) []*ParticipantList // temporary
}

func (*MetaInfoForList) listOptParticipantListsMeeting() {
}

func (*OffsetBasedPagination) listOptParticipantListsMeeting() {
}

func (*CursorBasedPagination) listOptParticipantListsMeeting() {
}

func (p *OffsetBasedPagination) applyLimitToParticipantListsMeetingList(ls []*ParticipantList) []*ParticipantList {
	p.Total = len(ls)
	if p.Total <= p.Limit {
		return ls
	}
	return ls[0:p.Limit]
}

func (p *CursorBasedPagination) applyLimitToParticipantListsMeetingList(ls []*ParticipantList) []*ParticipantList {
	if len(ls) <= p.Limit {
		p.HasNext = false
		p.HasPrevious = false
		return ls
	}
	if p.UpOrDown {
		p.HasPrevious = true
		return ls[len(ls)-p.Limit:]
	} else {
		p.HasNext = true
		return ls[:p.Limit]
	}
}

func (p *OffsetBasedPagination) applyOffsetToParticipantListsMeetingList(ctx context.Context, d driver.Driver, cond ParticipantListCondition) (ParticipantListCondition, error) {
	offID, err := d.OffsetValue(ctx, &Meeting{}, &ParticipantList{}, p.Offset)
	if err != nil {
		return cond, err
	}
	return ParticipantListAnd{cond, ParticipantListIdGtOrEq{offID}}, nil
}

func (p *CursorBasedPagination) applyOffsetToParticipantListsMeetingList(ctx context.Context, d driver.Driver, cond ParticipantListCondition) (ParticipantListCondition, error) {
	if p.UpOrDown {
		if p.Cursor == "" {
			p.Cursor = "zzzzzz" // biggest id with prefix
		}
		return ParticipantListAnd{cond, ParticipantListIdLt{p.Cursor}}, nil
	} else {
		if p.Cursor != "" {
			cond = ParticipantListAnd{cond, ParticipantListIdGt{p.Cursor}}
		}
		return cond, nil
	}
}

type ParticipantListAnd []ParticipantListCondition

func (p ParticipantListAnd) participantListCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	dc := make([]driver.Conditioner, 0, len(p))
	for _, c := range p {
		dc = append(dc, c.participantListCondToDriverMeetingCond(d))
	}
	return driver.And{Conditioners: dc, Operator: d}
}

type ParticipantListOr []ParticipantListCondition

func (p ParticipantListOr) participantListCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	dc := make([]driver.Conditioner, 0, len(p))
	for _, c := range p {
		dc = append(dc, c.participantListCondToDriverMeetingCond(d))
	}
	return driver.Or{Conditioners: dc, Operator: d}
}

type ParticipantListParentEq struct {
	Parent string
}

func (c ParticipantListParentEq) participantListCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.Eq{Key: "parent", Value: c.Parent, Operator: d, Descriptor: &ParticipantList{}, RootDescriptor: &Meeting{}, CurrentDescriptor: &ParticipantList{}}
}

type ParticipantListParentNotEq struct {
	Parent string
}

func (c ParticipantListParentNotEq) participantListCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.NotEq{Key: "parent", Value: c.Parent, Operator: d, Descriptor: &ParticipantList{}, RootDescriptor: &Meeting{}, CurrentDescriptor: &ParticipantList{}}
}

type ParticipantListIdEq struct {
	Id string
}

func (c ParticipantListIdEq) participantListCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.Eq{Key: "id", Value: c.Id, Operator: d, Descriptor: &ParticipantList{}, FieldMask: "id", RootDescriptor: &Meeting{}, CurrentDescriptor: &ParticipantList{}}
}

type ParticipantListStaffIdEq struct {
	StaffId string
}

func (c ParticipantListStaffIdEq) participantListCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.Eq{Key: "staff_id", Value: c.StaffId, Operator: d, Descriptor: &ParticipantList{}, FieldMask: "staff_id", RootDescriptor: &Meeting{}, CurrentDescriptor: &ParticipantList{}}
}

type ParticipantListParticipantIdEq struct {
	ParticipantId string
}

func (c ParticipantListParticipantIdEq) participantListCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.Eq{Key: "participant_id", Value: c.ParticipantId, Operator: d, Descriptor: &ParticipantList{}, FieldMask: "participant_id", RootDescriptor: &Meeting{}, CurrentDescriptor: &ParticipantList{}}
}

type ParticipantListFirstNameEq struct {
	FirstName string
}

func (c ParticipantListFirstNameEq) participantListCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.Eq{Key: "first_name", Value: c.FirstName, Operator: d, Descriptor: &ParticipantList{}, FieldMask: "first_name", RootDescriptor: &Meeting{}, CurrentDescriptor: &ParticipantList{}}
}

type ParticipantListEmailEq struct {
	Email string
}

func (c ParticipantListEmailEq) participantListCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.Eq{Key: "email", Value: c.Email, Operator: d, Descriptor: &ParticipantList{}, FieldMask: "email", RootDescriptor: &Meeting{}, CurrentDescriptor: &ParticipantList{}}
}

type ParticipantListLastNameEq struct {
	LastName string
}

func (c ParticipantListLastNameEq) participantListCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.Eq{Key: "last_name", Value: c.LastName, Operator: d, Descriptor: &ParticipantList{}, FieldMask: "last_name", RootDescriptor: &Meeting{}, CurrentDescriptor: &ParticipantList{}}
}

type ParticipantListParticipantJoinUrlEq struct {
	ParticipantJoinUrl string
}

func (c ParticipantListParticipantJoinUrlEq) participantListCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.Eq{Key: "participant_join_url", Value: c.ParticipantJoinUrl, Operator: d, Descriptor: &ParticipantList{}, FieldMask: "participant_join_url", RootDescriptor: &Meeting{}, CurrentDescriptor: &ParticipantList{}}
}

type ParticipantListIdNotEq struct {
	Id string
}

func (c ParticipantListIdNotEq) participantListCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.NotEq{Key: "id", Value: c.Id, Operator: d, Descriptor: &ParticipantList{}, FieldMask: "id", RootDescriptor: &Meeting{}, CurrentDescriptor: &ParticipantList{}}
}

type ParticipantListStaffIdNotEq struct {
	StaffId string
}

func (c ParticipantListStaffIdNotEq) participantListCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.NotEq{Key: "staff_id", Value: c.StaffId, Operator: d, Descriptor: &ParticipantList{}, FieldMask: "staff_id", RootDescriptor: &Meeting{}, CurrentDescriptor: &ParticipantList{}}
}

type ParticipantListParticipantIdNotEq struct {
	ParticipantId string
}

func (c ParticipantListParticipantIdNotEq) participantListCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.NotEq{Key: "participant_id", Value: c.ParticipantId, Operator: d, Descriptor: &ParticipantList{}, FieldMask: "participant_id", RootDescriptor: &Meeting{}, CurrentDescriptor: &ParticipantList{}}
}

type ParticipantListFirstNameNotEq struct {
	FirstName string
}

func (c ParticipantListFirstNameNotEq) participantListCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.NotEq{Key: "first_name", Value: c.FirstName, Operator: d, Descriptor: &ParticipantList{}, FieldMask: "first_name", RootDescriptor: &Meeting{}, CurrentDescriptor: &ParticipantList{}}
}

type ParticipantListEmailNotEq struct {
	Email string
}

func (c ParticipantListEmailNotEq) participantListCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.NotEq{Key: "email", Value: c.Email, Operator: d, Descriptor: &ParticipantList{}, FieldMask: "email", RootDescriptor: &Meeting{}, CurrentDescriptor: &ParticipantList{}}
}

type ParticipantListLastNameNotEq struct {
	LastName string
}

func (c ParticipantListLastNameNotEq) participantListCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.NotEq{Key: "last_name", Value: c.LastName, Operator: d, Descriptor: &ParticipantList{}, FieldMask: "last_name", RootDescriptor: &Meeting{}, CurrentDescriptor: &ParticipantList{}}
}

type ParticipantListParticipantJoinUrlNotEq struct {
	ParticipantJoinUrl string
}

func (c ParticipantListParticipantJoinUrlNotEq) participantListCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.NotEq{Key: "participant_join_url", Value: c.ParticipantJoinUrl, Operator: d, Descriptor: &ParticipantList{}, FieldMask: "participant_join_url", RootDescriptor: &Meeting{}, CurrentDescriptor: &ParticipantList{}}
}

type ParticipantListIdGt struct {
	Id string
}

func (c ParticipantListIdGt) participantListCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.Gt{Key: "id", Value: c.Id, Operator: d, Descriptor: &ParticipantList{}, FieldMask: "id", RootDescriptor: &Meeting{}, CurrentDescriptor: &ParticipantList{}}
}

type ParticipantListStaffIdGt struct {
	StaffId string
}

func (c ParticipantListStaffIdGt) participantListCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.Gt{Key: "staff_id", Value: c.StaffId, Operator: d, Descriptor: &ParticipantList{}, FieldMask: "staff_id", RootDescriptor: &Meeting{}, CurrentDescriptor: &ParticipantList{}}
}

type ParticipantListParticipantIdGt struct {
	ParticipantId string
}

func (c ParticipantListParticipantIdGt) participantListCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.Gt{Key: "participant_id", Value: c.ParticipantId, Operator: d, Descriptor: &ParticipantList{}, FieldMask: "participant_id", RootDescriptor: &Meeting{}, CurrentDescriptor: &ParticipantList{}}
}

type ParticipantListFirstNameGt struct {
	FirstName string
}

func (c ParticipantListFirstNameGt) participantListCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.Gt{Key: "first_name", Value: c.FirstName, Operator: d, Descriptor: &ParticipantList{}, FieldMask: "first_name", RootDescriptor: &Meeting{}, CurrentDescriptor: &ParticipantList{}}
}

type ParticipantListEmailGt struct {
	Email string
}

func (c ParticipantListEmailGt) participantListCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.Gt{Key: "email", Value: c.Email, Operator: d, Descriptor: &ParticipantList{}, FieldMask: "email", RootDescriptor: &Meeting{}, CurrentDescriptor: &ParticipantList{}}
}

type ParticipantListLastNameGt struct {
	LastName string
}

func (c ParticipantListLastNameGt) participantListCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.Gt{Key: "last_name", Value: c.LastName, Operator: d, Descriptor: &ParticipantList{}, FieldMask: "last_name", RootDescriptor: &Meeting{}, CurrentDescriptor: &ParticipantList{}}
}

type ParticipantListParticipantJoinUrlGt struct {
	ParticipantJoinUrl string
}

func (c ParticipantListParticipantJoinUrlGt) participantListCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.Gt{Key: "participant_join_url", Value: c.ParticipantJoinUrl, Operator: d, Descriptor: &ParticipantList{}, FieldMask: "participant_join_url", RootDescriptor: &Meeting{}, CurrentDescriptor: &ParticipantList{}}
}

type ParticipantListIdLt struct {
	Id string
}

func (c ParticipantListIdLt) participantListCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.Lt{Key: "id", Value: c.Id, Operator: d, Descriptor: &ParticipantList{}, FieldMask: "id", RootDescriptor: &Meeting{}, CurrentDescriptor: &ParticipantList{}}
}

type ParticipantListStaffIdLt struct {
	StaffId string
}

func (c ParticipantListStaffIdLt) participantListCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.Lt{Key: "staff_id", Value: c.StaffId, Operator: d, Descriptor: &ParticipantList{}, FieldMask: "staff_id", RootDescriptor: &Meeting{}, CurrentDescriptor: &ParticipantList{}}
}

type ParticipantListParticipantIdLt struct {
	ParticipantId string
}

func (c ParticipantListParticipantIdLt) participantListCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.Lt{Key: "participant_id", Value: c.ParticipantId, Operator: d, Descriptor: &ParticipantList{}, FieldMask: "participant_id", RootDescriptor: &Meeting{}, CurrentDescriptor: &ParticipantList{}}
}

type ParticipantListFirstNameLt struct {
	FirstName string
}

func (c ParticipantListFirstNameLt) participantListCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.Lt{Key: "first_name", Value: c.FirstName, Operator: d, Descriptor: &ParticipantList{}, FieldMask: "first_name", RootDescriptor: &Meeting{}, CurrentDescriptor: &ParticipantList{}}
}

type ParticipantListEmailLt struct {
	Email string
}

func (c ParticipantListEmailLt) participantListCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.Lt{Key: "email", Value: c.Email, Operator: d, Descriptor: &ParticipantList{}, FieldMask: "email", RootDescriptor: &Meeting{}, CurrentDescriptor: &ParticipantList{}}
}

type ParticipantListLastNameLt struct {
	LastName string
}

func (c ParticipantListLastNameLt) participantListCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.Lt{Key: "last_name", Value: c.LastName, Operator: d, Descriptor: &ParticipantList{}, FieldMask: "last_name", RootDescriptor: &Meeting{}, CurrentDescriptor: &ParticipantList{}}
}

type ParticipantListParticipantJoinUrlLt struct {
	ParticipantJoinUrl string
}

func (c ParticipantListParticipantJoinUrlLt) participantListCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.Lt{Key: "participant_join_url", Value: c.ParticipantJoinUrl, Operator: d, Descriptor: &ParticipantList{}, FieldMask: "participant_join_url", RootDescriptor: &Meeting{}, CurrentDescriptor: &ParticipantList{}}
}

type ParticipantListIdGtOrEq struct {
	Id string
}

func (c ParticipantListIdGtOrEq) participantListCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.GtOrEq{Key: "id", Value: c.Id, Operator: d, Descriptor: &ParticipantList{}, FieldMask: "id", RootDescriptor: &Meeting{}, CurrentDescriptor: &ParticipantList{}}
}

type ParticipantListStaffIdGtOrEq struct {
	StaffId string
}

func (c ParticipantListStaffIdGtOrEq) participantListCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.GtOrEq{Key: "staff_id", Value: c.StaffId, Operator: d, Descriptor: &ParticipantList{}, FieldMask: "staff_id", RootDescriptor: &Meeting{}, CurrentDescriptor: &ParticipantList{}}
}

type ParticipantListParticipantIdGtOrEq struct {
	ParticipantId string
}

func (c ParticipantListParticipantIdGtOrEq) participantListCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.GtOrEq{Key: "participant_id", Value: c.ParticipantId, Operator: d, Descriptor: &ParticipantList{}, FieldMask: "participant_id", RootDescriptor: &Meeting{}, CurrentDescriptor: &ParticipantList{}}
}

type ParticipantListFirstNameGtOrEq struct {
	FirstName string
}

func (c ParticipantListFirstNameGtOrEq) participantListCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.GtOrEq{Key: "first_name", Value: c.FirstName, Operator: d, Descriptor: &ParticipantList{}, FieldMask: "first_name", RootDescriptor: &Meeting{}, CurrentDescriptor: &ParticipantList{}}
}

type ParticipantListEmailGtOrEq struct {
	Email string
}

func (c ParticipantListEmailGtOrEq) participantListCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.GtOrEq{Key: "email", Value: c.Email, Operator: d, Descriptor: &ParticipantList{}, FieldMask: "email", RootDescriptor: &Meeting{}, CurrentDescriptor: &ParticipantList{}}
}

type ParticipantListLastNameGtOrEq struct {
	LastName string
}

func (c ParticipantListLastNameGtOrEq) participantListCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.GtOrEq{Key: "last_name", Value: c.LastName, Operator: d, Descriptor: &ParticipantList{}, FieldMask: "last_name", RootDescriptor: &Meeting{}, CurrentDescriptor: &ParticipantList{}}
}

type ParticipantListParticipantJoinUrlGtOrEq struct {
	ParticipantJoinUrl string
}

func (c ParticipantListParticipantJoinUrlGtOrEq) participantListCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.GtOrEq{Key: "participant_join_url", Value: c.ParticipantJoinUrl, Operator: d, Descriptor: &ParticipantList{}, FieldMask: "participant_join_url", RootDescriptor: &Meeting{}, CurrentDescriptor: &ParticipantList{}}
}

type ParticipantListIdLtOrEq struct {
	Id string
}

func (c ParticipantListIdLtOrEq) participantListCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.LtOrEq{Key: "id", Value: c.Id, Operator: d, Descriptor: &ParticipantList{}, FieldMask: "id", RootDescriptor: &Meeting{}, CurrentDescriptor: &ParticipantList{}}
}

type ParticipantListStaffIdLtOrEq struct {
	StaffId string
}

func (c ParticipantListStaffIdLtOrEq) participantListCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.LtOrEq{Key: "staff_id", Value: c.StaffId, Operator: d, Descriptor: &ParticipantList{}, FieldMask: "staff_id", RootDescriptor: &Meeting{}, CurrentDescriptor: &ParticipantList{}}
}

type ParticipantListParticipantIdLtOrEq struct {
	ParticipantId string
}

func (c ParticipantListParticipantIdLtOrEq) participantListCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.LtOrEq{Key: "participant_id", Value: c.ParticipantId, Operator: d, Descriptor: &ParticipantList{}, FieldMask: "participant_id", RootDescriptor: &Meeting{}, CurrentDescriptor: &ParticipantList{}}
}

type ParticipantListFirstNameLtOrEq struct {
	FirstName string
}

func (c ParticipantListFirstNameLtOrEq) participantListCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.LtOrEq{Key: "first_name", Value: c.FirstName, Operator: d, Descriptor: &ParticipantList{}, FieldMask: "first_name", RootDescriptor: &Meeting{}, CurrentDescriptor: &ParticipantList{}}
}

type ParticipantListEmailLtOrEq struct {
	Email string
}

func (c ParticipantListEmailLtOrEq) participantListCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.LtOrEq{Key: "email", Value: c.Email, Operator: d, Descriptor: &ParticipantList{}, FieldMask: "email", RootDescriptor: &Meeting{}, CurrentDescriptor: &ParticipantList{}}
}

type ParticipantListLastNameLtOrEq struct {
	LastName string
}

func (c ParticipantListLastNameLtOrEq) participantListCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.LtOrEq{Key: "last_name", Value: c.LastName, Operator: d, Descriptor: &ParticipantList{}, FieldMask: "last_name", RootDescriptor: &Meeting{}, CurrentDescriptor: &ParticipantList{}}
}

type ParticipantListParticipantJoinUrlLtOrEq struct {
	ParticipantJoinUrl string
}

func (c ParticipantListParticipantJoinUrlLtOrEq) participantListCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.LtOrEq{Key: "participant_join_url", Value: c.ParticipantJoinUrl, Operator: d, Descriptor: &ParticipantList{}, FieldMask: "participant_join_url", RootDescriptor: &Meeting{}, CurrentDescriptor: &ParticipantList{}}
}

type ParticipantListIdLike struct {
	Id string
}

func (c ParticipantListIdLike) participantListCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.Like{Key: "id", Value: c.Id, Operator: d, Descriptor: &ParticipantList{}, FieldMask: "id", RootDescriptor: &Meeting{}, CurrentDescriptor: &ParticipantList{}}
}

type ParticipantListStaffIdLike struct {
	StaffId string
}

func (c ParticipantListStaffIdLike) participantListCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.Like{Key: "staff_id", Value: c.StaffId, Operator: d, Descriptor: &ParticipantList{}, FieldMask: "staff_id", RootDescriptor: &Meeting{}, CurrentDescriptor: &ParticipantList{}}
}

type ParticipantListParticipantIdLike struct {
	ParticipantId string
}

func (c ParticipantListParticipantIdLike) participantListCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.Like{Key: "participant_id", Value: c.ParticipantId, Operator: d, Descriptor: &ParticipantList{}, FieldMask: "participant_id", RootDescriptor: &Meeting{}, CurrentDescriptor: &ParticipantList{}}
}

type ParticipantListFirstNameLike struct {
	FirstName string
}

func (c ParticipantListFirstNameLike) participantListCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.Like{Key: "first_name", Value: c.FirstName, Operator: d, Descriptor: &ParticipantList{}, FieldMask: "first_name", RootDescriptor: &Meeting{}, CurrentDescriptor: &ParticipantList{}}
}

type ParticipantListEmailLike struct {
	Email string
}

func (c ParticipantListEmailLike) participantListCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.Like{Key: "email", Value: c.Email, Operator: d, Descriptor: &ParticipantList{}, FieldMask: "email", RootDescriptor: &Meeting{}, CurrentDescriptor: &ParticipantList{}}
}

type ParticipantListLastNameLike struct {
	LastName string
}

func (c ParticipantListLastNameLike) participantListCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.Like{Key: "last_name", Value: c.LastName, Operator: d, Descriptor: &ParticipantList{}, FieldMask: "last_name", RootDescriptor: &Meeting{}, CurrentDescriptor: &ParticipantList{}}
}

type ParticipantListParticipantJoinUrlLike struct {
	ParticipantJoinUrl string
}

func (c ParticipantListParticipantJoinUrlLike) participantListCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.Like{Key: "participant_join_url", Value: c.ParticipantJoinUrl, Operator: d, Descriptor: &ParticipantList{}, FieldMask: "participant_join_url", RootDescriptor: &Meeting{}, CurrentDescriptor: &ParticipantList{}}
}

type ParticipantListDeleted struct {
	IsDeleted bool
}

func (c ParticipantListDeleted) participantListCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.Eq{Key: "is_deleted", Value: c.IsDeleted, Operator: d, Descriptor: &ParticipantList{}, FieldMask: "id", RootDescriptor: &Meeting{}, CurrentDescriptor: &ParticipantList{}}
}

type ParticipantListCreatedByEq struct {
	By string
}

func (c ParticipantListCreatedByEq) participantListCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.Eq{Key: "created_by", Value: c.By, Operator: d, Descriptor: &ParticipantList{}, RootDescriptor: &Meeting{}, CurrentDescriptor: &ParticipantList{}}
}

type ParticipantListCreatedOnEq struct {
	On *timestamp.Timestamp
}

func (c ParticipantListCreatedOnEq) participantListCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.Eq{Key: "created_on", Value: c.On, Operator: d, Descriptor: &ParticipantList{}, RootDescriptor: &Meeting{}, CurrentDescriptor: &ParticipantList{}}
}

type ParticipantListCreatedByNotEq struct {
	By string
}

func (c ParticipantListCreatedByNotEq) participantListCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.NotEq{Key: "created_by", Value: c.By, Operator: d, Descriptor: &ParticipantList{}, RootDescriptor: &Meeting{}, CurrentDescriptor: &ParticipantList{}}
}

type ParticipantListCreatedOnNotEq struct {
	On *timestamp.Timestamp
}

func (c ParticipantListCreatedOnNotEq) participantListCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.NotEq{Key: "created_on", Value: c.On, Operator: d, Descriptor: &ParticipantList{}, RootDescriptor: &Meeting{}, CurrentDescriptor: &ParticipantList{}}
}

type ParticipantListCreatedByGt struct {
	By string
}

func (c ParticipantListCreatedByGt) participantListCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.Gt{Key: "created_by", Value: c.By, Operator: d, Descriptor: &ParticipantList{}, RootDescriptor: &Meeting{}, CurrentDescriptor: &ParticipantList{}}
}

type ParticipantListCreatedOnGt struct {
	On *timestamp.Timestamp
}

func (c ParticipantListCreatedOnGt) participantListCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.Gt{Key: "created_on", Value: c.On, Operator: d, Descriptor: &ParticipantList{}, RootDescriptor: &Meeting{}, CurrentDescriptor: &ParticipantList{}}
}

type ParticipantListCreatedByLt struct {
	By string
}

func (c ParticipantListCreatedByLt) participantListCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.Lt{Key: "created_by", Value: c.By, Operator: d, Descriptor: &ParticipantList{}, RootDescriptor: &Meeting{}, CurrentDescriptor: &ParticipantList{}}
}

type ParticipantListCreatedOnLt struct {
	On *timestamp.Timestamp
}

func (c ParticipantListCreatedOnLt) participantListCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.Lt{Key: "created_on", Value: c.On, Operator: d, Descriptor: &ParticipantList{}, RootDescriptor: &Meeting{}, CurrentDescriptor: &ParticipantList{}}
}

type ParticipantListCreatedByGtOrEq struct {
	By string
}

func (c ParticipantListCreatedByGtOrEq) participantListCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.GtOrEq{Key: "created_by", Value: c.By, Operator: d, Descriptor: &ParticipantList{}, RootDescriptor: &Meeting{}, CurrentDescriptor: &ParticipantList{}}
}

type ParticipantListCreatedOnGtOrEq struct {
	On *timestamp.Timestamp
}

func (c ParticipantListCreatedOnGtOrEq) participantListCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.GtOrEq{Key: "created_on", Value: c.On, Operator: d, Descriptor: &ParticipantList{}, RootDescriptor: &Meeting{}, CurrentDescriptor: &ParticipantList{}}
}

type ParticipantListCreatedByLtOrEq struct {
	By string
}

func (c ParticipantListCreatedByLtOrEq) participantListCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.LtOrEq{Key: "created_by", Value: c.By, Operator: d, Descriptor: &ParticipantList{}, RootDescriptor: &Meeting{}, CurrentDescriptor: &ParticipantList{}}
}

type ParticipantListCreatedOnLtOrEq struct {
	On *timestamp.Timestamp
}

func (c ParticipantListCreatedOnLtOrEq) participantListCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.LtOrEq{Key: "created_on", Value: c.On, Operator: d, Descriptor: &ParticipantList{}, RootDescriptor: &Meeting{}, CurrentDescriptor: &ParticipantList{}}
}

type ParticipantListCreatedByLike struct {
	By string
}

func (c ParticipantListCreatedByLike) participantListCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.Like{Key: "created_by", Value: c.By, Operator: d, Descriptor: &ParticipantList{}, RootDescriptor: &Meeting{}, CurrentDescriptor: &ParticipantList{}}
}

type ParticipantListUpdatedByEq struct {
	By string
}

func (c ParticipantListUpdatedByEq) participantListCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.Eq{Key: "updated_by", Value: c.By, Operator: d, Descriptor: &ParticipantList{}, RootDescriptor: &Meeting{}, CurrentDescriptor: &ParticipantList{}}
}

type ParticipantListUpdatedOnEq struct {
	On *timestamp.Timestamp
}

func (c ParticipantListUpdatedOnEq) participantListCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.Eq{Key: "updated_on", Value: c.On, Operator: d, Descriptor: &ParticipantList{}, RootDescriptor: &Meeting{}, CurrentDescriptor: &ParticipantList{}}
}

type ParticipantListUpdatedByNotEq struct {
	By string
}

func (c ParticipantListUpdatedByNotEq) participantListCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.NotEq{Key: "updated_by", Value: c.By, Operator: d, Descriptor: &ParticipantList{}, RootDescriptor: &Meeting{}, CurrentDescriptor: &ParticipantList{}}
}

type ParticipantListUpdatedOnNotEq struct {
	On *timestamp.Timestamp
}

func (c ParticipantListUpdatedOnNotEq) participantListCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.NotEq{Key: "updated_on", Value: c.On, Operator: d, Descriptor: &ParticipantList{}, RootDescriptor: &Meeting{}, CurrentDescriptor: &ParticipantList{}}
}

type ParticipantListUpdatedByGt struct {
	By string
}

func (c ParticipantListUpdatedByGt) participantListCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.Gt{Key: "updated_by", Value: c.By, Operator: d, Descriptor: &ParticipantList{}, RootDescriptor: &Meeting{}, CurrentDescriptor: &ParticipantList{}}
}

type ParticipantListUpdatedOnGt struct {
	On *timestamp.Timestamp
}

func (c ParticipantListUpdatedOnGt) participantListCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.Gt{Key: "updated_on", Value: c.On, Operator: d, Descriptor: &ParticipantList{}, RootDescriptor: &Meeting{}, CurrentDescriptor: &ParticipantList{}}
}

type ParticipantListUpdatedByLt struct {
	By string
}

func (c ParticipantListUpdatedByLt) participantListCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.Lt{Key: "updated_by", Value: c.By, Operator: d, Descriptor: &ParticipantList{}, RootDescriptor: &Meeting{}, CurrentDescriptor: &ParticipantList{}}
}

type ParticipantListUpdatedOnLt struct {
	On *timestamp.Timestamp
}

func (c ParticipantListUpdatedOnLt) participantListCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.Lt{Key: "updated_on", Value: c.On, Operator: d, Descriptor: &ParticipantList{}, RootDescriptor: &Meeting{}, CurrentDescriptor: &ParticipantList{}}
}

type ParticipantListUpdatedByGtOrEq struct {
	By string
}

func (c ParticipantListUpdatedByGtOrEq) participantListCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.GtOrEq{Key: "updated_by", Value: c.By, Operator: d, Descriptor: &ParticipantList{}, RootDescriptor: &Meeting{}, CurrentDescriptor: &ParticipantList{}}
}

type ParticipantListUpdatedOnGtOrEq struct {
	On *timestamp.Timestamp
}

func (c ParticipantListUpdatedOnGtOrEq) participantListCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.GtOrEq{Key: "updated_on", Value: c.On, Operator: d, Descriptor: &ParticipantList{}, RootDescriptor: &Meeting{}, CurrentDescriptor: &ParticipantList{}}
}

type ParticipantListUpdatedByLtOrEq struct {
	By string
}

func (c ParticipantListUpdatedByLtOrEq) participantListCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.LtOrEq{Key: "updated_by", Value: c.By, Operator: d, Descriptor: &ParticipantList{}, RootDescriptor: &Meeting{}, CurrentDescriptor: &ParticipantList{}}
}

type ParticipantListUpdatedOnLtOrEq struct {
	On *timestamp.Timestamp
}

func (c ParticipantListUpdatedOnLtOrEq) participantListCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.LtOrEq{Key: "updated_on", Value: c.On, Operator: d, Descriptor: &ParticipantList{}, RootDescriptor: &Meeting{}, CurrentDescriptor: &ParticipantList{}}
}

type ParticipantListUpdatedByLike struct {
	By string
}

func (c ParticipantListUpdatedByLike) participantListCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.Like{Key: "updated_by", Value: c.By, Operator: d, Descriptor: &ParticipantList{}, RootDescriptor: &Meeting{}, CurrentDescriptor: &ParticipantList{}}
}

type ParticipantListDeletedByEq struct {
	By string
}

func (c ParticipantListDeletedByEq) participantListCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.Eq{Key: "deleted_by", Value: c.By, Operator: d, Descriptor: &ParticipantList{}, RootDescriptor: &Meeting{}, CurrentDescriptor: &ParticipantList{}}
}

type ParticipantListDeletedOnEq struct {
	On *timestamp.Timestamp
}

func (c ParticipantListDeletedOnEq) participantListCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.Eq{Key: "deleted_on", Value: c.On, Operator: d, Descriptor: &ParticipantList{}, RootDescriptor: &Meeting{}, CurrentDescriptor: &ParticipantList{}}
}

type ParticipantListDeletedByNotEq struct {
	By string
}

func (c ParticipantListDeletedByNotEq) participantListCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.NotEq{Key: "deleted_by", Value: c.By, Operator: d, Descriptor: &ParticipantList{}, RootDescriptor: &Meeting{}, CurrentDescriptor: &ParticipantList{}}
}

type ParticipantListDeletedOnNotEq struct {
	On *timestamp.Timestamp
}

func (c ParticipantListDeletedOnNotEq) participantListCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.NotEq{Key: "deleted_on", Value: c.On, Operator: d, Descriptor: &ParticipantList{}, RootDescriptor: &Meeting{}, CurrentDescriptor: &ParticipantList{}}
}

type ParticipantListDeletedByGt struct {
	By string
}

func (c ParticipantListDeletedByGt) participantListCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.Gt{Key: "deleted_by", Value: c.By, Operator: d, Descriptor: &ParticipantList{}, RootDescriptor: &Meeting{}, CurrentDescriptor: &ParticipantList{}}
}

type ParticipantListDeletedOnGt struct {
	On *timestamp.Timestamp
}

func (c ParticipantListDeletedOnGt) participantListCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.Gt{Key: "deleted_on", Value: c.On, Operator: d, Descriptor: &ParticipantList{}, RootDescriptor: &Meeting{}, CurrentDescriptor: &ParticipantList{}}
}

type ParticipantListDeletedByLt struct {
	By string
}

func (c ParticipantListDeletedByLt) participantListCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.Lt{Key: "deleted_by", Value: c.By, Operator: d, Descriptor: &ParticipantList{}, RootDescriptor: &Meeting{}, CurrentDescriptor: &ParticipantList{}}
}

type ParticipantListDeletedOnLt struct {
	On *timestamp.Timestamp
}

func (c ParticipantListDeletedOnLt) participantListCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.Lt{Key: "deleted_on", Value: c.On, Operator: d, Descriptor: &ParticipantList{}, RootDescriptor: &Meeting{}, CurrentDescriptor: &ParticipantList{}}
}

type ParticipantListDeletedByGtOrEq struct {
	By string
}

func (c ParticipantListDeletedByGtOrEq) participantListCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.GtOrEq{Key: "deleted_by", Value: c.By, Operator: d, Descriptor: &ParticipantList{}, RootDescriptor: &Meeting{}, CurrentDescriptor: &ParticipantList{}}
}

type ParticipantListDeletedOnGtOrEq struct {
	On *timestamp.Timestamp
}

func (c ParticipantListDeletedOnGtOrEq) participantListCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.GtOrEq{Key: "deleted_on", Value: c.On, Operator: d, Descriptor: &ParticipantList{}, RootDescriptor: &Meeting{}, CurrentDescriptor: &ParticipantList{}}
}

type ParticipantListDeletedByLtOrEq struct {
	By string
}

func (c ParticipantListDeletedByLtOrEq) participantListCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.LtOrEq{Key: "deleted_by", Value: c.By, Operator: d, Descriptor: &ParticipantList{}, RootDescriptor: &Meeting{}, CurrentDescriptor: &ParticipantList{}}
}

type ParticipantListDeletedOnLtOrEq struct {
	On *timestamp.Timestamp
}

func (c ParticipantListDeletedOnLtOrEq) participantListCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.LtOrEq{Key: "deleted_on", Value: c.On, Operator: d, Descriptor: &ParticipantList{}, RootDescriptor: &Meeting{}, CurrentDescriptor: &ParticipantList{}}
}

type ParticipantListDeletedByLike struct {
	By string
}

func (c ParticipantListDeletedByLike) participantListCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.Like{Key: "deleted_by", Value: c.By, Operator: d, Descriptor: &ParticipantList{}, RootDescriptor: &Meeting{}, CurrentDescriptor: &ParticipantList{}}
}

type ParticipantListIdIn struct {
	Id []string
}

func (c ParticipantListIdIn) participantListCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.In{Key: "id", Value: c.Id, Operator: d, Descriptor: &ParticipantList{}, FieldMask: "id", RootDescriptor: &Meeting{}, CurrentDescriptor: &ParticipantList{}}
}

type ParticipantListStaffIdIn struct {
	StaffId []string
}

func (c ParticipantListStaffIdIn) participantListCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.In{Key: "staff_id", Value: c.StaffId, Operator: d, Descriptor: &ParticipantList{}, FieldMask: "staff_id", RootDescriptor: &Meeting{}, CurrentDescriptor: &ParticipantList{}}
}

type ParticipantListParticipantIdIn struct {
	ParticipantId []string
}

func (c ParticipantListParticipantIdIn) participantListCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.In{Key: "participant_id", Value: c.ParticipantId, Operator: d, Descriptor: &ParticipantList{}, FieldMask: "participant_id", RootDescriptor: &Meeting{}, CurrentDescriptor: &ParticipantList{}}
}

type ParticipantListFirstNameIn struct {
	FirstName []string
}

func (c ParticipantListFirstNameIn) participantListCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.In{Key: "first_name", Value: c.FirstName, Operator: d, Descriptor: &ParticipantList{}, FieldMask: "first_name", RootDescriptor: &Meeting{}, CurrentDescriptor: &ParticipantList{}}
}

type ParticipantListEmailIn struct {
	Email []string
}

func (c ParticipantListEmailIn) participantListCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.In{Key: "email", Value: c.Email, Operator: d, Descriptor: &ParticipantList{}, FieldMask: "email", RootDescriptor: &Meeting{}, CurrentDescriptor: &ParticipantList{}}
}

type ParticipantListLastNameIn struct {
	LastName []string
}

func (c ParticipantListLastNameIn) participantListCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.In{Key: "last_name", Value: c.LastName, Operator: d, Descriptor: &ParticipantList{}, FieldMask: "last_name", RootDescriptor: &Meeting{}, CurrentDescriptor: &ParticipantList{}}
}

type ParticipantListParticipantJoinUrlIn struct {
	ParticipantJoinUrl []string
}

func (c ParticipantListParticipantJoinUrlIn) participantListCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.In{Key: "participant_join_url", Value: c.ParticipantJoinUrl, Operator: d, Descriptor: &ParticipantList{}, FieldMask: "participant_join_url", RootDescriptor: &Meeting{}, CurrentDescriptor: &ParticipantList{}}
}

type ParticipantListIdNotIn struct {
	Id []string
}

func (c ParticipantListIdNotIn) participantListCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.NotIn{Key: "id", Value: c.Id, Operator: d, Descriptor: &ParticipantList{}, FieldMask: "id", RootDescriptor: &Meeting{}, CurrentDescriptor: &ParticipantList{}}
}

type ParticipantListStaffIdNotIn struct {
	StaffId []string
}

func (c ParticipantListStaffIdNotIn) participantListCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.NotIn{Key: "staff_id", Value: c.StaffId, Operator: d, Descriptor: &ParticipantList{}, FieldMask: "staff_id", RootDescriptor: &Meeting{}, CurrentDescriptor: &ParticipantList{}}
}

type ParticipantListParticipantIdNotIn struct {
	ParticipantId []string
}

func (c ParticipantListParticipantIdNotIn) participantListCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.NotIn{Key: "participant_id", Value: c.ParticipantId, Operator: d, Descriptor: &ParticipantList{}, FieldMask: "participant_id", RootDescriptor: &Meeting{}, CurrentDescriptor: &ParticipantList{}}
}

type ParticipantListFirstNameNotIn struct {
	FirstName []string
}

func (c ParticipantListFirstNameNotIn) participantListCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.NotIn{Key: "first_name", Value: c.FirstName, Operator: d, Descriptor: &ParticipantList{}, FieldMask: "first_name", RootDescriptor: &Meeting{}, CurrentDescriptor: &ParticipantList{}}
}

type ParticipantListEmailNotIn struct {
	Email []string
}

func (c ParticipantListEmailNotIn) participantListCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.NotIn{Key: "email", Value: c.Email, Operator: d, Descriptor: &ParticipantList{}, FieldMask: "email", RootDescriptor: &Meeting{}, CurrentDescriptor: &ParticipantList{}}
}

type ParticipantListLastNameNotIn struct {
	LastName []string
}

func (c ParticipantListLastNameNotIn) participantListCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.NotIn{Key: "last_name", Value: c.LastName, Operator: d, Descriptor: &ParticipantList{}, FieldMask: "last_name", RootDescriptor: &Meeting{}, CurrentDescriptor: &ParticipantList{}}
}

type ParticipantListParticipantJoinUrlNotIn struct {
	ParticipantJoinUrl []string
}

func (c ParticipantListParticipantJoinUrlNotIn) participantListCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.NotIn{Key: "participant_join_url", Value: c.ParticipantJoinUrl, Operator: d, Descriptor: &ParticipantList{}, FieldMask: "participant_join_url", RootDescriptor: &Meeting{}, CurrentDescriptor: &ParticipantList{}}
}

func (c TrueCondition) participantListCondToDriverMeetingCond(d driver.Driver) driver.Conditioner {
	return driver.TrueCondition{Operator: d}
}

type participantListMapperObject struct {
	id                 string
	staffId            string
	participantId      string
	firstName          string
	email              string
	lastName           string
	participantJoinUrl string
}

func (s *participantListMapperObject) GetUniqueIdentifier() string {
	return s.id
}

func MapperParticipantList(rows []*ParticipantList) []*ParticipantList {

	ids := make([]string, 0, len(rows))
	uniqueIDMap := map[string]bool{}
	for _, r := range rows {
		if uniqueIDMap[r.Id] {
			continue
		}
		uniqueIDMap[r.Id] = true
		ids = append(ids, r.Id)
	}

	combinedParticipantListMappers := map[string]*participantListMapperObject{}

	for _, rw := range rows {

		tempParticipantList := &participantListMapperObject{}

		if rw == nil {
			rw = rw.GetEmptyObject()
		}
		tempParticipantList.id = rw.Id
		tempParticipantList.staffId = rw.StaffId
		tempParticipantList.participantId = rw.ParticipantId
		tempParticipantList.firstName = rw.FirstName
		tempParticipantList.email = rw.Email
		tempParticipantList.lastName = rw.LastName
		tempParticipantList.participantJoinUrl = rw.ParticipantJoinUrl

		if combinedParticipantListMappers[tempParticipantList.GetUniqueIdentifier()] == nil {
			combinedParticipantListMappers[tempParticipantList.GetUniqueIdentifier()] = tempParticipantList
		}
	}

	combinedParticipantLists := make(map[string]*ParticipantList, 0)

	for _, participantList := range combinedParticipantListMappers {
		tempParticipantList := &ParticipantList{}
		tempParticipantList.Id = participantList.id
		tempParticipantList.StaffId = participantList.staffId
		tempParticipantList.ParticipantId = participantList.participantId
		tempParticipantList.FirstName = participantList.firstName
		tempParticipantList.Email = participantList.email
		tempParticipantList.LastName = participantList.lastName
		tempParticipantList.ParticipantJoinUrl = participantList.participantJoinUrl

		if tempParticipantList.Id == "" {
			continue
		}

		combinedParticipantLists[tempParticipantList.Id] = tempParticipantList

	}
	list := make([]*ParticipantList, 0, len(combinedParticipantLists))
	for _, i := range ids {
		list = append(list, combinedParticipantLists[i])
	}
	return list
}

func (m *Meeting) IsUsedMultipleTimes(f string) bool {
	return false
}

type MeetingCondition interface {
	meetingCondToDriverMeetingCond(d driver.Driver) driver.Conditioner
}

type ParticipantListCondition interface {
	participantListCondToDriverMeetingCond(d driver.Driver) driver.Conditioner
}
